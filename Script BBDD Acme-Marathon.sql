start transaction;

drop database if exists `Acme-Marathon`;
create database `Acme-Marathon`;

grant select, insert, update, delete 
  on `Acme-Marathon`.* to 'acme-user'@'%';

grant select, insert, update, delete, create, drop, references, index, alter, 
        create temporary tables, lock tables, create view, create routine, 
        alter routine, execute, trigger, show view
    on `Acme-Marathon`.* to 'acme-manager'@'%';