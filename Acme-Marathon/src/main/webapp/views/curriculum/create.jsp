<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<security:authorize access="hasRole('WORKER')">

	<form:form action="curriculum/save.do" modelAttribute="form" >
	
			
		
		<fieldset>
		  <legend> <spring:message code="curriculum.personalRecord" /> </legend>
		  
		  	<br />
		  
			<acme:textbox code="curriculum.personalRecord.name" path="personalRecord.name"/>
			<br />
			
			<acme:textbox code="curriculum.personalRecord.photo" path="personalRecord.photo"/>
			<br />
			
			<acme:textbox code="curriculum.personalRecord.phoneNumber" path="personalRecord.phoneNumber"/>	
			<br />
			
			<acme:textbox code="curriculum.personalRecord.linkedInProfile" path="personalRecord.linkedInProfile"/>	
			<br />
			
			<acme:textbox code="curriculum.personalRecord.email" path="personalRecord.email"/>	
			
		</fieldset>
		
			
			
			
		</fieldset>
	
		<br />
		
	    <input type="submit" name="save" value="<spring:message code="curriculum.save"/>" /> 
	
		<acme:cancel url="/curriculum/list.do" code="curriculum.cancel" /> 
		
	</form:form> 

</security:authorize>