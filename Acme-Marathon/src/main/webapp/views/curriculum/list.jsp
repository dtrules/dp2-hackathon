<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="curriculums" id="row" pagesize="5"
	requestURI="${requestUri}" class="displaytag">

        
	    <display:column titleKey="curriculum.delete">
        <input type="submit" name="delete" value="<spring:message code="curriculum.delete" />"
				onclick="javascript: relativeRedir('curriculum/delete.do?curriculumId=${row.id}');" />
        </display:column>
        
          <display:column titleKey="curriculum.personalRecord">
                <input type="submit" name="show" value="<spring:message code="curriculum.personalRecord.show" />"
				onclick="javascript: relativeRedir('personalRecord/show.do?curriculumId=${row.id}');" />
        </display:column>
        
		 <display:column titleKey="curriculum.educationRecord">
                <input type="submit" name="show" value="<spring:message code="curriculum.educationRecord.list" />"
				onclick="javascript: relativeRedir('educationRecord/list.do?curriculumId=${row.id}');" />
        </display:column>
        
        <display:column titleKey="curriculum.educationRecord">
                <input type="submit" name="show" value="<spring:message code="curriculum.educationRecord.create" />"
				onclick="javascript: relativeRedir('educationRecord/create.do?curriculumId=${row.id}');" />
        </display:column>
        
         <display:column titleKey="curriculum.professionalRecord">
                <input type="submit" name="show" value="<spring:message code="curriculum.professionalRecord.list" />"
				onclick="javascript: relativeRedir('professionalRecord/list.do?curriculumId=${row.id}');" />
        </display:column>
        
          <display:column titleKey="curriculum.professionalRecord">
                <input type="submit" name="show" value="<spring:message code="curriculum.professionalRecord.create" />"
				onclick="javascript: relativeRedir('professionalRecord/create.do?curriculumId=${row.id}');" />
        </display:column>


</display:table>

<jstl:if test="${tieneCurriculum eq false}">
<input type="submit" name="save" value="<spring:message code="curriculum.create" />"
				onclick="javascript: relativeRedir('curriculum/create.do');" />

</jstl:if>