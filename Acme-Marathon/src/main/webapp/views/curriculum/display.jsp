<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-------------------------------------- PROFESSIONAL RECORD ---------------------------------------->

<h2><spring:message code="curriculum.professionalRecord" /></h2>


<display:table name="professionalRecordS" id="row" pagesize="5" requestURI="${requestUri}" class="displaytag">


	<spring:message code="curriculum.professionalRecord.company" var="companyHeader" />
	<display:column property="company" title="${companyHeader}" />
	
	<spring:message code="curriculum.professionalRecord.role" var="roleHeader" />
	<display:column property="role" title="${roleHeader}" />
	
		
        <display:column titleKey="curriculum.professionalRecord.display">
               <input type="submit" name="show" value="<spring:message code="curriculum.professionalRecord.display" />"
				onclick="javascript: relativeRedir('professionalRecord/show.do?professionalRecordId=${row.id}');" />
        </display:column>
         

	<display:column titleKey="curriculum.professionalRecord.edit">
                <input type="submit" name="edit" value="<spring:message code="curriculum.professionalRecord.edit" />"
				onclick="javascript: relativeRedir('professionalRecord/edit.do?professionalRecordId=${row.id}');" />
        </display:column>
	
	    <display:column titleKey="curriculum.delete">
        <input type="submit" name="delete" value="<spring:message code="curriculum.delete" />"
				onclick="javascript: relativeRedir('professionalRecord/delete.do?professionalRecordId=${row.id}');" />
                </display:column>
        

</display:table>
              
           <input type="submit" name="create" value="<spring:message code="curriculum.professionalRecord.create" />"
				onclick="javascript: relativeRedir('professionalRecord/create.do?curriculumId=${curriculumId}');" />


<!-------------------------------------- EDUCATION RECORD ---------------------------------------->

<h2><spring:message code="curriculum.educationRecord" /></h2>


<display:table name="educationRecordS" id="row" pagesize="5"
	requestURI="${requestUri}" class="displaytag">


	
	<spring:message code="curriculum.educationRecord.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" />
	
	<spring:message code="curriculum.educationRecord.institution" var="institutionHeader" />
	<display:column property="institution" title="${institutionHeader}" />
	
	

       <display:column titleKey="curriculum.educationRecord.display">
               <input type="submit" name="edit" value="<spring:message code="curriculum.educationRecord.display" />"
				onclick="javascript: relativeRedir('educationRecord/show.do?educationRecordId=${row.id}');" />
        </display:column>
        
	<display:column titleKey="curriculum.educationRecord.edit">
                <input type="submit" name="edit" value="<spring:message code="curriculum.educationRecord.edit" />"
				onclick="javascript: relativeRedir('educationRecord/edit.do?educationRecordId=${row.id}');" />
        </display:column>
        
        <display:column titleKey="curriculum.delete">
         <input type="submit" name="delete" value="<spring:message code="curriculum.delete" />"
				onclick="javascript: relativeRedir('educationRecord/delete.do?educationRecordId=${row.id}');" />
          </display:column>

</display:table>

      <input type="submit" name="delete" value="<spring:message code="curriculum.educationRecord.create" />"
	onclick="javascript: relativeRedir('educationRecord/create.do?curriculumId=${curriculumId}');" />
	


<!-------------------------------------- PERSONAL record ---------------------------------------->

<h2><spring:message code="curriculum.personalRecord" /></h2>


<display:table name="personalRecord" id="row" pagesize="5"
	requestURI="${requestUri}" class="displaytag">

		<spring:message code="curriculum.personalRecord.name" var="nameHeader" />
		<display:column property="name" title="${nameHeader}" />
	
	  <display:column titleKey="curriculum.personalRecord.display">
               <input type="submit" name="show" value="<spring:message code="curriculum.personalRecord.display" />"
				onclick="javascript: relativeRedir('personalRecord/show.do?personalRecordId=${row.id}');" />
        </display:column>

	<display:column titleKey="curriculum.personalRecord.edit">
                 <input type="submit" name="edit" value="<spring:message code="curriculum.personalRecord.edit" />"
				onclick="javascript: relativeRedir('personalRecord/edit.do?personalRecordId=${row.id}');" />
        </display:column>
        

	

</display:table>