<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<display:table name="organizer" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

    <!-- Name -->
    <spring:message code="organizer.name" var="nameHeader" />
    <display:column property="name" title="${nameHeader}" />
	
	<!-- Surname -->
	<spring:message code="organizer.surname" var="surnameHeader" />
	<display:column property="surname" title="${surnameHeader}" />

    <!-- Username -->
    <spring:message code="organizer.username" var="usernameHeader" />
    <display:column property="userAccount.username" title="${usernameHeader}" />
    


</display:table>