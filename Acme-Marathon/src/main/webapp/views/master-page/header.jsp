<%--
 * header.jsp
 *
 * Copyright (C) 2019 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
	<a href="#"><img src="images/logo.png" alt="Acme-Marathon" /></a>
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		<security:authorize access="hasRole('ADMIN')">
			<li><a class="fNiv"><spring:message	code="master.page.administrator" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="administrator/create.do"><spring:message code="master.page.administrator.register" /></a></li>
					<li><a href="administrator/dashboard.do"><spring:message code="master.page.administrator.dashboard" /></a></li>
					<li><a href="runner/list.do"><spring:message code="master.page.runner.score" /></a></li>
					<li><a href="complaint/list.do"><spring:message code="master.page.complaint.list" /></a></li>
					<li><a href="customization/administrator/edit.do"><spring:message code="master.page.administrator.customization" /></a></li>
				</ul>
			</li>
			<li><a class="fNiv"><spring:message	code="master.page.category" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="category/administrator/list.do"><spring:message code="master.page.category.list" /></a></li>
				</ul>
			</li>
			<li><a class="fNiv"><spring:message	code="master.page.broadcast" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="message/broadcast.do"><spring:message code="master.page.broadcast.create" /></a></li>
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="hasRole('ORGANIZER')">
			<li><a class="fNiv"><spring:message	code="master.page.organizer" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="payment/organizer/list.do"><spring:message code="master.page.payment.list" /></a></li>
					<li><a href="marathon/organizer/listByOrganizer.do"><spring:message code="master.page.marathon.list" /></a></li>
					<li><a href="checkpoint/organizer/list.do"><spring:message code="master.page.checkpoint.list" /></a></li>
					<li><a href="workerApplication/list.do"><spring:message code="master.page.workerApplication.list" /></a></li>
					<li><a href="runnerApplication/list.do"><spring:message code="master.page.organizer.runnerApplication.list" /></a></li>				
					<li><a href="organizer/edit.do"><spring:message code="master.page.sponsor.edit" /></a></li>
				</ul>
			</li>
		</security:authorize>
		
				<security:authorize access="hasRole('WORKER')">
			   <li><a class="fNiv"><spring:message	code="master.page.worker" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="curriculum/list.do"><spring:message code="master.page.runner.curriculum" /></a></li>
					<li><a href="payment/organizer/listWorker.do"><spring:message code="master.page.payment.list" /></a></li>
					<li><a href="workerApplication/list.do"><spring:message code="master.page.workerApplication.list" /></a></li>				
					<li><a href="worker/edit.do"><spring:message code="master.page.sponsor.edit" /></a></li>
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="hasRole('RUNNER')">
			<li><a class="fNiv"><spring:message	code="master.page.runner" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="complaint/listRunner.do"><spring:message code="master.page.complaint.list.runner" /></a></li>	
					<li><a href="runnerApplication/list.do"><spring:message code="master.page.runner.runnerApplication.list" /></a></li>						
				</ul>
			</li>
			<li><a class="fNiv"><spring:message	code="master.page.marathon" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="marathon/list.do"><spring:message code="master.page.runner.marathon.list" /></a></li>							
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="hasRole('SPONSOR')">
			<li><a class="fNiv"><spring:message	code="master.page.sponsor" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="sponsorship/sponsor/list.do"><spring:message code="master.page.sponsorship.list" /></a></li>
					<li><a href="sponsor/edit.do"><spring:message code="master.page.sponsor.edit" /></a></li>
					
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a></li>
			
			<li><a class="fNiv"><spring:message	code="master.page.register" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="runner/create.do"><spring:message code="master.page.runner.register" /></a></li>
					<li><a href="worker/create.do"><spring:message code="master.page.worker.register" /></a></li>
					<li><a href="organizer/create.do"><spring:message code="master.page.organizer.register" /></a></li>
				</ul>
			</li>
			
		<li><a class="fNiv" href="sponsor/create.do"><spring:message code="master.page.sponsor.register" /></a></li>
		
				<li><a href="sponsorship/listAll.do"><spring:message code="master.page.sponsorship.listAll" /></a></li>
		
			
		</security:authorize>
		
		<security:authorize access="isAuthenticated()">
			<li><a class="fNiv"><spring:message	code="master.page.folder" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="folder/actor/list.do"><spring:message code="master.page.folder.list" /></a></li>
					<li><a href="message/create.do"><spring:message code="master.page.message.create" /></a></li>
				</ul>
			</li>
			<li>
				<a class="fNiv"> 
					<spring:message code="master.page.profile" /> 
			        (<security:authentication property="principal.username" />)
				</a>
				<ul>
					<li class="arrow"></li>
					<li><a href="socialProfile/list.do"><spring:message code="master.page.socialProfile.list" /></a></li>
					<li><a href="socialProfile/forgetMe.do"><spring:message code="master.page.socialProfile.forget" /></a></li>
					<li><a href="socialProfile/exportData.do"><spring:message code="master.page.socialProfile.exportData" /></a></li>					
					<li><a href="j_spring_security_logout"><spring:message code="master.page.logout" /> </a></li>
				</ul>
			</li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

