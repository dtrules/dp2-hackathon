<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="worker/create.do" modelAttribute="workerForm">

	<%-- username--%>
	<acme:textbox code="worker.username" path="userAccount.username" />
	<br>

	<%-- password--%>
	<acme:password code="worker.password" path="userAccount.password" />
	<br>
	
	<%-- Confirm password--%>
	<acme:password code="worker.ConfirmPassword" path="confirmPassword" />
	<br>

	<%-- Name --%>
	<acme:textbox code="worker.name" path="name" />
	<br>


	<%-- Surname --%>
	<acme:textbox code="worker.surname" path="surname" />
	<br>

	<%-- Photo --%>
	<acme:textbox code="worker.photo" path="photo" />
	<br>

	<%-- Phone --%>
	<acme:textbox code="worker.phoneNumber" path="phoneNumber" />
	<br>

	<%-- email --%>
	<acme:textbox code="worker.email" path="email" />
	<br>

	<%-- Address --%>
	<acme:textbox code="worker.address" path="address" />
	<br>
	
	<%-- holderName --%>
	<acme:textbox code="worker.creditCard.holderName" path="creditCard.holderName" />
	<br>
	
	<%-- holderName --%>
	<acme:textbox code="worker.creditCard.brandName" path="creditCard.brandName" />
	<br>
	
	<%-- Number --%>
	<acme:textbox code="worker.creditCard.number" path="creditCard.number" />
	<br>
	
	<%-- expiryMonth --%>
	<acme:textbox code="worker.creditCard.expiryMonth" path="creditCard.expiryMonth" />
	<br>
	
	<%-- expiryYear --%>
	<acme:textbox code="worker.creditCard.expiryYear" path="creditCard.expiryYear" />
	<br>
	
	<%-- cvv --%>
	<acme:textbox code="worker.creditCard.cvv" path="creditCard.cvv" />
	<br>
	
	<p><input id="field_terms" onchange="this.setCustomValidity(validity.valueMissing ? '<spring:message code="worker.check.terms"/>' : '');" type="checkbox" required name="terms"><spring:message code="worker.terms"/></p>

	<script type="text/javascript">

		function phoneNumberValidator() {

			var phoneNumber = document.getElementById("phone").value;

			var patternCCACPN = /^(\+[1-9][0-9]{0,2}) (\([1-9][0-9]{0,2}\)) (\d{3}\d+)/
			$;
			var patternCCPN = /^(\+[1-9][0-9]{0,2}) (\d{3}\d+)/
			$;
			var patternPN = /^(\d{3}\d+)/
			$;

			if (patternCCACPN.test(phoneNumber))
				return true;
			else if (patternCCPN.test(phoneNumber))
				return true;
			else if (patternPN.test(phoneNumber))
				return true;
			else
				return confirm('<spring:message code="worker.confirm"/>');
		}
		
	</script>

	<%-- Buttons --%>
	<input type="submit" name="save"
		value="<spring:message code="worker.save"/>"
		onClick="javascript: return phoneNumberValidator()"/>
	<acme:cancel code="worker.cancel" url="/"/>
	
</form:form>

<script>

  document.getElementById("field_terms").setCustomValidity("<spring:message code="worker.check.terms"/>");

</script>