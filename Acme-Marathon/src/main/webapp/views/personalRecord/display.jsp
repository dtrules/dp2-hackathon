<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<a href="${photo}"><img src="${photo}" height = "150px" width = "150px"/></a>


<display:table name="personalRecord" id="row" requestURI="personalRecord/show.do" class="displaytag">

 <display:column>
              <a href="personalRecord/edit.do?personalRecordId=${row.id}">
                <spring:message code="personalRecord.edit"/>
              </a>
        </display:column>

	<spring:message code="personalRecord.name" var="nameHeader" />
	<display:column property="name" title="${nameHeader}" />
	
	<spring:message code="personalRecord.email" var="emailHeader" />
	<display:column property="email" title="${emailHeader}" />
	
		<spring:message code="personalRecord.photo" var="photoHeader" />
	<display:column property="photo" title="${photoHeader}" />
	
	<spring:message code="personalRecord.phoneNumber" var="phoneNumberHeader" />
	<display:column property="phoneNumber" title="${phoneNumberHeader}" />
	

	
	<spring:message code="personalRecord.linkedInProfile" var="linkedInProfileHeader" />
	<display:column property="linkedInProfile" title="${linkedInProfileHeader}" autolink = "true"/>
	

</display:table>


	<acme:cancel code="personalRecord.cancel" url="/curriculum/list.do" />
