<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="personalRecord/create.do?curriculumId=${curriculumId}" modelAttribute="personalRecord">

	

	
	
	<%-- title --%>
	<acme:textbox code="personalRecord.name" path="name" />
	<br>
	
	<acme:textbox code="personalRecord.photo" path="photo" />
	<br>
	
	<%-- startMoment --%>
	<acme:textbox code="personalRecord.email" path="email" />
	<br>
	
	<%-- endMoment --%>
	<acme:textbox code="personalRecord.phoneNumber" path="phoneNumber" />
	<br>
	
	<%-- attachment --%>
	<acme:textbox code="personalRecord.linkedinProfile" path="linkedinProfile" />
	<br>
	
	
	
	<input type="submit" name="save"
		value="<spring:message code="personalRecord.save"/>"/>
	
	<acme:cancel code="personalRecord.cancel" url="curriculum/list.do" />
	
</form:form>

