<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="sponsorships" id="row" pagesize="5"
	requestURI="${requestUri}" class="displaytag">



        
        <display:column>
              <a href="sponsorship/show.do?sponsorshipId=${row.id}">
                <spring:message code="sponsorship.display"/>
              </a>
        </display:column>
        
         <display:column>
        <a href="sponsor/show.do?sponsorId=${row.sponsor.id}">
            <spring:message code="sponsor.show"/>
        </a>
    </display:column>
    

	<spring:message code="sponsorship.banner" var="bannerHeader" />
	<display:column property="banner" title="${bannerHeader}" />
	
	<spring:message code="sponsorship.targetPage" var="targetPageHeader" />
	<display:column property="targetPage" title="${targetPageHeader}" />
	
	<spring:message code="sponsorship.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" />
	
	<spring:message code="sponsorship.marathon" var="marathonHeader" />
	<display:column property="marathon" title="${marathonHeader}" />
	
	
	


	

</display:table>


