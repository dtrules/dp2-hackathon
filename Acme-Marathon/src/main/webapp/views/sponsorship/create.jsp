<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="sponsorship/sponsor/create.do" modelAttribute="sponsorship">

	<form:hidden path="sponsor" />
	
	

	<%-- Banner --%>
	<acme:textbox code="sponsorship.banner" path="banner" />
	<br>
	
	<%-- TargetPage --%>
	<acme:textbox code="sponsorship.targetPage" path="targetPage" />
	<br>
	
	<%-- Title --%>
	<acme:textbox code="sponsorship.title" path="title" />
	<br>
	
	<!--Marathon-->
	<form:label path="marathon">
		<spring:message code="sponsorship.marathon" />:
	</form:label>
	<form:select id="marathons" path="marathon">
		<form:option value="0" label="----" />
		<jstl:forEach items="${marathons}" var="p">
			<form:option value="${p.id}" label="${p.ticker}" />
		</jstl:forEach>
	</form:select>
	<form:errors cssClass="error" path="marathon" />
	


	

	<input type="submit" name="save"
		value="<spring:message code="sponsorship.save"/>"/>
	
	<acme:cancel code="sponsorship.cancel" url="sponsorship/sponsor/list.do" />
	
</form:form>

