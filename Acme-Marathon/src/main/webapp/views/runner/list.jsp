<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="runners" id="row" pagesize="5" requestURI="${requestUri}" class="displaytag">

	<spring:message code="runner.name" var="nameHeader" />
	<display:column property="name" title="${nameHeader}" />

	<spring:message code="runner.email" var="emailHeader" />
	<display:column property="email" title="${emailHeader}" />

	<spring:message code="runner.photo" var="photoHeader" />
	<display:column property="photo" title="${photoHeader}" />
	
	<display:column titleKey ="runner.score" >
	<jstl:choose>
				<jstl:when test="${row.score == null}">
					<spring:message code="runner.score.null" />
				</jstl:when>
				<jstl:otherwise>
					<jstl:out value="${row.score}"></jstl:out>
				</jstl:otherwise>
	</jstl:choose>
	</display:column>  
	

	<security:authorize access="hasRole('ADMIN')">
	
	<display:column titleKey="runner.compute">
			<input type="submit" name="compute" value="<spring:message code="runner.compute" />"
				onclick="javascript: relativeRedir('administrator/score.do?runnerId=${row.id}');" />
	</display:column>
	
	</security:authorize>
	

</display:table>
