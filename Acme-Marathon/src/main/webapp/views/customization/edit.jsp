<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="customization/administrator/edit.do" modelAttribute="customization">

	<form:hidden path="id" />
	<form:hidden path="version" />

	<%-- systemName--%>
	<acme:textbox code="customization.systemName" path="systemName" />
	<br>

	<%-- welcomeMessageEn--%>
	<acme:textbox code="customization.welcomeMessageEn" path="welcomeMessageEn" />
	<br>

	<%-- welcomeMessageEs--%>
	<acme:textbox code="customization.welcomeMessageEs" path="welcomeMessageEs" />
	<br>

	<%-- countryCode --%>
	<acme:textbox code="customization.countryCode" path="countryCode" />
	<br>

	<%-- vat --%>
	<acme:textbox code="customization.vat" path="vat" />
	<br>

	<%-- brandNames --%>
	<acme:textbox code="customization.brandNames" path="brandNames" />
	<br>

	<%-- spamWords --%>
	<acme:textbox code="customization.spamWords" path="spamWords" />
	<br>

	<%-- Buttons --%>
	<input type="submit" name="save"
		   value="<spring:message code="customization.save"/>"/>

	<acme:cancel code="customization.cancel" url="welcome.do" />


</form:form>