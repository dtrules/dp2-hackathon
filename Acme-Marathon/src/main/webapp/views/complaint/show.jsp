<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


	<fieldset>
	
	<b><spring:message code="complaint.ticker"></spring:message>:</b><jstl:out value="${complaint.ticker}"></jstl:out>
	<br />

	<b><spring:message code="complaint.description"></spring:message>:</b><jstl:out value="${complaint.description}"></jstl:out>
	<br />
	
	<b><spring:message code="complaint.moment"></spring:message>:</b><jstl:out value="${complaint.moment}"></jstl:out>
	<br />
	
	<b><spring:message code="complaint.attachment"></spring:message>:</b><jstl:out value="${complaint.attachment}"></jstl:out>
	<br />
	
	<b><spring:message code="complaint.status"></spring:message>:</b><jstl:out value="${complaint.status}"></jstl:out>
	<br />
	
	<b><spring:message code="complaint.answerDescription"></spring:message>:</b><jstl:out value="${complaint.answerDescription}"></jstl:out>
	<br />
	
	<b><spring:message code="complaint.answerMoment"></spring:message>:</b><jstl:out value="${complaint.answerMoment}"></jstl:out>
	<br />
	
	<b><spring:message code="complaint.marathon"></spring:message>:</b><jstl:out value="${complaint.marathon.ticker}"></jstl:out>
	<br />
	
	<b><spring:message code="complaint.runner"></spring:message>:</b><jstl:out value="${complaint.runner.name}"></jstl:out>
	<br />
	
	</fieldset>

	<br />
	
<!-- Cancel -->

<acme:cancel code="complaint.cancel" url="/" />