<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="complaint/save.do" modelAttribute="complaint" method="post">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="ticker" />
	<form:hidden path="moment" />
	<form:hidden path="description" />
	<form:hidden path="attachment" />
	<form:hidden path="status" />
	<form:hidden path="runner" />
	<form:hidden path="marathon" />

	
	<security:authorize access="hasRole('ORGANIZER')">

	<form:label path="answerDescription">
			<spring:message code="complaint.answerDescription"/>
	</form:label>
	
	<form:input path="answerDescription" placeHolder="Description..."/>
	    	<form:errors cssClass="error" path="answerDescription" />
	    	
	 </security:authorize>
	 
	<br/>	  
	<br/> 
	
	<input type="submit" name="save" value="<spring:message code="complaint.save"/>" />
    <acme:cancel code="complaint.cancel" url="/" />


</form:form>