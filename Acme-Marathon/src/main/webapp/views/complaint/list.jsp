<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<display:table name="complaints" id="row" pagesize="5" requestURI="${requestUri}" class="displaytag">
	
	<spring:message code="complaint.ticker" var="ticker" />
	<display:column property="ticker" title="${ticker}" />
	
	<spring:message code="complaint.description" var="description" />
	<display:column property="description" title="${description}" />
	
	<spring:message code="complaint.moment" var="moment" />
	<display:column property="moment" title="${moment}" />
	

	<display:column titleKey="complaint.show">
			<input type="submit" name="show" value="<spring:message code="complaint.show" />"
				onclick="javascript: relativeRedir('complaint/show.do?complaintId=${row.id}');" />
	</display:column>
	
	<security:authorize access="hasRole('ORGANIZER')">
		<display:column titleKey="complaint.edit">
		<jstl:if test="${row.status eq true}">
		<input type="submit" name="edit" value="<spring:message code="complaint.edit" />"
				onclick="javascript: relativeRedir('complaint/edit.do?complaintId=${row.id}');" />
				</jstl:if>
			<jstl:if test="${row.status eq false}">
			<spring:message code="complaint.no.draft" />
		 </jstl:if>
		</display:column>
	</security:authorize>
	
	<security:authorize access="hasRole('ADMIN')">
		<display:column titleKey="complaint.change.status">
		<jstl:if test="${row.status eq true}">
		<input type="submit" name="change" value="<spring:message code="complaint.change.status" />"
				onclick="javascript: relativeRedir('complaint/changeStatus.do?complaintId=${row.id}');" />
				</jstl:if>
			<jstl:if test="${row.status eq false}">
			<spring:message code="complaint.no.draft" />
		 </jstl:if>
		</display:column>
	</security:authorize>

	
	
</display:table>

<br/>

<security:authorize access="hasRole('RUNNER')">
	<input type="submit" name="create"
		value="<spring:message code="complaint.create" />"
		onclick="javascript: relativeRedir('complaint/create.do');" />
</security:authorize>