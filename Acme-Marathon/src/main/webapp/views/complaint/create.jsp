<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="complaint/save.do" modelAttribute="complaint" method="post">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="ticker" />
	<form:hidden path="moment" />
	<form:hidden path="runner" />
	<form:hidden path="answerDescription" />
	
	
	<acme:textbox code="complaint.description" path="description" />
	<br/>
	
	<acme:textbox code="complaint.attachment" path="attachment" />
	<br/>

	<jstl:if test='${complaint.status == true}'>
	<acme:boolean code="complaint.status" trueCode="complaint.true" falseCode="complaint.false" path="status"/>
	<br/>
	<br/>			
	</jstl:if>
	
	<jstl:if test='${complaint.id == 0}'>
	
	<form:label path="marathon">
	<spring:message code="complaint.marathon" />:
	</form:label>
	
	<form:select path="marathon" >
	<form:options items="${marathons}" itemLabel="ticker" />
	</form:select>	
	
	</jstl:if>
	
	<br />	
	<br />	

	<input type="submit" name="save" value="<spring:message code="complaint.save"/>" />
    <acme:cancel code="complaint.cancel" url="/complaint/listRunner.do" />


</form:form>