<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


	<h2> <spring:message code="workerApplication.pending"/> </h2>
	<display:table name="pendingApps" id="row" pagesize="5" requestURI="${requestUri}" class="displaytag">
		
		<spring:message code="workerApplication.status" var="status" />
		<display:column style="background-color: grey" property="status" title="${status}" />
		
		<spring:message code="workerApplication.moment" var="moment" />
		<display:column style="background-color: grey" property="moment" title="${moment}" />
		
		<display:column style="background-color: grey" titleKey="workerApplication.show">
			<input type="submit" name="show" value="<spring:message code="workerApplication.show" />"
				onclick="javascript: relativeRedir('workerApplication/show.do?workerApplicationId=${row.id}');" />
		</display:column>
		
		<security:authorize access="hasRole('ORGANIZER')">
		<display:column style="background-color: grey" titleKey="workerApplication.accept">
			<jstl:if test="${row.status eq 'PENDING'}">
			<input type="submit" name="accept" value="<spring:message code="workerApplication.accept" />"
				onclick="javascript: relativeRedir('workerApplication/accept.do?workerApplicationId=${row.id}');" />
			</jstl:if>
		</display:column>
	</security:authorize>
	
	<security:authorize access="hasRole('ORGANIZER')">
		<display:column style="background-color: grey" titleKey="workerApplication.reject">
			<jstl:if test="${row.status eq 'PENDING'}">
			<input type="submit" name="reject" value="<spring:message code="workerApplication.reject" />"
				onclick="javascript: relativeRedir('workerApplication/reject.do?workerApplicationId=${row.id}');" />
			</jstl:if>
		</display:column>
	</security:authorize>
		
	</display:table>


<security:authorize access="hasRole('WORKER')">
	<input type="submit" name="create" value="<spring:message code="workerApplication.create" />"
		onclick="javascript: relativeRedir('workerApplication/create.do');" />
</security:authorize>

<br/>
<br/>
	
<h2> <spring:message code="workerApplication.accepted"/> </h2>
	<display:table name="acceptedApps" id="row" pagesize="5" requestURI="${requestUri}" class="displaytag">
		
		<spring:message code="workerApplication.status" var="status" />
			<display:column style="background-color: green" property="status" title="${status}" />
			
			<spring:message code="workerApplication.moment" var="moment" />
			<display:column style="background-color: green" property="moment" title="${moment}" />
		
			<display:column style="background-color: green" titleKey="workerApplication.show">
				<input type="submit" name="show" value="<spring:message code="workerApplication.show" />"
					onclick="javascript: relativeRedir('workerApplication/show.do?workerApplicationId=${row.id}');" />
			</display:column>
		
	</display:table>
	
	<br/>
	
<h2> <spring:message code="workerApplication.rejected"/> </h2>
	<display:table name="rejectedApps" id="row" pagesize="5" requestURI="${requestUri}" class="displaytag">
		
			<spring:message code="workerApplication.status" var="status" />
			<display:column style="background-color: red" property="status" title="${status}" />
			
			<spring:message code="workerApplication.moment" var="moment" />
			<display:column style="background-color: red" property="moment" title="${moment}" />
		
			<display:column style="background-color: red" titleKey="workerApplication.show">
				<input type="submit" name="show" value="<spring:message code="workerApplication.show" />"
					onclick="javascript: relativeRedir('workerApplication/show.do?workerApplicationId=${row.id}');" />
			</display:column>	
		
	</display:table>