<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="workerApplication/save.do" modelAttribute="workerApplication">

	<%-- Hidden properties from workerApplication--%>
	
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="moment" />
	<form:hidden path="status" />
	<form:hidden path="worker" />

	<!--Hours-->
	<form:label path="hours">
		<spring:message code="workerApplication.hours"/>
	</form:label>
	<form:input path="hours"/>
		<form:errors cssClass="error" path="hours" />
		<br />
	<br />
	    	
	<!--Comment-->
	<form:label path="comment">
		<spring:message code="workerApplication.comment"/>
	</form:label>
	<form:input path="comment" />
		<form:errors cssClass="error" path="comment" />
		<br />
	<br />
		
	<!--Marathon-->	
	<form:label path="marathon">
	<spring:message code="workerApplication.marathon" />:
	</form:label>
	
	<form:select path="marathon" >
	<form:options items="${marathons}" itemLabel="ticker" />
	</form:select>	
		
	<br />
	<br />
	
	<%-- Buttons --%>
	<input type="submit" name="save" value="<spring:message code="workerApplication.save"/>"/>
	<acme:cancel code="workerApplication.cancel" url="/workerApplication/list.do" />
	
</form:form>