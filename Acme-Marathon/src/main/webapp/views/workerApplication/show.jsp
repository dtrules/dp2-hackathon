<%--
 * show.jsp
 *
 * Copyright (C) 2019 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
	
<fieldset>

<b><spring:message code="workerApplication.moment"></spring:message>: </b><jstl:out value="${workerApplication.moment}"></jstl:out>
<br />

<b><spring:message code="workerApplication.status"></spring:message>: </b><jstl:out value="${workerApplication.status}"></jstl:out>
<br />

<b><spring:message code="workerApplication.comment"></spring:message>:</b><jstl:out value="${workerApplication.comment}"></jstl:out>
<br />
	
<b><spring:message code="workerApplication.hours"></spring:message>: </b><jstl:out value="${workerApplication.hours}"></jstl:out>
<br />

<b><spring:message code="workerApplication.worker"></spring:message>: </b><jstl:out value="${workerApplication.worker.name}"></jstl:out>
<br />

<b><spring:message code="workerApplication.marathon"></spring:message>: </b><jstl:out value="${workerApplication.marathon.ticker}"></jstl:out>


</fieldset>

<br />
<br />
	
<!-- Cancel -->

		<button type="button" onclick="javascript: relativeRedir('workerApplication/list.do')">
			<spring:message code="workerApplication.cancel" />
		</button>