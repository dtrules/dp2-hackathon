<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="educationRecord/create.do?curriculumId=${curriculumId}" modelAttribute="educationRecord">

	

	
	
	<%-- title --%>
	<acme:textbox code="educationRecord.title" path="title" />
	<br>
	
	<acme:textbox code="educationRecord.institution" path="institution" />
	<br>
	
	<%-- startMoment --%>
	<acme:textbox code="educationRecord.startMoment" path="startMoment" />
	<br>
	
	<%-- endMoment --%>
	<acme:textbox code="educationRecord.endMoment" path="endMoment" />
	<br>
	
	<%-- attachment --%>
	<acme:textbox code="educationRecord.attachment" path="attachment" />
	<br>
	
	
	<%-- comment --%>
	<acme:textbox code="educationRecord.comment" path="comment" />
	<br>
	
	<input type="submit" name="save"
		value="<spring:message code="educationRecord.save"/>"/>
	
	<acme:cancel code="educationRecord.cancel" url="curriculum/list.do" />
	
</form:form>

