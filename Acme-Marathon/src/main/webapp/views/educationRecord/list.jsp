<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="educationRecords" id="row" pagesize="5"
	requestURI="${requestUri}" class="displaytag">



	 <display:column>
              <a href="educationRecord/edit.do?educationRecordId=${row.id}">
                <spring:message code="educationRecord.edit"/>
              </a>
        </display:column>
        
        <display:column>
              <a href="educationRecord/show.do?educationRecordId=${row.id}">
                <spring:message code="educationRecord.display"/>
              </a>
        </display:column>
        
        

	<spring:message code="educationRecord.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" />
	
	<spring:message code="educationRecord.institution" var="institutionHeader" />
	<display:column property="institution" title="${institutionHeader}" />
	
	<spring:message code="educationRecord.startMoment" var="startMomentHeader" />
	<display:column property="startMoment" title="${startMomentHeader}" />
	
	<spring:message code="educationRecord.endMoment" var="endMomentHeader" />
	<display:column property="endMoment" title="${endMomentHeader}" />
	
	<spring:message code="educationRecord.comment" var="commentHeader" />
	<display:column property="comment" title="${commentHeader}" />
	
	<spring:message code="educationRecord.attachment" var="attachmentHeader" />
	<display:column property="attachment" title="${attachmentHeader}" />
	

		

</display:table>


