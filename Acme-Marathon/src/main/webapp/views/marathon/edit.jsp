<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="marathon/organizer/edit.do" modelAttribute="marathon">

	<%-- Hidden properties --%>
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="organizer" />
	<form:hidden path="ticker" />
	


    <%-- description--%>
    <acme:textbox code="marathon.description" path="description" />
    <br>
    
     <%-- address--%>
    <acme:textbox code="marathon.address" path="address" />
    <br>

	<%-- inscriptionStart --%>
	<form:label path="inscriptionStart"><spring:message code="marathon.inscriptionStart" /></form:label>
	<form:input path="inscriptionStart" placeholder="dd/mm/yyyy hh:mm" format="{0,date,dd/MM/yyyy HH:mm}" />
	<form:errors class="error" path="inscriptionStart" />
	<br><br>

	<%-- inscriptionEnd --%>
	<form:label path="inscriptionEnd"><spring:message code="marathon.inscriptionEnd" /></form:label>
	<form:input path="inscriptionEnd" placeholder="dd/mm/yyyy hh:mm" format="{0,date,dd/MM/yyyy HH:mm}" />
	<form:errors class="error" path="inscriptionEnd" />
	<br><br>
	
	<%-- marathonDate --%>
	<form:label path="marathonDate"><spring:message code="marathon.marathonDate" /></form:label>
	<form:input path="marathonDate" placeholder="dd/mm/yyyy hh:mm" format="{0,date,dd/MM/yyyy HH:mm}" />
	<form:errors class="error" path="marathonDate" />
	<br><br>


	<%-- category --%>
		<form:select path="category" >
	<form:options items="${categories}" itemLabel="name" />
	</form:select>	
	

	<%-- Buttons --%>
	<input type="submit" name="save"
		value="<spring:message code="marathon.save"/>"/>
	
	<acme:cancel code="marathon.cancel" url="marathon/organizer/listByOrganizer.do" />
</form:form>