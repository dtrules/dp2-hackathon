<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>



<display:table name="marathons" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

    <display:column>
        <a href="marathon/show.do?marathonId=${row.id}">
            <spring:message code="marathon.show"/>
        </a>
    </display:column>

    <display:column>
        <a href="organizer/show.do?organizerId=${row.organizer.id}">
            <spring:message code="marathon.organizer"/>
        </a>
    </display:column>

    <!-- Ticker -->
    <spring:message code="marathon.ticker" var="tickerHeader" />
    <display:column property="ticker" title="${tickerHeader}" />
	
	<!-- Address -->
	<spring:message code="marathon.address" var="addressHeader" />
	<display:column property="address" title="${addressHeader}" />

    <!-- Description -->
    <spring:message code="marathon.description" var="descriptionHeader" />
    <display:column property="description" title="${descriptionHeader}" />

    <!-- InscriptionStart -->
    <spring:message code="marathon.inscriptionStart" var="inscriptionStartHeader" />
    <display:column property="inscriptionStart" title="${inscriptionStartHeader}" />
    
    <!-- InscriptionEnd -->
    <spring:message code="marathon.inscriptionEnd" var="inscriptionEndHeader" />
    <display:column property="inscriptionEnd" title="${inscriptionEndHeader}" />
    
     <!-- MarathonDate -->
    <spring:message code="marathon.marathonDate" var="marathonDateHeader" />
    <display:column property="marathonDate" title="${marathonDateHeader}" />
    
      <!-- Category -->
    <spring:message code="marathon.category" var="categoryHeader" />
    <display:column property="category.name" title="${categoryHeader}" />
    
    <security:authorize access="hasRole('RUNNER')">
		<display:column titleKey="marathon.apply">
			<input type="submit" name="apply" value="<spring:message code="marathon.apply" />"
				onclick="javascript: relativeRedir('runnerApplication/create.do?marathonId=${row.id}');" />
		</display:column>
	</security:authorize>


</display:table>

