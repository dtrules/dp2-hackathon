<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<a title="Sponsorship" href="${sponsorship.targetPage}"><img src="${sponsorship.banner}" alt="Sponsorship" height = "150px" width = "500px"/></a>


<display:table name="marathon" id="row" requestURI="marathon/display.do" class="displaytag">

	<!-- organizer -->
	<spring:message code="marathon.organizer" var="organizerHeader" />
	<display:column property="organizer.name" title="${organizerHeader}" />

	<!-- Category -->
	<spring:message code="marathon.category" var="categoryHeader" />
	<display:column property="category" title="${categoryHeader}" />

	<!-- Ticker -->
	<spring:message code="marathon.ticker" var="tickerHeader" />
	<display:column property="ticker" title="${tickerHeader}" />


	<!-- Description -->
	<spring:message code="marathon.description" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}" />

	<!-- InscriptionStart -->
	<spring:message code="marathon.inscriptionStart" var="inscriptionStartHeader" />
	<display:column property="inscriptionStart" title="${inscriptionStartHeader}" />

	<!-- InscriptionEnd -->
	<spring:message code="marathon.inscriptionEnd" var="inscriptionEndHeader" />
	<display:column property="inscriptionEnd" title="${inscriptionEndHeader}" />

	<!-- MarathonDate -->
	<spring:message code="marathon.marathonDate" var="marathonDateHeader" />
	<display:column property="marathonDate" title="${marathonDateHeader}" />

	<!-- Address -->
	<spring:message code="marathon.address" var="addressHeader" />
	<display:column property="address" title="${addressHeader}" />

	
	
</display:table>




<security:authorize access="isAnonymous()">
	<acme:cancel code="marathon.cancel" url="/marathon/list.do" />
</security:authorize>

<security:authorize access="isAuthenticated()" >
	<acme:cancel code="marathon.cancel.list" url="/marathon/list.do" />
</security:authorize>

<security:authorize access="hasRole('ORGANIZER')">
	<acme:cancel code="marathon.cancel" url="/marathon/organizer/listByOrganizer.do" />
</security:authorize>
