<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="payment/organizer/save.do" modelAttribute="payment" method="post">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="ticker" />
	<form:hidden path="moment" />
	<form:hidden path="organizer" />


	<acme:textbox code="payment.money" path="money" />
	<br/>
	
	<jstl:if test='${payment.id == 0}'>
	
	<form:label path="worker">
	<spring:message code="payment.worker" />:
	</form:label>
	
	<form:select path="worker" >
	<form:options items="${workers}" itemLabel="name" />
	</form:select>	
	
	</jstl:if>
	
	<br />	
	<br />	

	<input type="submit" name="save" value="<spring:message code="payment.save"/>" />
    <acme:cancel code="payment.cancel" url="/payment/organizer/list.do" />


</form:form>