<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<display:table name="payments" id="row" pagesize="5" requestURI="${requestUri}" class="displaytag">
	
	<spring:message code="payment.ticker" var="ticker" />
	<display:column property="ticker" title="${ticker}" />
	
	<spring:message code="payment.money" var="money" />
	<display:column property="money" title="${money}" />
	
	<spring:message code="payment.moment" var="moment" />
	<display:column property="moment" title="${moment}" />
	
	<security:authorize access="hasRole('ORGANIZER')">
	<display:column titleKey="payment.show">
			<input type="submit" name="show" value="<spring:message code="payment.show" />"
				onclick="javascript: relativeRedir('payment/organizer/show.do?paymentId=${row.id}');" />
	</display:column>
	</security:authorize>
	


</display:table>

<br/>

<security:authorize access="hasRole('ORGANIZER')">
	<input type="submit" name="create"
		value="<spring:message code="payment.create" />"
		onclick="javascript: relativeRedir('payment/organizer/create.do');" />
</security:authorize>