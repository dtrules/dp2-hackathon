<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


	<fieldset>
	
	<b><spring:message code="payment.ticker"></spring:message>:</b><jstl:out value="${payment.ticker}"></jstl:out>
	<br />

	<b><spring:message code="payment.money"></spring:message>:</b><jstl:out value="${payment.money}"></jstl:out>
	<br />
	
	<b><spring:message code="payment.moment"></spring:message>:</b><jstl:out value="${payment.moment}"></jstl:out>
	<br />
	
	<b><spring:message code="payment.organizer"></spring:message>:</b><jstl:out value="${payment.organizer.name}"></jstl:out>
	<br />
	
	<b><spring:message code="payment.worker"></spring:message>:</b><jstl:out value="${payment.worker.name}"></jstl:out>
	<br />
	
	</fieldset>

	<br />
	
<!-- Cancel -->

<security:authorize access="hasRole('ORGANIZER')">	
				<button type="button" onclick="javascript: relativeRedir('payment/organizer/list.do')">
				<spring:message code="payment.cancel" />
				</button>
</security:authorize>