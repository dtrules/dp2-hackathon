<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="message/broadcast.do" modelAttribute="mess">

	<%-- Hidden properties --%>
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="folder" />
	<form:hidden path="sender" />
	<form:hidden path="moment" />
	<form:hidden path="receiver" />

	<%-- subject--%>
	<acme:textbox code="message.subject" path="subject" />
	<br>

	<%-- body--%>
	<acme:textarea code="message.body" path="body" />
	<br>

	<%-- priority --%>
	<form:label path="priority"><spring:message code="message.priority" /></form:label>
	<form:input path="priority" placeholder="LOW|NEUTRAL|HIGH" />
	<form:errors class="error" path="priority" />
	<br><br>

	<%-- tags--%>
	<acme:textbox code="message.tags" path="tags" />
	<br>



	<%-- Buttons --%>
	<input type="submit" name="save"
		value="<spring:message code="message.save"/>"/>
	
	<acme:cancel code="message.cancel" url="folder/actor/list.do" />
</form:form>