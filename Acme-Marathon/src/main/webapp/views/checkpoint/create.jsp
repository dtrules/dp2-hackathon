<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="checkpoint/organizer/create.do" modelAttribute="checkpoint" method="post">

	<form:hidden path="id" />
	<form:hidden path="version" />
	
	
	<spring:message code="checkpoint.remember.moment" />
	
	<br/>
			
			<br/>
			<br/>
			<br/>
	<acme:textbox code="checkpoint.number" path="number" />
	<br/>
	
<form:label path="moment"><spring:message code="checkpoint.moment" /></form:label>
	<form:input path="moment" placeholder="dd/mm/yyyy hh:mm" format="{0,date,dd/MM/yyyy HH:mm}" />
	<form:errors class="error" path="moment" />
	
		<br/>
			<br/>
	
	<form:label path="marathon">
	<spring:message code="checkpoint.marathon" />:
	</form:label>
	
	<form:select path="marathon" >
	<form:options items="${marathons}" itemLabel="description" />
	</form:select>	
	
	
	<br />	
	<br />	

	<input type="submit" name="save" value="<spring:message code="checkpoint.save"/>" />
    <acme:cancel code="checkpoint.cancel" url="/checkpoint/organizer/list.do" />


</form:form>