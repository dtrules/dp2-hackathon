<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<display:table name="checkpoints" id="row" pagesize="5" requestURI="${requestUri}" class="displaytag">
	
	
	   <display:column>
        <a href="checkpoint/organizer/show.do?checkpointId=${row.id}">
            <spring:message code="checkpoint.display"/>
        </a>
    </display:column>

	<spring:message code="checkpoint.number" var="number" />
	<display:column property="number" title="${number}" />
	
	<spring:message code="checkpoint.moment" var="moment" />
	<display:column property="moment" title="${moment}" />
	
	<spring:message code="checkpoint.marathon" var="marathon" />
	<display:column property="marathon.description" title="${marathon}" />
	
</display:table>


	<a href=checkpoint/organizer/create.do><spring:message code="checkpoint.create" /></a>
<br/>