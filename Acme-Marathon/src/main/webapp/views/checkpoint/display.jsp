<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="checkpoint" id="row" requestURI="checkpoint/organizer/show.do" class="displaytag">

	<!-- Number -->
	<spring:message code="checkpoint.number" var="numberHeader" />
	<display:column property="number" title="${numberHeader}" />

	<!-- Moment -->
	<spring:message code="checkpoint.moment" var="momentHeader" />
	<display:column property="moment" title="${momentHeader}" />

	<!-- Marathon -->
	<spring:message code="checkpoint.marathon" var="marathonHeader" />
	<display:column property="marathon.description" title="${marathonHeader}" />

	
</display:table>


<acme:cancel code="checkpoint.cancel" url="/checkpoint/organizer/list.do" />
