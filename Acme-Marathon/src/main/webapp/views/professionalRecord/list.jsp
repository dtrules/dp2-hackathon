<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="professionalRecords" id="row" pagesize="5"
	requestURI="${requestUri}" class="displaytag">



	 <display:column>
              <a href="professionalRecord/edit.do?professionalRecordId=${row.id}">
                <spring:message code="professionalRecord.edit"/>
              </a>
        </display:column>
        
        <display:column>
              <a href="professionalRecord/show.do?professionalRecordId=${row.id}">
                <spring:message code="professionalRecord.display"/>
              </a>
        </display:column>

	<spring:message code="professionalRecord.company" var="companyHeader" />
	<display:column property="company" title="${companyHeader}" />
	
	<spring:message code="professionalRecord.role" var="roleHeader" />
	<display:column property="role" title="${roleHeader}" />
	
	<spring:message code="professionalRecord.startMoment" var="startMomentHeader" />
	<display:column property="startMoment" title="${startMomentHeader}" />
	
	<spring:message code="professionalRecord.endMoment" var="endMomentHeader" />
	<display:column property="endMoment" title="${endMomentHeader}" />
	
	<spring:message code="professionalRecord.comment" var="commentHeader" />
	<display:column property="comment" title="${commentHeader}" />
	
	<spring:message code="professionalRecord.attachment" var="attachmentHeader" />
	<display:column property="attachment" title="${attachmentHeader}" />
	


</display:table>


		
