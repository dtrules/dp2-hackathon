<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="professionalRecord/edit.do" modelAttribute="professionalRecord">

	<form:hidden path="id" />
	<form:hidden path="version" />

<%-- title --%>
	<acme:textbox code="professionalRecord.company" path="company" />
	<br>
	
	<acme:textbox code="professionalRecord.role" path="role" />
	<br>
	
	<%-- startMoment --%>
	<acme:textbox code="professionalRecord.startMoment" path="startMoment" />
	<br>
	
	<%-- endMoment --%>
	<acme:textbox code="professionalRecord.endMoment" path="endMoment" />
	<br>
	
	<%-- attachment --%>
	<acme:textbox code="professionalRecord.attachment" path="attachment" />
	<br>
	
	
	<%-- comment --%>
	<acme:textbox code="professionalRecord.comment" path="comment" />
	<br>
	
	

	
	<input type="submit" name="save"
		value="<spring:message code="professionalRecord.save"/>"/>
		
		
	
	<acme:cancel code="professionalRecord.cancel" url="curriculum/list.do" />
	
</form:form>

