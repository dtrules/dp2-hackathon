<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="runnerApplication/edit.do" modelAttribute="runnerApplication">

	<%-- Hidden properties from RunnerApplication--%>
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="moment" />
	<form:hidden path="status" />
	<form:hidden path="price" />
	<form:hidden path="dorsalNumber" />
	<form:hidden path="runner" />
	<form:hidden path="marathon" />
	    	
	<!--Comments-->
	<form:label path="comments">
		<spring:message code="runnerApplication.comments"/>
	</form:label>
	<form:input path="comments" />
		<form:errors cssClass="error" path="comments" />
		
	<br />
	<br />
		
	<!--Position-->
	<jstl:if test="${runnerApplication.status == 'RUNNED'}">
	
	<form:label path="position">
		<spring:message code="runnerApplication.position"/>
	</form:label>
	<form:input path="position"/>
		<form:errors cssClass="error" path="position" />
	
	</jstl:if>
		
	<br />
	<br />
	
	<%-- Buttons --%>
	<input type="submit" name="save" value="<spring:message code="runnerApplication.save"/>"/>
	<acme:cancel code="runnerApplication.cancel" url="/runnerApplication/list.do" />
	
</form:form>