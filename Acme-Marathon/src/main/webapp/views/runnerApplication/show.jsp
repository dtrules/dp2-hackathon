<%--
 * show.jsp
 *
 * Copyright (C) 2019 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
	
<b><spring:message code="runnerApplication.moment"></spring:message>: </b><jstl:out value="${runnerApplication.moment}"></jstl:out>
<br />

<b><spring:message code="runnerApplication.status"></spring:message>: </b><jstl:out value="${runnerApplication.status}"></jstl:out>
<br />
	
<b><spring:message code="runnerApplication.price"></spring:message>: </b><jstl:out value="${runnerApplication.price}"></jstl:out>
<br />

<b><spring:message code="runnerApplication.comments"></spring:message>:</b><jstl:out value="${runnerApplication.comments}"></jstl:out>
<br />
	
<b><spring:message code="runnerApplication.dorsalNumber"></spring:message>: </b><jstl:out value="${runnerApplication.dorsalNumber}"></jstl:out>
<br />
	
<b><spring:message code="runnerApplication.shirtSize"></spring:message>: </b><jstl:out value="${runnerApplication.shirtSize}"></jstl:out>
<br />
	
<b><spring:message code="runnerApplication.position"></spring:message>: </b><jstl:out value="${runnerApplication.position}"></jstl:out>
<br />

<b><spring:message code="runnerApplication.runner"></spring:message>: </b><jstl:out value="${runnerApplication.runner.name}"></jstl:out>
<br />

<b><spring:message code="runnerApplication.marathon"></spring:message>: </b><jstl:out value="${runnerApplication.marathon.ticker}"></jstl:out>
<br />
<br />
	
<!-- Cancel -->

	<security:authorize access="hasRole('RUNNER')">	
		<button type="button" onclick="javascript: relativeRedir('runnerApplication/list.do')">
			<spring:message code="runnerApplication.cancel" />
		</button>
	</security:authorize>

	<security:authorize access="hasRole('ORGANIZER')">	
		<button type="button" onclick="javascript: relativeRedir('runnerApplication/list.do')">
			<spring:message code="runnerApplication.cancel" />
		</button>
	</security:authorize>