<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<security:authorize access="hasAnyRole('RUNNER','ORGANIZER')">

	<h2> <spring:message code="runnerApplication.pending"/> </h2>
	<display:table name="pendingApps" id="row" pagesize="5" requestURI="${requestUri}" class="displaytag">
		
		<spring:message code="runnerApplication.status" var="status" />
		<display:column property="status" title="${status}" />
		
		<spring:message code="runnerApplication.moment" var="moment" />
		<display:column property="moment" title="${moment}" />
		
		<spring:message code="runnerApplication.price" var="price" />
		<display:column property="price" title="${price}" />
		
		<spring:message code="runnerApplication.dorsalNumber" var="dorsalNumber" />
		<display:column property="dorsalNumber" title="${dorsalNumber}" />
		
		<spring:message code="runnerApplication.shirtSize" var="shirtSize" />
		<display:column property="shirtSize" title="${shirtSize}" />
	
		<display:column titleKey="runnerApplication.show">
			<input type="submit" name="show" value="<spring:message code="runnerApplication.show" />"
				onclick="javascript: relativeRedir('runnerApplication/show.do?runnerApplicationId=${row.id}');" />
		</display:column>
		
		<security:authorize access="hasRole('ORGANIZER')">
		<display:column titleKey="runnerApplication.accept">
			<jstl:if test="${row.status eq 'PENDING'}">
			<input type="submit" name="accept" value="<spring:message code="runnerApplication.accept" />"
				onclick="javascript: relativeRedir('runnerApplication/accept.do?runnerApplicationId=${row.id}');" />
			</jstl:if>
		</display:column>
		</security:authorize>
	
	<security:authorize access="hasRole('ORGANIZER')">
		<display:column titleKey="runnerApplication.reject">
			<jstl:if test="${row.status eq 'PENDING'}">
			<input type="submit" name="reject" value="<spring:message code="runnerApplication.reject" />"
				onclick="javascript: relativeRedir('runnerApplication/reject.do?runnerApplicationId=${row.id}');" />
			</jstl:if>
		</display:column>
	</security:authorize>
		
	</display:table>
	
</security:authorize>

<h2> <spring:message code="runnerApplication.accepted"/> </h2>
	<display:table name="acceptedApps" id="row" pagesize="5" requestURI="${requestUri}" class="displaytag">
		
		<spring:message code="runnerApplication.status" var="status" />
			<display:column property="status" title="${status}" />
			
			<spring:message code="runnerApplication.moment" var="moment" />
			<display:column property="moment" title="${moment}" />
			
			<spring:message code="runnerApplication.price" var="price" />
			<display:column property="price" title="${price}" />
			
			<spring:message code="runnerApplication.dorsalNumber" var="dorsalNumber" />
			<display:column property="dorsalNumber" title="${dorsalNumber}" />
			
			<spring:message code="runnerApplication.shirtSize" var="shirtSize" />
			<display:column property="shirtSize" title="${shirtSize}" />
		
			<display:column titleKey="runnerApplication.show">
				<input type="submit" name="show" value="<spring:message code="runnerApplication.show" />"
					onclick="javascript: relativeRedir('runnerApplication/show.do?runnerApplicationId=${row.id}');" />
			</display:column>
			
			<security:authorize access="hasRole('ORGANIZER')">
			<display:column titleKey="runnerApplication.run">
			<jstl:if test="${row.status eq 'ACCEPTED'}">
			<input type="submit" name="run" value="<spring:message code="runnerApplication.run" />"
				onclick="javascript: relativeRedir('runnerApplication/run.do?runnerApplicationId=${row.id}');" />
			</jstl:if>
			</display:column>
			</security:authorize>
		
	</display:table>
	
	<br/>
	
<h2> <spring:message code="runnerApplication.rejected"/> </h2>
	<display:table name="rejectedApps" id="row" pagesize="5" requestURI="${requestUri}" class="displaytag">
		
		<spring:message code="runnerApplication.status" var="status" />
			<display:column property="status" title="${status}" />
			
			<spring:message code="runnerApplication.moment" var="moment" />
			<display:column property="moment" title="${moment}" />
			
			<spring:message code="runnerApplication.price" var="price" />
			<display:column property="price" title="${price}" />
			
			<spring:message code="runnerApplication.dorsalNumber" var="dorsalNumber" />
			<display:column property="dorsalNumber" title="${dorsalNumber}" />
			
			<spring:message code="runnerApplication.shirtSize" var="shirtSize" />
			<display:column property="shirtSize" title="${shirtSize}" />
		
			<display:column titleKey="runnerApplication.show">
				<input type="submit" name="show" value="<spring:message code="runnerApplication.show" />"
					onclick="javascript: relativeRedir('runnerApplication/show.do?runnerApplicationId=${row.id}');" />
			</display:column>	
		
	</display:table>
	
	
<h2> <spring:message code="runnerApplication.runned"/> </h2>
	<display:table name="runnedApps" id="row" pagesize="5" requestURI="${requestUri}" class="displaytag">
		
		<spring:message code="runnerApplication.status" var="status" />
			<display:column property="status" title="${status}" />
			
			<spring:message code="runnerApplication.moment" var="moment" />
			<display:column property="moment" title="${moment}" />
			
			<spring:message code="runnerApplication.position" var="position" />
			<display:column property="position" title="${position}" />
			
			<spring:message code="runnerApplication.comments" var="comments" />
			<display:column property="comments" title="${comments}" />
		
			<display:column titleKey="runnerApplication.show">
				<input type="submit" name="show" value="<spring:message code="runnerApplication.show" />"
					onclick="javascript: relativeRedir('runnerApplication/show.do?runnerApplicationId=${row.id}');" />
			</display:column>	
		
		<security:authorize access="hasRole('ORGANIZER')">
			<display:column titleKey="runnerApplication.edit">
				<input type="submit" name="edit" value="<spring:message code="runnerApplication.edit" />"
					onclick="javascript: relativeRedir('runnerApplication/edit.do?runnerApplicationId=${row.id}');" />
			</display:column>
		</security:authorize>
		
	</display:table>