
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.RunnerApplication;

@Repository
public interface RunnerApplicationRepository extends JpaRepository<RunnerApplication, Integer> {

	@Query("select r from RunnerApplication r join r.runner ru where ru.id = ?1")
	public Collection<RunnerApplication> getRunnerApplicationsByRunner(int runnerId);
	
	@Query("select r from RunnerApplication r join r.runner ru where r.status = 'RUNNED' and ru.id = ?1")
	public Collection<RunnerApplication> getRunnerApplicationsRunnedByRunner(int runnerId);

	@Query("select r from RunnerApplication r join r.marathon m where m.id = ?1")
	public Collection<RunnerApplication> getRunnerApplicationsByMarathon(int marathonId);

	@Query("select r from RunnerApplication r join r.marathon m join m.organizer o where o.id = ?1")
	public Collection<RunnerApplication> getRunnerApplicationsByOrganizer(int organizerId);

}
