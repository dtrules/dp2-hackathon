
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Worker;

@Repository
public interface WorkerRepository extends JpaRepository<Worker, Integer> {

	//	@Query("select w from WorkerApplication wp join wp.worker w where wp join wp.marathon m join m.organizer o.id=?1")
	//	Collection<Worker> findWorkerByOrganizer(int organizerId);

	@Query("select w from Worker w where w.id = ?1")
	Worker findById(int id);

	@Query("select w from Worker w where w.userAccount.username = ?1")
	Worker findByUserName(String username);

	@Query("select w from Worker w where w.userAccount.id = ?1")
	Worker findByUserAccountId(int id);

}
