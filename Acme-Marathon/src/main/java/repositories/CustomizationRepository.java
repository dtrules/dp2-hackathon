
package repositories;

import domain.Customization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomizationRepository extends JpaRepository<Customization, Integer> {

    @Query("select c from Customization c")
    public Customization findCustomization();

}
