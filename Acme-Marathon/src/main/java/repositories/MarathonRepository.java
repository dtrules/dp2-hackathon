
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Marathon;

@Repository
public interface MarathonRepository extends JpaRepository<Marathon, Integer> {

	@Query("select m from Marathon m where m.organizer.id=?1")
	Collection<Marathon> findByOrganizer(int organizerId);

	@Query("select count(r) from RunnerApplication r where r.marathon.id = ?1")
	Integer numberOfRunnerApplicationsByMarathon(int marathonId);

	@Query("select m from Marathon m where m.category.id=?1")
	Collection<Marathon> findMarathonsByCategory(int categoryId);
}
