
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Actor;
import domain.Administrator;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {

	@Query("select admin from Administrator admin where admin.userAccount.id = ?1")
	Administrator findByUserAccountId(int id);

	@Query("select a from Actor a where a.isSuspicious = true")
	Collection<Actor> findSuspicious();

	//@Query("select 1.0*count(f)/(select count(f) from Finder f where f.positions.size = 0) from Finder f where f.positions.size > 0")
	//	Double ratioFinders();
	//	@Query("select avg(1.0*(select count(i.provider) from Item i where i.provider.id=p.id)) from Provider p")
	//	Double avgItemsProvider();

	//	@Query("select c from Company c")
	//	Collection<Company> getCompaniesWithMorePositions();

	//	@Query("select avg(a.score) from Audit a")
	//	Double avgAuditScore();
	//
	//	@Query("select max(a.score) from Audit a")
	//	int maxAuditScore();
	//
	//	@Query("select min(a.score) from Audit a")
	//	int minAuditScore();
	//
	//	@Query("select min(1.0 * (select avg(0.1 * (select avg(a.score) from Audit a where a.position.id = p.id and a.position.company.id = c.id)) from Position p)) from Company c")
	//	double getMinimumScoreCompany();
	//
	//	@Query("select max(1.0 * (select avg(0.1 * (select avg(a.score) from Audit a where a.position.id = p.id and a.position.company.id = c.id)) from Position p)) from Company c")
	//	double getMaximumScoreCompany();
	//
	//	@Query("select avg(1.0 * (select avg(0.1 * (select avg(a.score) from Audit a where a.position.id = p.id and a.position.company.id = c.id)) from Position p)) from Company c")
	//	double getAverageScoreCompany();
	//
	//	@Query("select stddev(1.0 * (select avg(0.1 * (select avg(a.score) from Audit a where a.position.id = p.id and a.position.company.id = c.id)) from Position p)) from Company c")
	//	double getStandardDeviationScoreCompany();
	//
	//	@Query("select stddev(a.score) from Audit a")
	//	Double stddevAuditScore();
	//	//-------------------------------------------------------------------------
	//
	//	@Query("select avg(1.0*(select count(i.provider) from Item i where i.provider.id=p.id)) from Provider p")
	//	Double avgItemsProvider();
	//
	//	@Query("select min(1.0*(select count(i.provider) from Item i where i.provider.id=p.id)) from Provider p")
	//	Double minItemsProvider();
	//
	//	@Query("select max(1.0*(select count(i.provider) from Item i where i.provider.id=p.id)) from Provider p")
	//	Double maxItemsProvider();
	//
	//	@Query("select stddev(1.0*(select count(i.provider) from Item i where i.provider.id=p.id)) from Provider p")
	//	Double stddevItemsProvider();
	//	//-----------------------------------------------------------
	//
	//	@Query("select avg(h.curriculas.size) from Rookie h")
	//	Double avgCurriculas();
	//
	//	@Query("select max(h.curriculas.size) from Rookie h")
	//	int maxCurriculas();
	//
	//	@Query("select min(h.curriculas.size) from Rookie h")
	//	int minCurriculas();
	//
	//	@Query("select stddev(h.curriculas.size) from Rookie h")
	//	Double stddevCurriculas();
	//
	//	@Query("select avg(f.positions.size) from Finder f")
	//	Double avgResults();
	//
	//	@Query("select max(f.positions.size) from Finder f")
	//	int maxResults();
	//
	//	@Query("select min(f.positions.size) from Finder f")
	//	int minResults();
	//
	//	@Query("select stddev(f.positions.size) from Finder f")
	//	Double stddevResults();
	//
	//	
	//
	//	@Query("select max(1.0*(select count(p) from Position p where p.company.id = c.id)) from Company c")
	//	Double maxPositions();
	//
	//	@Query("select avg(1.0*(select count(p) from Position p where p.company.id = c.id)) from Company c")
	//	Double avgPositions();
	//
	//	@Query("select min(1.0*(select count(p) from Position p where p.company.id = c.id)) from Company c")
	//	Double minPositions();
	//
	//	@Query("select stddev(1.0*(select count(p) from Position p where p.company.id = c.id)) from Company c")
	//	Double stddevPositions();
	//
	//	//Administrator applications per rookie dashboard data
	//
	//	@Query("select max(1.0*(select count(a) from Application a where a.rookie.id = h.id)) from Rookie h")
	//	Double maxAppRookie();
	//
	//	@Query("select min(1.0*(select count(a) from Application a where a.rookie.id = h.id)) from Rookie h")
	//	Double minAppRookie();
	//
	//	@Query("select avg(1.0*(select count(a) from Application a where a.rookie.id = h.id)) from Rookie h")
	//	Double avgAppRookie();
	//
	//	@Query("select stddev(1.0*(select count(a) from Application a where a.rookie.id = h.id)) from Rookie h")
	//	Double stddevAppRookie();
	//
	//	//	//Best and worst positions in terms of salary
	//	//	@Query("select p from Position p where p.salaryOffered = max(p.salaryOffered) from Position")
	//
	//	//-----------NIVEL A--------------
	//
	//	@Query("select avg(1.0*(select count(s) from Sponsorship s where s.provider.id = p.id)) from Provider p")
	//	double avgSponsorshipProvider();
	//
	//	@Query("select max(1.0*(select count(s) from Sponsorship s where s.provider.id = p.id)) from Provider p")
	//	int maxSponsorshipProvider();
	//
	//	@Query("select min(1.0*(select count(s) from Sponsorship s where s.provider.id = p.id)) from Provider p")
	//	int minSponsorshipProvider();
	//
	//	@Query("select stddev(1.0*(select count(s) from Sponsorship s where s.provider.id = p.id)) from Provider p")
	//	double stddevSponsorshipProvider();
	//
	//	@Query("select avg(1.0*(select count(s) from Sponsorship s where s.position.id = p.id)) from Position p")
	//	double avgSponsorshipPosition();
	//
	//	@Query("select max(1.0*(select count(s) from Sponsorship s where s.position.id = p.id)) from Position p")
	//	int maxSponsorshipPosition();
	//
	//	@Query("select min(1.0*(select count(s) from Sponsorship s where s.position.id = p.id)) from Position p")
	//	int minSponsorshipPosition();
	//
	//	@Query("select stddev(1.0*(select count(s) from Sponsorship s where s.position.id = p.id)) from Position p")
	//	double stddevSponsorshipPosition();
	//	

	//DASHBOARD ACME MARATHON

	//NIVEL C

	@Query("select avg(1.0 *(select count(a) from RunnerApplication a where a.runner.id=r.id and a.status='ACCEPTED'))from Runner r")
	Double avgMarathonsRunner();

	@Query("select max(1.0 *(select count(a) from RunnerApplication a where a.runner.id=r.id and a.status='ACCEPTED'))from Runner r")
	int maxMarathonsRunner();

	@Query("select min(1.0 *(select count(a) from RunnerApplication a where a.runner.id=r.id and a.status='ACCEPTED'))from Runner r")
	int minMarathonsRunner();

	@Query("select stddev(1.0 *(select count(a) from RunnerApplication a where a.runner.id=r.id and a.status='ACCEPTED'))from Runner r")
	Double stddevMarathonsRunner();

	@Query("select avg(1.0 *(select count(m) from Marathon m where m.organizer.id = o.id)) from Organizer o")
	Double avgMarathonsOrganizer();

	@Query("select max(1.0 *(select count(m) from Marathon m where m.organizer.id = o.id)) from Organizer o")
	Double maxMarathonsOrganizer();

	@Query("select min(1.0 *(select count(m) from Marathon m where m.organizer.id = o.id)) from Organizer o")
	Double minMarathonsOrganizer();

	@Query("select stddev(1.0 *(select count(m) from Marathon m where m.organizer.id = o.id)) from Organizer o")
	Double stddevMarathonsOrganizer();

	@Query("select avg(1.0*(select count(a) from RunnerApplication a where a.marathon.id = m.id))from Marathon m")
	Double avgApplicationsMarathon();

	@Query("select max(1.0*(select count(a) from RunnerApplication a where a.marathon.id = m.id))from Marathon m")
	Double maxApplicationsMarathon();

	@Query("select min(1.0*(select count(a) from RunnerApplication a where a.marathon.id = m.id))from Marathon m")
	Double minApplicationsMarathon();

	@Query("select stddev(1.0*(select count(a) from RunnerApplication a where a.marathon.id = m.id))from Marathon m")
	Double stddevApplicationsMarathon();

	@Query("select avg(1.0*(select max(a.price) from RunnerApplication a where a.marathon.id = m.id)) from Marathon m")
	Double avgPriceMarathon();

	@Query("select max(1.0*(select max(a.price) from RunnerApplication a where a.marathon.id = m.id)) from Marathon m")
	Double maxPriceMarathon();

	@Query("select min(1.0*(select max(a.price) from RunnerApplication a where a.marathon.id = m.id)) from Marathon m")
	Double minPriceMarathon();

	@Query("select stddev(1.0*(select max(a.price) from RunnerApplication a where a.marathon.id = m.id)) from Marathon m")
	Double stddevPriceMarathon();

	@Query("select count(a)/(select count(a) from RunnerApplication a) from RunnerApplication a where a.status='PENDING')")
	Double ratioPending();

	@Query("select count(a)/(select count(a) from RunnerApplication a) from RunnerApplication a where a.status='ACCEPTED')")
	Double ratioAccepted();

	@Query("select count(a)/(select count(a) from RunnerApplication a) from RunnerApplication a where a.status='REJECTED')")
	Double ratioRejected();

	//NIVEL B

	@Query("select avg(1.0*(select count(c) from Complaint c where c.marathon.id = m.id)) from Marathon m")
	Double avgComplaintsMarathon();

	@Query("select max(1.0*(select count(c) from Complaint c where c.marathon.id = m.id)) from Marathon m")
	int maxComplaintsMarathon();

	@Query("select min(1.0*(select count(c) from Complaint c where c.marathon.id = m.id)) from Marathon m")
	int minComplaintsMarathon();

	@Query("select stddev(1.0*(select count(c) from Complaint c where c.marathon.id = m.id)) from Marathon m")
	Double stddevComplaintsMarathon();

	@Query("select avg(1.0*(select count(c) from Complaint c where c.runner.id = r.id)) from Runner r")
	Double avgComplaintsRunner();

	@Query("select max(1.0*(select count(c) from Complaint c where c.runner.id = r.id)) from Runner r")
	int maxComplaintsRunner();

	@Query("select min(1.0*(select count(c) from Complaint c where c.runner.id = r.id)) from Runner r")
	int minComplaintsRunner();

	@Query("select stddev(1.0*(select count(c) from Complaint c where c.runner.id = r.id)) from Runner r")
	Double stddevComplaintsRunner();

	@Query("select r, (select count(c) from Complaint c where c.runner.id = r.id) as cuenta from Runner r order by cuenta desc")
	Collection<Object> top3RunnersTermsOfComplaints();

	@Query("select o, (select count(c) from Complaint c join c.marathon m where m.organizer.id = o.id) as cuenta from Organizer o order by cuenta desc")
	Collection<Object> top3OrganizersTermsOfComplaints();

	@Query("select count(m1)*1.0 /(select count(m) from Marathon m) from Marathon m1 where (select count(c) from Complaint c where c.marathon.id = m1.id) > 0")
	Double ratioMarathonsComplaint();

	//NIVEL A

	@Query("select m, (select count(s) from Sponsorship s where s.marathon.id = m.id) as cuenta from Marathon m order by cuenta desc")
	Collection<Object> top5MarathonsTermsOfSponsorships();

	@Query("select avg(1.0*(select count(s) from Sponsorship s where s.sponsor.id = sp.id)) from Sponsor sp")
	Double avgSponsorshipsSponsor();

	@Query("select max(1.0*(select count(s) from Sponsorship s where s.sponsor.id = sp.id)) from Sponsor sp")
	int maxSponsorshipsSponsor();

	@Query("select min(1.0*(select count(s) from Sponsorship s where s.sponsor.id = sp.id)) from Sponsor sp")
	int minavgSponsorshipsSponsor();

	@Query("select stddev(1.0*(select count(s) from Sponsorship s where s.sponsor.id = sp.id)) from Sponsor sp")
	Double stddevavgSponsorshipsSponsor();

	@Query("select sp, (select count(s) from Sponsorship s where s.sponsor.id = sp.id) as cuenta from Sponsor sp order by cuenta desc")
	Collection<Object> top5SponsorsTermsOfSponsorships();

}
