
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Curriculum;

@Repository
public interface CurriculumRepository extends JpaRepository<Curriculum, Integer> {

	@Query("select c from Curriculum c join c.professionalRecords p where p.id = ?1")
	Curriculum findCurriculumByProfessionalRecordId(int professionalRecordId);

	@Query("select c from Curriculum c where c.personalRecord.id = ?1")
	Curriculum findCurriculumByPersonalRecordId(int personalRecordId);

	@Query("select c from Curriculum c join c.educationRecords p where p.id = ?1")
	Curriculum findCurriculumByEducationRecordId(int educationRecordId);

	@Query("select c from Curriculum c join c.worker r where r.id = ?1")
	Collection<Curriculum> findCurriculumByWorker(int workerId);
}
