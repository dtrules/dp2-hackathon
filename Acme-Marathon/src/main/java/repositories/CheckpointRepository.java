
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Checkpoint;

@Repository
public interface CheckpointRepository extends JpaRepository<Checkpoint, Integer> {

	@Query("select c from Checkpoint c where c.marathon.id = ?1")
	Collection<Checkpoint> findByMarathonId(int marathonId);
}
