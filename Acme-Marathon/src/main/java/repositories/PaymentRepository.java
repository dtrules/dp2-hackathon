
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Payment;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Integer> {
	
	@Query("select p from Payment p join p.organizer o where o.id=?1")
	Collection<Payment> findPaymentByOrganizer (int organizerId);
	
	@Query("select p from Payment p join p.worker w where w.id=?1")
	Collection<Payment> findPaymentByWorker (int workerId);


}
