
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Organizer;

@Repository
public interface OrganizerRepository extends JpaRepository<Organizer, Integer> {

	@Query("select r from Organizer r where r.id = ?1")
	Organizer findById(int id);

	@Query("select r from Organizer r where r.userAccount.username = ?1")
	Organizer findByUserName(String username);

	@Query("select r from Organizer r where r.userAccount.id = ?1")
	Organizer findByUserAccountId(int id);

}
