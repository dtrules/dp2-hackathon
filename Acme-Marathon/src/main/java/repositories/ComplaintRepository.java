
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Complaint;

@Repository
public interface ComplaintRepository extends JpaRepository<Complaint, Integer> {

	@Query("select c from Complaint c join c.runner r where r.id=?1")
	Collection<Complaint> findComplaintByRunner(int runnerId);
	
	@Query("select c from Complaint c join c.marathon m where m.id=?1")
	Collection<Complaint> findComplaintByMarathon(int marathonId);
	
}