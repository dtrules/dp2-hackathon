
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.WorkerApplication;

@Repository
public interface WorkerApplicationRepository extends JpaRepository<WorkerApplication, Integer> {

	@Query("select app from WorkerApplication app join app.worker w where w.id = ?1")
	public Collection<WorkerApplication> getWorkerApplicationsByWorker(int workerId);

	@Query("select app from WorkerApplication app join app.marathon m where m.id = ?1")
	public Collection<WorkerApplication> getWorkerApplicationsByMarathon(int marathonId);

	@Query("select r from WorkerApplication r join r.marathon m join m.organizer o where o.id = ?1")
	public Collection<WorkerApplication> getWorkerApplicationsByOrganizer(int organizerId);

}
