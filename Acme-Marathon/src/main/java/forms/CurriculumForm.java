
package forms;

import domain.PersonalRecord;

public class CurriculumForm {

	//Personal Record
	private PersonalRecord	personalRecord;


	public PersonalRecord getPersonalRecord() {
		return this.personalRecord;
	}

	public void setPersonalRecord(final PersonalRecord personalRecord) {
		this.personalRecord = personalRecord;
	}

}
