
package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class WorkerApplication extends DomainEntity {

	// Attributes ---------------------------------------------------------------------------
	private Date		moment;
	private String		status;
	private Integer		hours;
	private String		comment;

	private Worker		worker;
	private Marathon	marathon;


	// Getters & setters ---------------------------------------------------------------------
	@Past
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getMoment() {
		return this.moment;
	}

	public void setMoment(final Date moment) {
		this.moment = moment;
	}

	@NotBlank
	@Pattern(regexp = "^PENDING|ACCEPTED|REJECTED$")
	public String getStatus() {
		return this.status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	@SafeHtml
	public String getComment() {
		return this.comment;
	}

	public void setComment(final String comment) {
		this.comment = comment;
	}

	@Min(0)
	@NotNull
	public Integer getHours() {
		return this.hours;
	}

	public void setHours(final Integer hours) {
		this.hours = hours;
	}

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Worker getWorker() {
		return this.worker;
	}

	public void setWorker(final Worker worker) {
		this.worker = worker;
	}

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Marathon getMarathon() {
		return this.marathon;
	}

	public void setMarathon(final Marathon marathon) {
		this.marathon = marathon;
	}

}
