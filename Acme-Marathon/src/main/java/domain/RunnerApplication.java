
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class RunnerApplication extends DomainEntity {

	// Attributes ---------------------------------------------------------------------------
	private Date				moment;
	private String				status;
	private Double				price;
	private Collection<String>	comments;
	private Integer				dorsalNumber;
	private String				shirtSize;
	private Integer				position;

	private Runner				runner;
	private Marathon			marathon;


	// Getters & setters ---------------------------------------------------------------------
	@Past
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getMoment() {
		return this.moment;
	}

	public void setMoment(final Date moment) {
		this.moment = moment;
	}

	@NotNull
	@Pattern(regexp = "^PENDING|ACCEPTED|REJECTED|RUNNED")
	public String getStatus() {
		return this.status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	@NotNull
	@Min(0)
	public Double getPrice() {
		return this.price;
	}

	public void setPrice(final Double price) {
		this.price = price;
	}

	
	@ElementCollection
	public Collection<String> getComments() {
		return this.comments;
	}

	public void setComments(final Collection<String> comments) {
		this.comments = comments;
	}

	@Min(0)
	public Integer getDorsalNumber() {
		return this.dorsalNumber;
	}

	public void setDorsalNumber(final Integer dorsalNumber) {
		this.dorsalNumber = dorsalNumber;
	}

	@Pattern(regexp = "^XXS|XS|S|M|L|XL|XXL")
	public String getShirtSize() {
		return this.shirtSize;
	}

	public void setShirtSize(final String shirtSize) {
		this.shirtSize = shirtSize;
	}

	@Min(1)
	public Integer getPosition() {
		return this.position;
	}

	public void setPosition(final Integer position) {
		this.position = position;
	}

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Runner getRunner() {
		return this.runner;
	}

	public void setRunner(final Runner runner) {
		this.runner = runner;
	}

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Marathon getMarathon() {
		return this.marathon;
	}

	public void setMarathon(final Marathon marathon) {
		this.marathon = marathon;
	}

}
