
package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Marathon extends DomainEntity {

	// Attributes

	private String		ticker;
	private Date		inscriptionStart;
	private Date		inscriptionEnd;
	private Date		marathonDate;
	private String		address;
	private String		description;

	//Relationships

	private Organizer	organizer;
	private Category	category;


	@Column(unique = true)
	@NotBlank
	@Pattern(regexp = "^\\d{6}-\\w{6}$")
	public String getTicker() {
		return this.ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getInscriptionStart() {
		return this.inscriptionStart;
	}

	public void setInscriptionStart(Date inscriptionStart) {
		this.inscriptionStart = inscriptionStart;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getInscriptionEnd() {
		return this.inscriptionEnd;
	}

	public void setInscriptionEnd(Date inscriptionEnd) {
		this.inscriptionEnd = inscriptionEnd;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getMarathonDate() {
		return this.marathonDate;
	}

	public void setMarathonDate(Date marathonDate) {
		this.marathonDate = marathonDate;
	}
	@NotBlank
	@SafeHtml
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@NotBlank
	@SafeHtml
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@NotNull
	@ManyToOne(optional = false)
	public Organizer getOrganizer() {
		return this.organizer;
	}

	public void setOrganizer(Organizer organizer) {
		this.organizer = organizer;
	}

	@NotNull
	@ManyToOne(optional = false)
	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}
