
package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Customization extends DomainEntity {

	// Attributes -------------------------------------------------------------
	private String				systemName;
	private String				welcomeMessageEn;
	private String				welcomeMessageEs;
	private String				countryCode;
	private double				vat;
	private Collection<String>	brandNames;
	private Collection<String>	spamWords;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getSystemName() {
		return this.systemName;
	}

	public void setSystemName(final String systemName) {
		this.systemName = systemName;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getWelcomeMessageEn() {
		return this.welcomeMessageEn;
	}

	public void setWelcomeMessageEn(final String welcomeMessageEn) {
		this.welcomeMessageEn = welcomeMessageEn;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getWelcomeMessageEs() {
		return this.welcomeMessageEs;
	}

	public void setWelcomeMessageEs(final String welcomeMessageEs) {
		this.welcomeMessageEs = welcomeMessageEs;
	}

	@NotBlank
	@Pattern(regexp = "([+]?\\d{1,2})")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(final String countryCode) {
		this.countryCode = countryCode;
	}

	public double getVat(){
		return this.vat;
	}

	public void setVat(double vat){
		this.vat=vat;
	}

	@NotNull
	@NotEmpty
	@ElementCollection
	public Collection<String> getBrandNames() {
		return this.brandNames;
	}

	public void setBrandNames(final Collection<String> brandNames) {
		this.brandNames = brandNames;
	}

	@NotNull
	@NotEmpty
	@ElementCollection
	public Collection<String> getSpamWords() {
		return this.spamWords;
	}

	public void setSpamWords(final Collection<String> spamWords) {
		this.spamWords = spamWords;
	}

}
