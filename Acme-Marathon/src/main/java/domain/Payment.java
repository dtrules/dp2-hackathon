
package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Payment extends DomainEntity {

	// Attributes

	private String		ticker;
	private Date		moment;
	private Double		money;
	//relationship
	private Organizer	organizer;
	private Worker		worker;


	// Getters & setters

	@Column(unique = true)
	@NotBlank
	@Pattern(regexp = "^\\d{6}-\\w{6}$")
	public String getTicker() {
		return this.ticker;
	}

	public void setTicker(final String ticker) {
		this.ticker = ticker;
	}

	@Past
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getMoment() {
		return this.moment;
	}

	public void setMoment(final Date moment) {
		this.moment = moment;
	}

	@NotNull
	public Double getMoney() {
		return this.money;
	}

	public void setMoney(final Double money) {
		this.money = money;
	}

	// Relationships ----------------------------------------------------------

	@ManyToOne(optional = false)
	@NotNull
	@Valid
	public Organizer getOrganizer() {
		return this.organizer;
	}

	public void setOrganizer(final Organizer org) {
		this.organizer = org;
	}

	@ManyToOne(optional = false)
	@NotNull
	@Valid
	public Worker getWorker() {
		return this.worker;
	}

	public void setWorker(final Worker worker) {
		this.worker = worker;
	}

}
