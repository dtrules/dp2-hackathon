
package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.CurriculumService;
import services.WorkerService;
import domain.Curriculum;
import domain.Worker;
import forms.CurriculumForm;

@Controller
@RequestMapping("/curriculum")
public class CurriculumController extends AbstractController {

	@Autowired
	private WorkerService		workerService;

	@Autowired
	private CurriculumService	curriculumService;


	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView res;

		final Worker worker = this.workerService.findByPrincipal();

		final Collection<Curriculum> curriculums = this.curriculumService.findCurriculumByWorker(worker.getId());
		Boolean tieneCurriculum = true;
		if (curriculums.isEmpty())
			tieneCurriculum = false;

		res = new ModelAndView("curriculum/list");
		res.addObject("curriculums", curriculums);
		res.addObject("tieneCurriculum", tieneCurriculum);
		res.addObject("requestURI", "curriculum/list.do");

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView createCurriculum() {
		ModelAndView result;

		final CurriculumForm form = new CurriculumForm();

		result = this.createEditModelAndView(form);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int curriculumId) {
		ModelAndView res;
		Curriculum curriculum;

		curriculum = this.curriculumService.findOneToEdit(curriculumId);
		res = this.createEditModelAndView(curriculum);

		res.addObject("curriculum", curriculum);

		return res;
	}

	//	@RequestMapping(value = "/show", method = RequestMethod.GET)
	//	public ModelAndView show() {
	//		ModelAndView result;
	//		Worker worker = this.workerService.findByPrincipal();
	//		Collection<Curriculum> curriculum = this.curriculumService.findCurriculumByWorker(worker.getId());
	//
	//		result = new ModelAndView("curriculum/show");
	//		result.addObject("personalRecord", curriculum.getPersonalRecord());
	//		result.addObject("educationRecords", curriculum.getEducationRecords());
	//		result.addObject("professionalRecords", curriculum.getProfessionalRecords());
	//		result.addObject("curriculum", curriculum);
	//
	//		return result;
	//	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int curriculumId) {
		ModelAndView result;

		Curriculum res;

		res = this.curriculumService.findOne(curriculumId);

		this.curriculumService.delete(res);
		result = new ModelAndView("redirect:/curriculum/list.do");

		return result;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute("form") @Valid final CurriculumForm form, final BindingResult binding) {

		ModelAndView res;
		Curriculum c;

		c = this.curriculumService.reconstruct(form, binding);

		if (binding.hasErrors()) {
			System.out.println(binding.getAllErrors());
			res = this.createEditModelAndView(c);
		} else
			try {
				this.curriculumService.save(c);
				res = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				res = this.createEditModelAndView(form, "curriculum.commit.error");

			}

		return res;
	}

	protected ModelAndView createEditModelAndView(final Curriculum sp) {
		ModelAndView res;

		res = this.createEditModelAndView(sp, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(final Curriculum sp, final String messageCode) {
		ModelAndView res;

		if (sp.getId() == 0)
			res = new ModelAndView("curriculum/create");
		else
			res = new ModelAndView("curriculum/edit");
		res.addObject("curriculum", sp);
		res.addObject("message", messageCode);

		return res;
	}

	private ModelAndView createEditModelAndView(final CurriculumForm form) {
		return this.createEditModelAndView(form, null);
	}

	private ModelAndView createEditModelAndView(final CurriculumForm form, final String message) {
		ModelAndView result;

		result = new ModelAndView("curriculum/create");
		result.addObject("form", form);
		result.addObject("message", message);

		return result;
	}

}
