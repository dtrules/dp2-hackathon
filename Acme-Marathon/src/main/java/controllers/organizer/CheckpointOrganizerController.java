
package controllers.organizer;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.CheckpointService;
import services.MarathonService;
import services.OrganizerService;
import controllers.AbstractController;
import domain.Checkpoint;
import domain.Marathon;
import domain.Organizer;

@Controller
@RequestMapping("/checkpoint/organizer")
public class CheckpointOrganizerController extends AbstractController {

	@Autowired
	private CheckpointService	checkpointService;

	@Autowired
	private OrganizerService	organizerService;

	@Autowired
	private ActorService		actorService;

	@Autowired
	private MarathonService		marathonService;


	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView res;

		final Organizer principal = this.organizerService.findByPrincipal();

		final Collection<Checkpoint> checkpoints = this.checkpointService.findByOrganizer(principal.getId());

		res = new ModelAndView("checkpoint/organizer/list");
		res.addObject("checkpoints", checkpoints);
		res.addObject("requestURI", "checkpoint/organizer/list.do");

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;
		Checkpoint p;

		p = this.checkpointService.create();
		res = this.createEditModelAndView(p);

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Checkpoint checkpoint, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(checkpoint);
		//System.out.println(binding.getAllErrors());
		else
			try {

				this.checkpointService.save(checkpoint);

				result = new ModelAndView("redirect:/checkpoint/organizer/list.do");

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.createEditModelAndView(checkpoint, "checkpoint.commit.error");
			}

		return result;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final int checkpointId) {
		ModelAndView result;
		Checkpoint p;
		p = this.checkpointService.findOne(checkpointId);

		result = new ModelAndView("checkpoint/organizer/show");
		result.addObject("checkpoint", p);
		result.addObject("checkpointId", checkpointId);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Checkpoint p) {
		ModelAndView res;

		res = this.createEditModelAndView(p, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(final Checkpoint p, final String messageCode) {
		ModelAndView res;

		final Collection<Marathon> marathons = this.marathonService.listByOrganizer(this.actorService.findByPrincipal().getId());

		res = new ModelAndView("checkpoint/organizer/create");

		res.addObject("checkpoint", p);
		res.addObject("message", messageCode);
		res.addObject("marathons", marathons);

		return res;
	}

}
