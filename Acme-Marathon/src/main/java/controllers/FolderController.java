
package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.FolderService;
import domain.Actor;
import domain.Folder;

@Controller
@RequestMapping("/folder/actor")
public class FolderController extends AbstractController {

	//Services
	@Autowired
	FolderService	folderService;

	@Autowired
	ActorService	actorService;


	//List-------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final ModelAndView res;
		final Actor actor = this.actorService.findByPrincipal();

		final Collection<Folder> folders = this.folderService.findAllFoldersByActor(actor.getId());
		res = new ModelAndView("folder/actor/list");
		res.addObject("folders", folders);
		res.addObject("requestURI", "folder/actor/list.do");
		return res;
	}

	//Create--------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final Folder folder = this.folderService.create();
		res = this.createModelAndView(folder);

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView create(@Valid final Folder folder, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors())
			res = this.createModelAndView(folder);
		else
			try {
				this.folderService.save(folder);

				res = new ModelAndView("redirect:/folder/actor/list.do");
			} catch (final Throwable error) {
				res = this.createModelAndView(folder, "folder.error.save");

			}
		return res;

	}

	//Edit-------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int folderId) {
		final ModelAndView res;

		final Folder folder = this.folderService.findOneToEdit(folderId);

		Assert.notNull(folder);
		res = this.editModelAndView(folder);

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Folder folder, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.editModelAndView(folder);

			System.out.println(binding.hasErrors());
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.folderService.save(folder);
				res = new ModelAndView("redirect:/folder/actor/list.do");
				res.addObject("folder", folder);

			} catch (final Throwable error) {
				res = this.editModelAndView(folder, "folder.error.save");
			}
		return res;

	}
	//Delete---------------------------------------------

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int folderId) {
		ModelAndView result;

		final Folder folder = this.folderService.findOneToEdit(folderId);

		try {

			this.folderService.delete(folder);
		} catch (final Throwable th) {

		}

		result = new ModelAndView("redirect:/folder/actor/list.do");

		return result;
	}

	//Ancillary methods ----------------------------------------------------

	private ModelAndView editModelAndView(final Folder folder) {

		return this.editModelAndView(folder, null);
	}

	private ModelAndView editModelAndView(final Folder folder, final String message) {
		ModelAndView res;

		final boolean systemFolder = this.folderService.checkIfSystemFolder(folder);

		res = new ModelAndView("folder/actor/edit");
		res.addObject("folder", folder);
		res.addObject("message", message);
		res.addObject("systemFolder", systemFolder);

		return res;
	}

	private ModelAndView createModelAndView(final Folder folder) {

		return this.createModelAndView(folder, null);
	}

	private ModelAndView createModelAndView(final Folder folder, final String message) {
		ModelAndView res;
		res = new ModelAndView("folder/actor/create");
		res.addObject("folder", folder);
		res.addObject("message", message);

		return res;
	}

}
