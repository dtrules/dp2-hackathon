
package controllers;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.UserAccountRepository;
import services.CreditCardService;
import services.CustomizationService;
import services.SponsorService;
import domain.CreditCard;
import domain.Customization;
import domain.Sponsor;
import forms.SponsorForm;

@Controller
@RequestMapping("/sponsor")
public class SponsorController extends AbstractController {

	@Autowired
	private SponsorService			sponsorService;

	@Autowired
	private CustomizationService	customizationService;

	@Autowired
	private CreditCardService		creditCardService;

	@Autowired
	private UserAccountRepository	userAccountRepository;


	@ExceptionHandler(TypeMismatchException.class)
	public ModelAndView handleMismatchException(final TypeMismatchException oops) {
		return new ModelAndView("redirect:/");
	}

	// List ----------------------------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Sponsor> sponsors;

		try {
			sponsors = this.sponsorService.findAll();
			result = new ModelAndView("sponsor/list");
			result.addObject("sponsors", sponsors);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final int sponsorId) {
		ModelAndView result;
		Sponsor p;
		p = this.sponsorService.findOne(sponsorId);

		result = new ModelAndView("sponsor/show");
		result.addObject("sponsor", p);
		result.addObject("sponsorid", sponsorId);

		return result;
	}

	//Create ------------------------------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		SponsorForm pro;

		try {
			pro = new SponsorForm();
			result = new ModelAndView("sponsor/create");
			result.addObject("sponsorForm", pro);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute("sponsorForm") @Valid final SponsorForm sponsorForm, final BindingResult binding) {
		ModelAndView result;
		Sponsor sponsor;

		sponsor = this.sponsorService.reconstruct(sponsorForm, binding);
		final CreditCard creditCard = sponsor.getCreditCard();
		final Customization customization = this.customizationService.findCustomization();
		final Collection<String> brands = customization.getBrandNames();

		if (!sponsorForm.getUserAccount().getPassword().equals(sponsorForm.getConfirmPassword()))
			if (LocaleContextHolder.getLocale().getLanguage().toUpperCase().contains("ES")) {
				binding.addError(new FieldError("sponsorForm", "confirmPassword", sponsorForm.getConfirmPassword(), false, null, null, "Las contraseņas deben coincidir"));
				return this.createEditModelAndView(sponsorForm);
			}

			else {
				binding.addError(new FieldError("sponsorForm", "confirmPassword", sponsorForm.getConfirmPassword(), false, null, null, "Passwords must be the same"));
				return this.createEditModelAndView(sponsorForm);
			}

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = new ModelAndView("sponsor/create");
			result.addObject("sponsorForm", sponsorForm);
		}

		else
			try {

				this.sponsorService.save(sponsor);
				result = new ModelAndView("redirect:../security/login.do");
			} catch (final Throwable oops) {
				System.out.println(sponsor);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.createEditModelAndView(sponsorForm);

				if (oops instanceof DataIntegrityViolationException)
					result = this.createEditModelAndView(sponsorForm, "sponsor.duplicated.username");
				else
					result = this.createEditModelAndView(sponsorForm, "sponsor.registration.error");
			}
		return result;
	}

	//Edit --------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView result;
		final Sponsor sponsor;

		try {
			sponsor = this.sponsorService.findByPrincipal();

			result = new ModelAndView("sponsor/edit");
			result.addObject("sponsor", sponsor);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@ModelAttribute("sponsor") final Sponsor sponsor, final BindingResult binding) {
		ModelAndView result;
		final Sponsor pro;

		pro = this.sponsorService.reconstruct(sponsor, binding);

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = this.editModelAndView(pro);
			result.addObject("sponsor", sponsor);
		}

		else
			try {
				this.sponsorService.save(pro);
				result = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				System.out.println(pro);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.editModelAndView(pro, "sponsor.registration.error");
			}
		return result;
	}

	//Ancillary methods -------------------------------------------------------------------------
	protected ModelAndView createEditModelAndView(final SponsorForm sponsorForm) {
		ModelAndView result;

		result = this.createEditModelAndView(sponsorForm, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final SponsorForm sponsorForm, final String message) {
		ModelAndView result;

		result = new ModelAndView("sponsor/create");
		result.addObject("sponsorForm", sponsorForm);
		result.addObject("message", message);

		return result;
	}

	protected ModelAndView editModelAndView(final Sponsor sponsor) {
		ModelAndView result;

		result = this.editModelAndView(sponsor, null);

		return result;
	}

	protected ModelAndView editModelAndView(final Sponsor sponsor, final String message) {
		ModelAndView result;

		result = new ModelAndView("sponsor/edit");
		result.addObject("sponsor", sponsor);
		result.addObject("message", message);

		return result;
	}

	private ModelAndView forbiddenOpperation() {
		return new ModelAndView("redirect:/");
	}

}
