package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.OrganizerService;
import services.WorkerService;
import services.PaymentService;
import controllers.AbstractController;
import domain.Actor;
import domain.Payment;
import domain.Worker;
import domain.Organizer;


@Controller
@RequestMapping("/payment/organizer")
public class PaymentController extends AbstractController {

	@Autowired
	private PaymentService    paymentService;
	@Autowired
	private WorkerService	  workerService;


	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView res;
		Collection<Payment> payments;

		payments = this.paymentService.getPaymentsByOrganizer();

		res = new ModelAndView("payment/list");
		res.addObject("payments", payments);
		res.addObject("requestURI", "payment/list.do");

		return res;
	}
	
	@RequestMapping(value = "/listWorker", method = RequestMethod.GET)
	public ModelAndView listWorker() {

		ModelAndView res;
		Collection<Payment> payments;

		payments = this.paymentService.getPaymentsByOrganizer();

		res = new ModelAndView("payment/list");
		res.addObject("payments", payments);
		res.addObject("requestURI", "payment/list.do");

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		
		ModelAndView res;
		Payment p;

		//metodo para que devuelva los workers de las marathons de un organizer
		final Collection<Worker> workers = this.workerService.findAll();

		p = this.paymentService.create();
		res = this.createEditModelAndView(p);
		res.addObject("workers", workers);

		return res;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final int paymentId) {
		ModelAndView result;
		Payment p;
		p = this.paymentService.findOne(paymentId);
		result = new ModelAndView("payment/show");
		result.addObject("payment", p);

		return result;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST, params = "save")
	public ModelAndView save(Payment p, final BindingResult binding) {
		
		ModelAndView res;
		Payment reconstructed;
		
		reconstructed = this.paymentService.reconstruct(p, binding);
		final Collection<Worker> workers = this.workerService.findAll();

		if (binding.hasErrors()) {
			System.out.println(binding.getAllErrors());
			res = this.createEditModelAndView(p);
			res.addObject("workers", workers);
		} else
			try {
				this.paymentService.save(reconstructed);
				res = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				oops.printStackTrace();
				res = this.createEditModelAndView(p, "Payment.commit.error");
				res.addObject("workers", workers);
			}

		return res;
	}

	//Metodos auxiliares
	
	protected ModelAndView createEditModelAndView(final Payment p) {
		ModelAndView res;

		res = this.createEditModelAndView(p, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(final Payment p, final String messageCode) {
		ModelAndView res;

		if (p.getId() == 0)
			res = new ModelAndView("payment/create");
		else
			res = new ModelAndView("payment/edit");
		
		res.addObject("payment", p);
		res.addObject("message", messageCode);

		return res;
	}

}
