
package controllers;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.UserAccountRepository;
import services.OrganizerService;
import domain.CreditCard;
import domain.Organizer;
import forms.OrganizerForm;

@Controller
@RequestMapping("/organizer")
public class OrganizerController extends AbstractController {

	@Autowired
	private OrganizerService		organizerService;

	//@Autowired
	//private ConfigurationsService	configurationsService;

	@Autowired
	private UserAccountRepository	userAccountRepository;


	@ExceptionHandler(TypeMismatchException.class)
	public ModelAndView handleMismatchException(final TypeMismatchException oops) {
		return new ModelAndView("redirect:/");
	}

	// List ------------------------------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Organizer> bros;

		try {
			bros = this.organizerService.findAll();
			result = new ModelAndView("runner/list");
			result.addObject("organizers", bros);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	// Register ------------------------------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		OrganizerForm bro;

		try {
			bro = new OrganizerForm();
			result = new ModelAndView("organizer/create");
			result.addObject("organizerForm", bro);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	// Save the new organizer ------------------------------------------------------------------------------------

	// Edit ------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView result;
		final Organizer organizer;

		try {
			organizer = this.organizerService.findByPrincipal();

			result = new ModelAndView("organizer/edit");
			result.addObject("organizer", organizer);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute("organizerForm") @Valid final OrganizerForm organizerForm, final BindingResult binding) {
		ModelAndView result;
		Organizer organizer;

		organizer = this.organizerService.reconstruct(organizerForm, binding);
		final CreditCard creditCard = organizer.getCreditCard();
		//final Configurations configurations = this.configurationsService.getConfiguration();
		//final Collection<String> brands = configurations.getBrandNames();

		if (!organizerForm.getUserAccount().getPassword().equals(organizerForm.getConfirmPassword()))
			if (LocaleContextHolder.getLocale().getLanguage().toUpperCase().contains("ES")) {
				binding.addError(new FieldError("runnerForm", "confirmPassword", organizerForm.getConfirmPassword(), false, null, null, "Las contraseņas deben coincidir"));
				return this.createEditModelAndView(organizerForm);
			}

			else {
				binding.addError(new FieldError("organizerForm", "confirmPassword", organizerForm.getConfirmPassword(), false, null, null, "Passwords must be the same"));
				return this.createEditModelAndView(organizerForm);
			}

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = new ModelAndView("organizer/create");
			result.addObject("organizerForm", organizerForm);
		}

		else
			try {

				this.organizerService.save(organizer);
				result = new ModelAndView("redirect:../security/login.do");
			} catch (final Throwable oops) {
				System.out.println(organizer);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.createEditModelAndView(organizerForm);

				if (oops instanceof DataIntegrityViolationException)
					result = this.createEditModelAndView(organizerForm, "organizer.duplicated.username");
				else
					result = this.createEditModelAndView(organizerForm, "organizer.registration.error");
			}
		return result;
	}
	// SAVE ------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@ModelAttribute("organizer") final Organizer prune, final BindingResult binding) {
		ModelAndView result;
		final Organizer organizer;

		organizer = this.organizerService.reconstruct(prune, binding);

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = this.editModelAndView(organizer);
			result.addObject("organizer", prune);
		}

		else
			try {
				this.organizerService.save(organizer);
				result = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				System.out.println(organizer);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.editModelAndView(organizer, "organizer.registration.error");
			}
		return result;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final int organizerId) {
		ModelAndView result;
		Organizer p;
		p = this.organizerService.findOne(organizerId);

		result = new ModelAndView("organizer/show");
		result.addObject("organizer", p);
		result.addObject("organizerId", organizerId);

		return result;
	}

	// Ancillary methods -----------------------------------------------------------------------
	protected ModelAndView createEditModelAndView(final OrganizerForm organizerForm) {
		ModelAndView result;

		result = this.createEditModelAndView(organizerForm, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final OrganizerForm organizerForm, final String message) {
		ModelAndView result;

		result = new ModelAndView("organizer/create");
		result.addObject("organizerForm", organizerForm);
		result.addObject("message", message);

		return result;
	}

	protected ModelAndView editModelAndView(final Organizer organizer) {
		ModelAndView result;

		result = this.editModelAndView(organizer, null);

		return result;
	}

	protected ModelAndView editModelAndView(final Organizer organizer, final String message) {
		ModelAndView result;

		result = new ModelAndView("organizer/edit");
		result.addObject("organizer", organizer);
		result.addObject("message", message);

		return result;
	}

	private ModelAndView forbiddenOpperation() {
		return new ModelAndView("redirect:/");
	}
}
