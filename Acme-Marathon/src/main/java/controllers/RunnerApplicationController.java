
package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.MarathonService;
import services.MessageService;
import services.OrganizerService;
import services.RunnerApplicationService;
import services.RunnerService;
import domain.Actor;
import domain.Marathon;
import domain.Runner;
import domain.RunnerApplication;
import domain.Worker;
import domain.Organizer;

@Controller
@RequestMapping("/runnerApplication")
public class RunnerApplicationController extends AbstractController {

	@Autowired
	private RunnerApplicationService	runnerApplicationService;

	@Autowired
	private RunnerService				runnerService;

	@Autowired
	private ActorService				actorService;

	@Autowired
	private MessageService				messageService;

	@Autowired
	private MarathonService				marathonService;

	@Autowired
	private OrganizerService			organizerService;


	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		Actor principal;
		ModelAndView res;

		Collection<RunnerApplication> pendingApps = null;
		Collection<RunnerApplication> acceptedApps = null;
		Collection<RunnerApplication> rejectedApps = null;
		Collection<RunnerApplication> runnedApps = null;

		principal = this.actorService.findByPrincipal();

		if (this.runnerService.findOneNoAssert(principal.getId()) != null && principal instanceof Runner) {
			pendingApps = this.runnerApplicationService.findRunnerApplicationsByStatusRunner("PENDING");
			acceptedApps = this.runnerApplicationService.findRunnerApplicationsByStatusRunner("ACCEPTED");
			rejectedApps = this.runnerApplicationService.findRunnerApplicationsByStatusRunner("REJECTED");
			runnedApps = this.runnerApplicationService.findRunnerApplicationsByStatusRunner("RUNNED");
		} else if (this.organizerService.findOneNoAssert(principal.getId()) != null && principal instanceof Organizer) {
			pendingApps = this.runnerApplicationService.findRunnerApplicationsByStatusOrganizer("PENDING");
			acceptedApps = this.runnerApplicationService.findRunnerApplicationsByStatusOrganizer("ACCEPTED");
			rejectedApps = this.runnerApplicationService.findRunnerApplicationsByStatusOrganizer("REJECTED");
			runnedApps = this.runnerApplicationService.findRunnerApplicationsByStatusOrganizer("RUNNED");
		}

		res = new ModelAndView("runnerApplication/list");
		res.addObject("pendingApps", pendingApps);
		res.addObject("acceptedApps", acceptedApps);
		res.addObject("rejectedApps", rejectedApps);
		res.addObject("runnedApps", runnedApps);
		res.addObject("requestURI", "runnerApplication/list.do");

		return res;
	}

	@RequestMapping(value = "/accept", method = RequestMethod.GET)
	public ModelAndView accept(@RequestParam final int runnerApplicationId) {

		ModelAndView result;
		final RunnerApplication runnerApplication;

		runnerApplication = this.runnerApplicationService.findOne(runnerApplicationId);
		final Runner runner = runnerApplication.getRunner();

		//		final Message notification = new Message();
		//
		//		notification.setSubject("Your application has been accepted.");
		//		notification.setBody("Your application to " + runnerApplication.getMarathon().getTicker() + " has been accepted.");
		//		final Date moment = new Date();
		//		notification.setMoment(moment);
		//		final Collection<String> tags = new ArrayList<String>();
		//		notification.setTags(tags);
		//		notification.setSender(this.actorService.findByUserAccount(runner.getUserAccount()));
		//		notification.setReceiver(this.actorService.findByUserAccount(runner.getUserAccount()));
		//
		//		this.messageService.save(notification);

		this.runnerApplicationService.acceptRunnerApplication(runnerApplicationId);

		result = new ModelAndView("redirect:/runnerApplication/list.do");

		return result;
	}

	@RequestMapping(value = "/reject", method = RequestMethod.GET)
	public ModelAndView reject(@RequestParam final int runnerApplicationId) {

		ModelAndView result;
		final RunnerApplication runnerApplication;

		runnerApplication = this.runnerApplicationService.findOne(runnerApplicationId);
		final Runner runner = runnerApplication.getRunner();

		//		final Message notification = new Message();
		//
		//		notification.setSubject("Your application has been rejected.");
		//		notification.setBody("Your application to " + runnerApplication.getMarathon().getTicker() + " has been rejected.");
		//		final Date moment = new Date();
		//		notification.setMoment(moment);
		//		final Collection<String> tags = new ArrayList<String>();
		//		notification.setTags(tags);
		//		notification.setSender(this.actorService.findByUserAccount(runner.getUserAccount()));
		//		notification.setReceiver(this.actorService.findByUserAccount(runner.getUserAccount()));
		//
		//		this.messageService.save(notification);

		this.runnerApplicationService.rejectRunnerApplication(runnerApplicationId);

		result = new ModelAndView("redirect:/runnerApplication/list.do");

		return result;
	}
	
	@RequestMapping(value = "/run", method = RequestMethod.GET)
	public ModelAndView run(@RequestParam final int runnerApplicationId) {

		ModelAndView result;
		final RunnerApplication runnerApplication;

		runnerApplication = this.runnerApplicationService.findOne(runnerApplicationId);
		final Runner runner = runnerApplication.getRunner();

		this.runnerApplicationService.runRunnerApplication(runnerApplicationId);

		result = new ModelAndView("redirect:/runnerApplication/list.do");

		return result;
	}


	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final int runnerApplicationId) {

		ModelAndView result;
		final RunnerApplication app;

		app = this.runnerApplicationService.findOne(runnerApplicationId);
		result = new ModelAndView("runnerApplication/show");
		result.addObject("runnerApplication", app);

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int marathonId) {

		ModelAndView res;
		RunnerApplication app;

		final Marathon m = this.marathonService.findOne(marathonId);

		this.runnerApplicationService.checkRunnerHasAlreadyApplied(m);

		app = this.runnerApplicationService.create();
		app.setMarathon(this.marathonService.findOne(marathonId));
		res = this.createEditModelAndView(app);

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final RunnerApplication r, final BindingResult binding) {
		ModelAndView res;

		final Integer dorsal = this.marathonService.numberOfRunnerApplicationsByMarathon(r.getMarathon().getId());
		r.setDorsalNumber(dorsal);

		if (binding.hasErrors()) {
			System.out.println(binding.getAllErrors());
			res = this.createEditModelAndView(r);
		} else
			try {
				this.runnerApplicationService.save(r);
				res = new ModelAndView("redirect:/runnerApplication/list.do");
			} catch (final Throwable oops) {
				res = this.createEditModelAndView(r, "runnerApplication.commit.error");
			}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int runnerApplicationId) {
		ModelAndView res;
		RunnerApplication r;

		r = this.runnerApplicationService.findOneToEdit(runnerApplicationId);

		res = this.createEditModelAndView(r);

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final RunnerApplication r, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			System.out.println(binding.getAllErrors());
			res = this.createEditModelAndView(r);
		} else
			try {
				this.runnerApplicationService.save(r);
				res = new ModelAndView("redirect:/runnerApplication/list.do");
			} catch (final Throwable oops) {
				oops.printStackTrace();
				res = this.createEditModelAndView(r, "runnerApplication.commit.error");
			}
		return res;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int runnerApplicationId) {

		ModelAndView res;

		this.runnerApplicationService.delete(runnerApplicationId);

		res = new ModelAndView("redirect:/runnerApplication/list.do");
		return res;
	}

	protected ModelAndView createEditModelAndView(final RunnerApplication app) {
		ModelAndView res;

		res = this.createEditModelAndView(app, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(final RunnerApplication app, final String messageCode) {
		ModelAndView res;

		if (app.getId() == 0)
			res = new ModelAndView("runnerApplication/create");
		else
			res = new ModelAndView("runnerApplication/edit");
		res.addObject("runnerApplication", app);
		res.addObject("message", messageCode);

		return res;
	}

}
