package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;

import services.MarathonService;
import services.ComplaintService;
import services.OrganizerService;
import controllers.AbstractController;
import domain.Actor;
import domain.Administrator;
import domain.Complaint;
import domain.Marathon;
import domain.Organizer;
import domain.Runner;


@Controller
@RequestMapping("/complaint")
public class ComplaintController extends AbstractController {

	@Autowired
	private ComplaintService ComplaintService;
	@Autowired
	private MarathonService	 MarathonService;
	@Autowired
	private ActorService	 actorService;
	@Autowired
	private OrganizerService organizerService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView res;
		Collection<Complaint> Complaints;

		Complaints = this.ComplaintService.findAll();

		res = new ModelAndView("complaint/list");
		res.addObject("complaints", Complaints);
		res.addObject("requestURI", "complaint/list.do");

		return res;
	}

	@RequestMapping(value = "/listRunner", method = RequestMethod.GET)
	public ModelAndView listRunner() {

		ModelAndView res;
		Collection<Complaint> Complaints;

		final int logueadoId = this.actorService.findByPrincipal().getId();
		Complaints = this.ComplaintService.getComplaintsByRunner(logueadoId);

		res = new ModelAndView("complaint/list");
		res.addObject("complaints", Complaints);
		res.addObject("requestURI", "complaint/listRunner.do");

		return res;
	}
	
	@RequestMapping(value = "/listMarathon", method = RequestMethod.GET)
	public ModelAndView listByMarathon(@RequestParam final int marathonId) {

		ModelAndView res;
		Collection<Complaint> Complaints;

		Complaints = this.ComplaintService.getComplaintsByMarathon(marathonId);

		res = new ModelAndView("complaint/list");
		res.addObject("complaints", Complaints);
		res.addObject("requestURI", "complaint/listMarathon.do");

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		
		ModelAndView res;
		Complaint c;

		final Collection<Marathon> Marathons = this.MarathonService.findAll();

		c = this.ComplaintService.create();
		res = this.createEditModelAndView(c);
		res.addObject("marathons", Marathons);

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int complaintId) {
		
		ModelAndView res;
		Complaint c;
		
		Actor principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);
		final Organizer org = this.organizerService.findByPrincipal();
		
		c = this.ComplaintService.findOne(complaintId);
		res = this.createEditModelAndView(c);

		//comprobamos que la complaint este en modo draft y sea el due�o legitimo el que accede
		// si no es asi, se le manda fuera
		if (c.getStatus() == false || !c.getMarathon().getOrganizer().equals(org))
			res = new ModelAndView("redirect:/#");
		
		return res;
	}
	
	@RequestMapping(value = "/changeStatus", method = RequestMethod.GET)
	public ModelAndView changeStatus (@RequestParam final int complaintId) {
		
		ModelAndView res;
		Complaint c;
		
		Actor principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
	
		c = this.ComplaintService.findOne(complaintId);
		res = new ModelAndView("complaint/changeStatus");
		res.addObject("complaint", c);


		//comprobamos que la complaint esta en modo borrador
		if (c.getStatus() == false)
			res = new ModelAndView("redirect:/#");
		
		return res;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final int complaintId) {
		ModelAndView result;
		Complaint c;
		c = this.ComplaintService.findOne(complaintId);
		result = new ModelAndView("complaint/show");
		result.addObject("complaint", c);

		return result;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST, params = "save")
	public ModelAndView save(Complaint c, final BindingResult binding) {
		
		ModelAndView res;
		Complaint reconstructed;
		
		reconstructed = this.ComplaintService.reconstruct(c, binding);
		final Collection<Marathon> marathons = this.MarathonService.findAll();

		if (binding.hasErrors()) {
			System.out.println(binding.getAllErrors());
			res = this.createEditModelAndView(c);
			res.addObject("marathons", marathons);
		} else
			try {
				this.ComplaintService.save(reconstructed);
				res = new ModelAndView("redirect:/#");
			} catch (final Throwable oops) {
				oops.printStackTrace();
				res = this.createEditModelAndView(c, "complaint.commit.error");
				res.addObject("marathons", marathons);
			}

		return res;
	}

	//Metodos auxiliares
	
	protected ModelAndView createEditModelAndView(final Complaint c) {
		ModelAndView res;

		res = this.createEditModelAndView(c, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(final Complaint c, final String messageCode) {
		ModelAndView res;

		if (c.getId() == 0)
			res = new ModelAndView("complaint/create");
		else
			res = new ModelAndView("complaint/edit");
		
		res.addObject("complaint", c);
		res.addObject("message", messageCode);

		return res;
	}

}
