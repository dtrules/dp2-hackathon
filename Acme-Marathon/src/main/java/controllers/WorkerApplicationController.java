
package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;

import services.MarathonService;
import services.MessageService;
import services.OrganizerService;
import services.WorkerApplicationService;
import services.WorkerService;
import domain.Actor;
import domain.Marathon;
import domain.Message;
import domain.Worker;
import domain.Organizer;
import domain.WorkerApplication;

@Controller
@RequestMapping("/workerApplication")
public class WorkerApplicationController extends AbstractController {

	@Autowired
	private WorkerApplicationService	workerApplicationService;

	@Autowired
	private WorkerService				workerService;
	
	@Autowired
	private OrganizerService			organizerService;

	@Autowired
	private ActorService				actorService;

	@Autowired
	private MessageService				messageService;

	@Autowired
	private MarathonService				marathonService;


	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		Actor principal;
		ModelAndView res;

		Collection<WorkerApplication> pendingApps = null;
		Collection<WorkerApplication> acceptedApps = null;
		Collection<WorkerApplication> rejectedApps = null;

		principal = this.actorService.findByPrincipal();

		if (this.workerService.findOneNoAssert(principal.getId()) != null && principal instanceof Worker) {
			pendingApps = this.workerApplicationService.findWorkerApplicationsByStatusWorker("PENDING");
			acceptedApps = this.workerApplicationService.findWorkerApplicationsByStatusWorker("ACCEPTED");
			rejectedApps = this.workerApplicationService.findWorkerApplicationsByStatusWorker("REJECTED");
		} else if (this.organizerService.findOneNoAssert(principal.getId()) != null && principal instanceof Organizer) {
			pendingApps = this.workerApplicationService.findWorkerApplicationsByStatusOrganizer("PENDING");
			acceptedApps = this.workerApplicationService.findWorkerApplicationsByStatusOrganizer("ACCEPTED");
			rejectedApps = this.workerApplicationService.findWorkerApplicationsByStatusOrganizer("REJECTED");
		}

		res = new ModelAndView("workerApplication/list");
		res.addObject("pendingApps", pendingApps);
		res.addObject("acceptedApps", acceptedApps);
		res.addObject("rejectedApps", rejectedApps);
		res.addObject("requestURI", "workerApplication/list.do");

		return res;
	}

	@RequestMapping(value = "/accept", method = RequestMethod.GET)
	public ModelAndView accept(@RequestParam final int workerApplicationId) {

		ModelAndView result;

		this.workerApplicationService.acceptWorkerApplication(workerApplicationId);

		result = new ModelAndView("redirect:/workerApplication/list.do");

		return result;
	}

	@RequestMapping(value = "/reject", method = RequestMethod.GET)
	public ModelAndView reject(@RequestParam final int workerApplicationId) {

		ModelAndView result;

		this.workerApplicationService.rejectWorkerApplication(workerApplicationId);

		result = new ModelAndView("redirect:/workerApplication/list.do");

		return result;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final int workerApplicationId) {

		ModelAndView result;
		final WorkerApplication app;

		app = this.workerApplicationService.findOne(workerApplicationId);
		result = new ModelAndView("workerApplication/show");
		result.addObject("workerApplication", app);

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView res;
		WorkerApplication app;

		final Collection<Marathon> Marathons = this.marathonService.findAll();
		
		app = this.workerApplicationService.create();
		res = this.createEditModelAndView(app);
		res.addObject("marathons", Marathons);

		return res;
	}
	

	@RequestMapping(value = "/save", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final WorkerApplication app, final BindingResult binding) {
		
		ModelAndView res;
		
		final Collection<Marathon> marathons = this.marathonService.findAll();

		if (binding.hasErrors()) {
			System.out.println(binding.getAllErrors());
			res = this.createEditModelAndView(app);
			res.addObject("marathons", marathons);
		} else
			try {
				this.workerApplicationService.save(app);
				res = new ModelAndView("redirect:/workerApplication/list.do");
			} catch (final Throwable oops) {
				oops.printStackTrace();
				res = this.createEditModelAndView(app, "workerApplication.commit.error");
				res.addObject("marathons", marathons);
			}
		
		return res;
	}


	protected ModelAndView createEditModelAndView(final WorkerApplication app) {
		ModelAndView res;

		res = this.createEditModelAndView(app, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(final WorkerApplication app, final String messageCode) {
		ModelAndView res;

        res = new ModelAndView("workerApplication/create");
		res.addObject("workerApplication", app);
		res.addObject("message", messageCode);

		return res;
	}

}
