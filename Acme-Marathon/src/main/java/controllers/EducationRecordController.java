
package controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.CurriculumService;
import services.EducationRecordService;
import services.WorkerService;
import domain.Actor;
import domain.Curriculum;
import domain.EducationRecord;
import domain.Worker;

@Controller
@RequestMapping("/educationRecord")
public class EducationRecordController extends AbstractController {

	@Autowired
	private WorkerService			workerService;

	@Autowired
	private ActorService			actorService;

	@Autowired
	private EducationRecordService	educationRecordService;

	@Autowired
	private CurriculumService		curriculumService;


	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam final int curriculumId) {

		ModelAndView res;

		final Actor principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);
		final Curriculum curriculum = this.curriculumService.findOne(curriculumId);
		res = new ModelAndView("educationRecord/list");
		res.addObject("educationRecords", curriculum.getEducationRecords());
		res.addObject("requestURI", "educationRecord/list.do");

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int curriculumId) {
		ModelAndView res;
		EducationRecord educationRecord;

		educationRecord = this.educationRecordService.create(curriculumId);
		res = this.createEditModelAndView(educationRecord, curriculumId);

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int educationRecordId) {
		ModelAndView res;
		EducationRecord educationRecord;

		educationRecord = this.educationRecordService.findOneToEdit(educationRecordId);
		res = this.editModelAndView(educationRecord);

		res.addObject("educationRecord", educationRecord);

		return res;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final int educationRecordId) {
		ModelAndView result;
		EducationRecord educationRecord;
		educationRecord = this.educationRecordService.findOne(educationRecordId);
		result = new ModelAndView("educationRecord/show");
		result.addObject("educationRecord", educationRecord);

		return result;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int educationRecordId) {
		ModelAndView result;

		EducationRecord res;

		res = this.educationRecordService.findOne(educationRecordId);

		this.educationRecordService.delete(res);
		result = new ModelAndView("redirect:/curriculum/list.do");

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@RequestParam final int curriculumId, @Valid final EducationRecord sp, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			System.out.println(binding.getAllErrors());
			res = this.createEditModelAndView(sp, curriculumId);
		} else
			try {
				this.educationRecordService.save(sp, curriculumId);
				res = new ModelAndView("redirect:/curriculum/list.do");
			} catch (final Throwable oops) {
				res = this.createEditModelAndView(sp, curriculumId, "educationRecord.commit.error");
			}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final EducationRecord sp, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			System.out.println(binding.getAllErrors());
			res = this.editModelAndView(sp);
		} else
			try {
				this.educationRecordService.saveEdit(sp);
				res = new ModelAndView("redirect:/curriculum/list.do");
			} catch (final Throwable oops) {
				res = this.editModelAndView(sp, "educationRecord.commit.error");
			}
		return res;
	}

	protected ModelAndView createEditModelAndView(final EducationRecord sp, final int curriculumId) {
		ModelAndView res;

		res = this.createEditModelAndView(sp, curriculumId, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(final EducationRecord sp, final int curriculumId, final String messageCode) {
		ModelAndView res;

		if (sp.getId() == 0)
			res = new ModelAndView("educationRecord/create");
		else
			res = new ModelAndView("educationRecord/edit");
		res.addObject("educationRecord", sp);
		res.addObject("curriculumId", curriculumId);
		res.addObject("message", messageCode);

		return res;
	}

	protected ModelAndView editModelAndView(final EducationRecord sp) {
		ModelAndView res;

		res = this.editModelAndView(sp, null);

		return res;
	}

	protected ModelAndView editModelAndView(final EducationRecord sp, final String messageCode) {
		ModelAndView res;

		res = new ModelAndView("educationRecord/edit");
		res.addObject("educationRecord", sp);
		res.addObject("message", messageCode);

		return res;
	}

}
