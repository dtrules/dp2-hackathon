
package controllers;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import security.UserAccountRepository;
import services.RunnerService;
import domain.CreditCard;
import domain.Runner;
import forms.RunnerForm;

@Controller
@RequestMapping("/runner")
public class RunnerController extends AbstractController {

	@Autowired
	private RunnerService			runnerService;

	//@Autowired
	//private ConfigurationsService	configurationsService;

	@Autowired
	private UserAccountRepository	userAccountRepository;


	@ExceptionHandler(TypeMismatchException.class)
	public ModelAndView handleMismatchException(final TypeMismatchException oops) {
		return new ModelAndView("redirect:/");
	}

	// List ------------------------------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Runner> bros;

		try {
			bros = this.runnerService.findAll();
			result = new ModelAndView("runner/list");
			result.addObject("runners", bros);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	// Register ------------------------------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		RunnerForm bro;

		try {
			bro = new RunnerForm();
			result = new ModelAndView("runner/create");
			result.addObject("runnerForm", bro);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	// Save the new runner ------------------------------------------------------------------------------------

	// Edit ------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView result;
		final Runner runner;

		try {
			runner = this.runnerService.findByPrincipal();

			result = new ModelAndView("runner/edit");
			result.addObject("runner", runner);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute("runnerForm") @Valid final RunnerForm runnerForm, final BindingResult binding) {
		ModelAndView result;
		Runner runner;

		runner = this.runnerService.reconstruct(runnerForm, binding);
		final CreditCard creditCard = runner.getCreditCard();
		//final Configurations configurations = this.configurationsService.getConfiguration();
		//final Collection<String> brands = configurations.getBrandNames();

		if (!runnerForm.getUserAccount().getPassword().equals(runnerForm.getConfirmPassword()))
			if (LocaleContextHolder.getLocale().getLanguage().toUpperCase().contains("ES")) {
				binding.addError(new FieldError("runnerForm", "confirmPassword", runnerForm.getConfirmPassword(), false, null, null, "Las contraseņas deben coincidir"));
				return this.createEditModelAndView(runnerForm);
			}

			else {
				binding.addError(new FieldError("runnerForm", "confirmPassword", runnerForm.getConfirmPassword(), false, null, null, "Passwords must be the same"));
				return this.createEditModelAndView(runnerForm);
			}

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = new ModelAndView("runner/create");
			result.addObject("runnerForm", runnerForm);
		}

		else
			try {

				this.runnerService.save(runner);
				result = new ModelAndView("redirect:../security/login.do");
			} catch (final Throwable oops) {
				System.out.println(runner);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.createEditModelAndView(runnerForm);

				if (oops instanceof DataIntegrityViolationException)
					result = this.createEditModelAndView(runnerForm, "runner.duplicated.username");
				else
					result = this.createEditModelAndView(runnerForm, "runner.registration.error");
			}
		return result;
	}
	// SAVE ------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@ModelAttribute("runner") final Runner prune, final BindingResult binding) {
		ModelAndView result;
		final Runner runner;

		runner = this.runnerService.reconstruct(prune, binding);

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = this.editModelAndView(runner);
			result.addObject("runner", prune);
		}

		else
			try {
				this.runnerService.save(runner);
				result = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				System.out.println(runner);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.editModelAndView(runner, "runner.registration.error");
			}
		return result;
	}

	// Ancillary methods -----------------------------------------------------------------------
	protected ModelAndView createEditModelAndView(final RunnerForm runnerForm) {
		ModelAndView result;

		result = this.createEditModelAndView(runnerForm, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final RunnerForm runnerForm, final String message) {
		ModelAndView result;

		result = new ModelAndView("runner/create");
		result.addObject("runnerForm", runnerForm);
		result.addObject("message", message);

		return result;
	}

	protected ModelAndView editModelAndView(final Runner runner) {
		ModelAndView result;

		result = this.editModelAndView(runner, null);

		return result;
	}

	protected ModelAndView editModelAndView(final Runner runner, final String message) {
		ModelAndView result;

		result = new ModelAndView("runner/edit");
		result.addObject("runner", runner);
		result.addObject("message", message);

		return result;
	}

	private ModelAndView forbiddenOpperation() {
		return new ModelAndView("redirect:/");
	}
}
