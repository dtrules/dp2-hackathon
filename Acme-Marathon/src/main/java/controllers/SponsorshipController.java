
package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.SponsorshipService;
import domain.Sponsorship;

@Controller
@RequestMapping("/sponsorship")
public class SponsorshipController extends AbstractController {

	//Constructor --------------------------

	public SponsorshipController() {
		super();
	}


	//Services ----------------------

	@Autowired
	private SponsorshipService	sponsorshipService;


	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	public ModelAndView listAll() {
		ModelAndView result;
		Collection<Sponsorship> sponsorships = this.sponsorshipService.findAll();
		result = new ModelAndView("sponsorship/listAll");
		result.addObject("sponsorships", sponsorships);

		return result;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final int sponsorshipId) {
		ModelAndView result;
		Sponsorship p;
		p = this.sponsorshipService.findOne(sponsorshipId);

		result = new ModelAndView("sponsorship/show");
		result.addObject("sponsorship", p);
		result.addObject("sponsorshipId", sponsorshipId);

		return result;
	}

}
