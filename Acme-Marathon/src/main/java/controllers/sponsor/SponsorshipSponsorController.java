
package controllers.sponsor;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.MarathonService;
import services.SponsorService;
import services.SponsorshipService;
import controllers.AbstractController;
import domain.Marathon;
import domain.Sponsor;
import domain.Sponsorship;

@Controller
@RequestMapping("/sponsorship/sponsor")
public class SponsorshipSponsorController extends AbstractController {

	//Constructor --------------------------

	public SponsorshipSponsorController() {
		super();
	}


	//Services ----------------------

	@Autowired
	private SponsorshipService	sponsorshipService;

	@Autowired
	private SponsorService		sponsorService;

	@Autowired
	private MarathonService		marathonService;


	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		Collection<Marathon> marathons = this.marathonService.findAll();
		final Sponsorship sponsorship = this.sponsorshipService.create();

		final ModelAndView result = this.createEditModelAndView(sponsorship);
		result.addObject("marathons", marathons);
		return result;

	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Sponsorship sponsorship, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(sponsorship);
		//System.out.println(binding.getAllErrors());
		else
			try {

				this.sponsorshipService.save(sponsorship);

				result = new ModelAndView("redirect:/sponsorship/sponsor/list.do");

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.createEditModelAndView(sponsorship, "sponsorship.commit.error");
			}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int sponsorshipId) {
		ModelAndView res;
		Sponsorship sponsorship;

		sponsorship = this.sponsorshipService.findOneToEdit(sponsorshipId);
		res = this.createEditModelAndView(sponsorship);

		res.addObject("sponsorship", sponsorship);

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Sponsorship sp, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			System.out.println(binding.getAllErrors());
			res = this.createEditModelAndView(sp);
		} else
			try {
				this.sponsorshipService.save(sp);
				res = new ModelAndView("redirect:/sponsorship/sponsor/list.do");
			} catch (final Throwable oops) {
				res = this.createEditModelAndView(sp, "sponsorship.commit.error");
			}
		return res;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int sponsorshipId) {

		ModelAndView res;

		this.sponsorshipService.delete(sponsorshipId);

		res = new ModelAndView("redirect:/sponsorship/sponsor/list.do");
		return res;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Sponsorship> sponsorships = new ArrayList<Sponsorship>();
		Sponsor sponsor = this.sponsorService.findByPrincipal();
		sponsorships = this.sponsorshipService.findSponsorshipsBySponsor(sponsor.getId());
		result = new ModelAndView("sponsorship/sponsor/list");
		result.addObject("sponsorships", sponsorships);

		return result;
	}

	private ModelAndView createEditModelAndView(final Sponsorship sponsorship) {

		return this.createEditModelAndView(sponsorship, null);
	}

	protected ModelAndView createEditModelAndView(final Sponsorship sp, final String messageCode) {
		ModelAndView res;
		final Collection<Marathon> marathons = this.marathonService.findAll();

		if (sp.getId() == 0)
			res = new ModelAndView("sponsorship/sponsor/create");
		else
			res = new ModelAndView("sponsorship/sponsor/edit");
		res.addObject("sponsorship", sp);
		res.addObject("message", messageCode);
		res.addObject("marathons", marathons);

		return res;
	}
}
