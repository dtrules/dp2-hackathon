
package controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.CurriculumService;
import services.PersonalRecordService;
import domain.Actor;
import domain.Curriculum;
import domain.PersonalRecord;

@Controller
@RequestMapping("/personalRecord")
public class PersonalRecordController extends AbstractController {

	@Autowired
	private PersonalRecordService	personalRecordService;

	@Autowired
	private ActorService		actorService;
	
	@Autowired
	private CurriculumService		curriculumService;


	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int curriculumId) {
		ModelAndView res;
		PersonalRecord personalRecord;

		personalRecord = this.personalRecordService.create(curriculumId);
		res = this.createEditModelAndView(personalRecord);
		res.addObject("personalRecord", personalRecord);

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int personalRecordId) {
		ModelAndView res;
		PersonalRecord personalRecord;

		personalRecord = this.personalRecordService.findOne(personalRecordId);
		res = this.createEditModelAndView(personalRecord);

		res.addObject("personalRecord", personalRecord);

		return res;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final int curriculumId) {
		ModelAndView result;
		Curriculum curriculum = this.curriculumService.findOne(curriculumId);
		PersonalRecord personalRecord = curriculum.getPersonalRecord();
		final String photo = personalRecord.getPhoto();
		
		result = new ModelAndView("personalRecord/show");
		result.addObject("personalRecord", personalRecord);
		result.addObject("photo", photo);

		return result;
	}

		@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
		public ModelAndView delete(final PersonalRecord personalRecord, final BindingResult binding) {
			ModelAndView result;
	
			try {
	
				this.personalRecordService.delete(personalRecord);
				result = new ModelAndView("redirect:/personalRecord/list.do");
				result.addObject("personalRecordId", personalRecord.getId());
	
			}
	
			catch (final Throwable oops) {
				result = this.createEditModelAndView(personalRecord, "personalRecord.commit.error");
			}
			return result;
		}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final PersonalRecord sp, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			System.out.println(binding.getAllErrors());
			res = this.createEditModelAndView(sp);
		} else
			try {
				this.personalRecordService.save(sp);
				res = new ModelAndView("redirect:/curriculum/list.do");
			} catch (final Throwable oops) {
				res = this.createEditModelAndView(sp, "personalRecord.commit.error");
			}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final PersonalRecord sp, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			System.out.println(binding.getAllErrors());
			res = this.createEditModelAndView(sp);
		} else
			try {
				this.personalRecordService.save(sp);
				res = new ModelAndView("redirect:/curriculum/list.do");
			} catch (final Throwable oops) {
				res = this.createEditModelAndView(sp, "personalRecord.commit.error");
			}
		return res;
	}

	protected ModelAndView createEditModelAndView(final PersonalRecord sp) {
		ModelAndView res;

		res = this.createEditModelAndView(sp, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(final PersonalRecord sp, final String messageCode) {
		ModelAndView res;

		if (sp.getId() == 0)
			res = new ModelAndView("personalRecord/create");
		else
			res = new ModelAndView("personalRecord/edit");
		res.addObject("personalRecord", sp);
		res.addObject("message", messageCode);

		return res;
	}

}
