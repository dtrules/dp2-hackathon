
package controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.CurriculumService;
import services.ProfessionalRecordService;
import domain.Curriculum;
import domain.ProfessionalRecord;

@Controller
@RequestMapping("/professionalRecord")
public class ProfessionalRecordController extends AbstractController {

	@Autowired
	private ProfessionalRecordService	professionalRecordService;

	@Autowired
	private CurriculumService	curriculumService;


	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam final int curriculumId) {

		ModelAndView res;

		final Curriculum curriculum = this.curriculumService.findOne(curriculumId);
		res = new ModelAndView("professionalRecord/list");
		res.addObject("professionalRecords", curriculum.getProfessionalRecords());
		res.addObject("requestURI", "professionalRecord/list.do");

		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int curriculumId) {
		ModelAndView res;
		ProfessionalRecord professionalRecord;

		professionalRecord = this.professionalRecordService.create(curriculumId);
		
		res = this.createEditModelAndView(professionalRecord,curriculumId);

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int professionalRecordId) {
		ModelAndView res;
		ProfessionalRecord professionalRecord;

		professionalRecord = this.professionalRecordService.findOneToEdit(professionalRecordId);
		res = this.editModelAndView(professionalRecord);

		res.addObject("professionalRecord", professionalRecord);

		return res;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final int professionalRecordId) {
		ModelAndView result;
		ProfessionalRecord professionalRecord;
		professionalRecord = this.professionalRecordService.findOne(professionalRecordId);
		result = new ModelAndView("professionalRecord/show");
		result.addObject("professionalRecord", professionalRecord);

		return result;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int professionalRecordId) {
		ModelAndView result;

		ProfessionalRecord res;

		res = this.professionalRecordService.findOne(professionalRecordId);

		this.professionalRecordService.delete(res);
		result = new ModelAndView("redirect:/curriculum/list.do");

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@RequestParam final int curriculumId, @Valid final ProfessionalRecord sp, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			System.out.println(binding.getAllErrors());
			res = this.createEditModelAndView(sp,curriculumId);
		} else
			try {
				this.professionalRecordService.save(sp, curriculumId);
				res = new ModelAndView("redirect:/curriculum/list.do");
				res.addObject("curriculumId", curriculumId);
			} catch (final Throwable oops) {
				res = this.createEditModelAndView(sp,curriculumId, "professionalRecord.commit.error");
			}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final ProfessionalRecord sp, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			System.out.println(binding.getAllErrors());
			res = this.editModelAndView(sp);
		} else
			try {
				this.professionalRecordService.saveEdit(sp);
				res = new ModelAndView("redirect:/curriculum/list.do");
			} catch (final Throwable oops) {
				res = this.editModelAndView(sp, "professionalRecord.commit.error");
			}
		return res;
	}

	protected ModelAndView createEditModelAndView(final ProfessionalRecord sp, final int curriculumId) {
		ModelAndView res;

		res = this.createEditModelAndView(sp,curriculumId, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(final ProfessionalRecord sp,final int curriculumId, final String messageCode) {
		ModelAndView res;

		res = new ModelAndView("professionalRecord/create");
		res.addObject("professionalRecord", sp);
		res.addObject("curriculumId", curriculumId);
		res.addObject("message", messageCode);

		return res;
	}

	protected ModelAndView editModelAndView(final ProfessionalRecord sp) {
		ModelAndView res;

		res = this.editModelAndView(sp, null);

		return res;
	}

	protected ModelAndView editModelAndView(final ProfessionalRecord sp, final String messageCode) {
		ModelAndView res;

		res = new ModelAndView("professionalRecord/edit");
		res.addObject("professionalRecord", sp);
		res.addObject("message", messageCode);

		return res;
	}

}
