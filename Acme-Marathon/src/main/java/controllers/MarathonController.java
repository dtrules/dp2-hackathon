
package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.CategoryService;
import services.MarathonService;
import services.OrganizerService;
import services.SponsorshipService;
import domain.Category;
import domain.Marathon;
import domain.Organizer;
import domain.Sponsorship;

@Controller
@RequestMapping("/marathon")
public class MarathonController extends AbstractController {

	@Autowired
	private MarathonService		marathonService;

	@Autowired
	private OrganizerService	organizerService;

	@Autowired
	private CategoryService		categoryService;

	@Autowired
	private ActorService		actorService;

	@Autowired
	private SponsorshipService	sponsorshipService;


	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView res;

		final Collection<Marathon> marathons = this.marathonService.findAll();

		res = new ModelAndView("marathon/list");
		res.addObject("marathons", marathons);
		res.addObject("requestURI", "marathon/list.do");

		return res;
	}

	@RequestMapping(value = "/organizer/listByOrganizer", method = RequestMethod.GET)
	public ModelAndView listByOrganizer() {

		ModelAndView res;

		final Organizer principal = this.organizerService.findByPrincipal();

		final Collection<Marathon> marathons = this.marathonService.listByOrganizer(principal.getId());

		res = new ModelAndView("marathon/organizer/listByOrganizer");
		res.addObject("marathons", marathons);
		res.addObject("requestURI", "marathon/organizer/listByOrganizer.do");

		return res;
	}

	@RequestMapping(value = "/organizer/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;
		Marathon p;

		p = this.marathonService.create();
		res = this.createEditModelAndView(p);

		return res;
	}

	@RequestMapping(value = "/organizer/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int marathonId) {
		ModelAndView res;
		Marathon p;

		p = this.marathonService.findOneToEdit(marathonId);
		res = this.createEditModelAndView(p);

		return res;
	}

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam final int marathonId) {
		ModelAndView result;
		final Sponsorship sponsorship = this.sponsorshipService.findRandomOneByMarathon(marathonId);
		Marathon p;
		p = this.marathonService.findOne(marathonId);

		result = new ModelAndView("marathon/show");
		result.addObject("marathon", p);
		result.addObject("marathonid", marathonId);
		result.addObject("sponsorship", sponsorship);

		return result;
	}

	@RequestMapping(value = "/organizer/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Marathon p, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			System.out.println(binding.getAllErrors());
			res = this.createEditModelAndView(p);
		} else
			try {
				this.marathonService.save(p);
				res = new ModelAndView("redirect:/marathon/organizer/listByOrganizer.do");
			} catch (final Throwable oops) {
				res = this.createEditModelAndView(p, "marathon.commit.error");
			}
		return res;
	}

	protected ModelAndView createEditModelAndView(final Marathon p) {
		ModelAndView res;

		res = this.createEditModelAndView(p, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(final Marathon p, final String messageCode) {
		ModelAndView res;
		final Collection<Category> categories = this.categoryService.findAll();

		if (p.getId() == 0)
			res = new ModelAndView("marathon/organizer/create");
		else
			res = new ModelAndView("marathon/organizer/edit");

		res.addObject("marathon", p);
		res.addObject("message", messageCode);
		res.addObject("categories", categories);

		return res;
	}

}
