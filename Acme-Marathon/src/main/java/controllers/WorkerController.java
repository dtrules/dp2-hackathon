
package controllers;

import domain.CreditCard;
import domain.Worker;
import forms.WorkerForm;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import security.UserAccountRepository;
import services.WorkerService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping("/worker")
public class WorkerController extends AbstractController {

	@Autowired
	private WorkerService workerService;

	//@Autowired
	//private ConfigurationsService	configurationsService;

	@Autowired
	private UserAccountRepository	userAccountRepository;


	@ExceptionHandler(TypeMismatchException.class)
	public ModelAndView handleMismatchException(final TypeMismatchException oops) {
		return new ModelAndView("redirect:/");
	}

	// List ------------------------------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Worker> bros;

		try {
			bros = this.workerService.findAll();
			result = new ModelAndView("worker/list");
			result.addObject("workers", bros);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	// Register ------------------------------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		WorkerForm bro;

		try {
			bro = new WorkerForm();
			result = new ModelAndView("worker/create");
			result.addObject("workerForm", bro);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	// Edit ------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView result;
		final Worker worker;

		try {
			worker = this.workerService.findByPrincipal();

			result = new ModelAndView("worker/edit");
			result.addObject("worker", worker);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}
	
	// Save the new worker ------------------------------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute("workerForm") @Valid final WorkerForm workerForm, final BindingResult binding) {
		ModelAndView result;
		Worker worker;

		worker = this.workerService.reconstruct(workerForm, binding);
		final CreditCard creditCard = worker.getCreditCard();
		//final Configurations configurations = this.configurationsService.getConfiguration();
		//final Collection<String> brands = configurations.getBrandNames();

		if (!workerForm.getUserAccount().getPassword().equals(workerForm.getConfirmPassword()))
			if (LocaleContextHolder.getLocale().getLanguage().toUpperCase().contains("ES")) {
				binding.addError(new FieldError("workerForm", "confirmPassword", workerForm.getConfirmPassword(), false, null, null, "Las contraseņas deben coincidir"));
				return this.createEditModelAndView(workerForm);
			}

			else {
				binding.addError(new FieldError("workerForm", "confirmPassword", workerForm.getConfirmPassword(), false, null, null, "Passwords must be the same"));
				return this.createEditModelAndView(workerForm);
			}

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = new ModelAndView("worker/create");
			result.addObject("workerForm", workerForm);
		}

		else
			try {

				this.workerService.save(worker);
				result = new ModelAndView("redirect:../security/login.do");
			} catch (final Throwable oops) {
				System.out.println(worker);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.createEditModelAndView(workerForm);

				if (oops instanceof DataIntegrityViolationException)
					result = this.createEditModelAndView(workerForm, "worker.duplicated.username");
				else
					result = this.createEditModelAndView(workerForm, "worker.registration.error");
			}
		return result;
	}
	// SAVE EDIT ------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@ModelAttribute("worker") final Worker prune, final BindingResult binding) {
		ModelAndView result;
		final Worker worker;

		worker = this.workerService.reconstruct(prune, binding);

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = this.editModelAndView(worker);
			result.addObject("worker", prune);
		}

		else
			try {
				this.workerService.save(worker);
				result = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				System.out.println(worker);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.editModelAndView(worker, "worker.registration.error");
			}
		return result;
	}

	// Ancillary methods -----------------------------------------------------------------------
	protected ModelAndView createEditModelAndView(final WorkerForm workerForm) {
		ModelAndView result;

		result = this.createEditModelAndView(workerForm, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final WorkerForm workerForm, final String message) {
		ModelAndView result;

		result = new ModelAndView("worker/create");
		result.addObject("workerForm", workerForm);
		result.addObject("message", message);

		return result;
	}

	protected ModelAndView editModelAndView(final Worker worker) {
		ModelAndView result;

		result = this.editModelAndView(worker, null);

		return result;
	}

	protected ModelAndView editModelAndView(final Worker worker, final String message) {
		ModelAndView result;

		result = new ModelAndView("worker/edit");
		result.addObject("worker", worker);
		result.addObject("message", message);

		return result;
	}

	private ModelAndView forbiddenOpperation() {
		return new ModelAndView("redirect:/");
	}
}
