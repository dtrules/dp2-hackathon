/*
 * AdministratorController.java
 * 
 * Copyright (C) 2019 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AdministratorService;
import services.RunnerService;
import domain.Administrator;
import domain.CreditCard;
import domain.Runner;
import forms.AdministratorForm;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public AdministratorController() {
		super();
	}


	// Services -------------------------------------------------------------
	@Autowired
	private AdministratorService	administratorService;

	@Autowired
	private RunnerService			runnerService;


	// Register ------------------------------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		AdministratorForm bro;

		try {
			bro = new AdministratorForm();
			result = new ModelAndView("administrator/create");
			result.addObject("administratorForm", bro);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	// Edit ------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView result;
		final Administrator admin;

		try {
			admin = this.administratorService.findByPrincipal();

			result = new ModelAndView("admin/edit");
			result.addObject("admin", admin);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
			System.out.println(oops.getClass());
			System.out.println(oops.getCause());
			result = this.forbiddenOpperation();
		}

		return result;
	}

	// Save the new administrator ------------------------------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute("administratorForm") @Valid final AdministratorForm administratorForm, final BindingResult binding) {
		ModelAndView result;
		Administrator administrator;

		administrator = this.administratorService.reconstruct(administratorForm, binding);
		final CreditCard creditCard = administrator.getCreditCard();
		//final Configurations configurations = this.configurationsService.getConfiguration();
		//final Collection<String> brands = configurations.getBrandNames();

		if (!administratorForm.getUserAccount().getPassword().equals(administratorForm.getConfirmPassword()))
			if (LocaleContextHolder.getLocale().getLanguage().toUpperCase().contains("ES")) {
				binding.addError(new FieldError("administratorForm", "confirmPassword", administratorForm.getConfirmPassword(), false, null, null, "Las contraseņas deben coincidir"));
				return this.createEditModelAndView(administratorForm);
			}

			else {
				binding.addError(new FieldError("administratorForm", "confirmPassword", administratorForm.getConfirmPassword(), false, null, null, "Passwords must be the same"));
				return this.createEditModelAndView(administratorForm);
			}

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = new ModelAndView("administrator/create");
			result.addObject("administratorForm", administratorForm);
		}

		else
			try {

				this.administratorService.save(administrator);
				result = new ModelAndView("redirect:../security/login.do");
			} catch (final Throwable oops) {
				System.out.println(administrator);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.createEditModelAndView(administratorForm);

				if (oops instanceof DataIntegrityViolationException)
					result = this.createEditModelAndView(administratorForm, "administrator.duplicated.username");
				else
					result = this.createEditModelAndView(administratorForm, "administrator.registration.error");
			}
		return result;
	}
	// SAVE EDIT ------------------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@ModelAttribute("administrator") final Administrator prune, final BindingResult binding) {
		ModelAndView result;
		final Administrator administrator;

		administrator = this.administratorService.reconstruct(prune, binding);

		if (binding.hasErrors()) {
			final List<ObjectError> errors = binding.getAllErrors();
			for (final ObjectError e : errors)
				System.out.println(e.toString());

			result = this.editModelAndView(administrator);
			result.addObject("administrator", prune);
		}

		else
			try {
				this.administratorService.save(administrator);
				result = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				System.out.println(administrator);
				System.out.println(oops.getMessage());
				System.out.println(oops.getClass());
				System.out.println(oops.getCause());
				result = this.editModelAndView(administrator, "administrator.registration.error");
			}
		return result;
	}

	//Score
	@RequestMapping(value = "/score", method = RequestMethod.GET)
	public ModelAndView obtainScore(@RequestParam final int runnerId) {
		ModelAndView result;

		final Runner r = this.runnerService.findOne(runnerId);
		final Double score = this.administratorService.obtainScore(runnerId);
		r.setScore(score);

		this.runnerService.save(r);

		result = new ModelAndView("redirect:../runner/list.do");

		return result;
	}

	// Dashboard -----------------------------------------------------------
	@RequestMapping("/dashboard")
	public ModelAndView dashboard() {
		final ModelAndView result;

		// Queries
		final Double avgMarathonsRunner = this.administratorService.avgMarathonsRunner();
		final int maxMarathonsRunner = this.administratorService.maxMarathonsRunner();
		final int minMarathonsRunner = this.administratorService.minMarathonsRunner();
		final Double stddevMarathonsRunner = this.administratorService.stddevMarathonsRunner();

		final Double avgMarathonsOrganizer = this.administratorService.avgMarathonsOrganizer();
		final Double maxMarathonsOrganizer = this.administratorService.maxMarathonsOrganizer();
		final Double minMarathonsOrganizer = this.administratorService.minMarathonsOrganizer();
		final Double stddevMarathonsOrganizer = this.administratorService.stddevMarathonsOrganizer();

		final Double avgApplicationsMarathon = this.administratorService.avgApplicationsMarathon();
		final Double maxApplicationsMarathon = this.administratorService.maxApplicationsMarathon();
		final Double minApplicationsMarathon = this.administratorService.minApplicationsMarathon();
		final Double stddevApplicationsMarathon = this.administratorService.stddevApplicationsMarathon();

		final Double avgPriceMarathon = this.administratorService.avgPriceMarathon();
		final Double maxPriceMarathon = this.administratorService.maxPriceMarathon();
		final Double minPriceMarathon = this.administratorService.minPriceMarathon();
		final Double stddevPriceMarathon = this.administratorService.stddevPriceMarathon();

		final Double ratioPending = this.administratorService.ratioPending();
		final Double ratioAccepted = this.administratorService.ratioAccepted();
		final Double ratioRejected = this.administratorService.ratioRejected();

		final Double avgComplaintMarathon = this.administratorService.avgComplaintsMarathon();
		final int maxComplaintMarathon = this.administratorService.maxComplaintsMarathon();
		final int minComplaintMarathon = this.administratorService.minComplaintsMarathon();
		final Double stddevComplaintMarathon = this.administratorService.stddevComplaintsMarathon();

		final Double avgComplaintRunner = this.administratorService.avgComplaintsRunner();
		final int maxComplaintRunner = this.administratorService.maxComplaintsRunner();
		final int minComplaintRunner = this.administratorService.minComplaintsRunner();
		final Double stddevComplaintRunner = this.administratorService.stddevComplaintsRunner();

		final Collection<Object> top3Runners = this.administratorService.top3Runners();
		final Collection<Object> top3Organizers = this.administratorService.top3Organizers();
		final Double ratioMarathons = this.administratorService.ratioMarathons();

		final Collection<Object> top5Marathons = this.administratorService.top5MarathonsTermsSponsorships();
		final Double avgSponsorshipSponsor = this.administratorService.avgSponsorshipsSponsor();
		final int maxSponsorshipSponsor = this.administratorService.maxSponsorshipsSponsor();
		final int minSponsorshipSponsor = this.administratorService.minSponsorshipsSponsor();
		final Double stddevSponsorshipSponsor = this.administratorService.stddevSponsorshipsSponsor();
		final Collection<Object> top5Sponsors = this.administratorService.top5SponsorsTermsSponsorships();

		result = new ModelAndView("administrator/dashboard");

		result.addObject("avgPositions", avgMarathonsRunner);
		result.addObject("maxPositions", maxMarathonsRunner);
		result.addObject("minPositions", minMarathonsRunner);
		result.addObject("stddevPositions", stddevMarathonsRunner);

		result.addObject("avgAppRookie", avgMarathonsOrganizer);
		result.addObject("maxAppRookie", maxMarathonsOrganizer);
		result.addObject("minAppRookie", minMarathonsOrganizer);
		result.addObject("stddevAppRookie", stddevMarathonsOrganizer);

		result.addObject("avgSalary", avgApplicationsMarathon);
		result.addObject("maxSalary", maxApplicationsMarathon);
		result.addObject("minSalary", minApplicationsMarathon);
		result.addObject("stddevSalary", stddevApplicationsMarathon);

		result.addObject("avgCurriculas", avgPriceMarathon);
		result.addObject("maxCurriculas", maxPriceMarathon);
		result.addObject("minCurriculas", minPriceMarathon);
		result.addObject("stddevCurriculas", stddevPriceMarathon);

		result.addObject("avgResults", ratioPending);
		result.addObject("maxResults", ratioAccepted);
		result.addObject("minResults", ratioRejected);

		result.addObject("avgComplaintMarathon", avgComplaintMarathon);
		result.addObject("maxComplaintMarathon", maxComplaintMarathon);
		result.addObject("minComplaintMarathon", minComplaintMarathon);
		result.addObject("stddevComplaintMarathon", stddevComplaintMarathon);

		result.addObject("avgComplaintRunner", avgComplaintRunner);
		result.addObject("maxComplaintRunner", maxComplaintRunner);
		result.addObject("minComplaintRunner", minComplaintRunner);
		result.addObject("stddevComplaintRunner", stddevComplaintRunner);

		result.addObject("top3Runners", top3Runners);
		result.addObject("top3Organizers", top3Organizers);
		result.addObject("ratioMarathons", ratioMarathons);

		result.addObject("top5Marathons", top5Marathons);
		result.addObject("avgSponsorshipSponsor", avgSponsorshipSponsor);
		result.addObject("maxSponsorshipSponsor", maxSponsorshipSponsor);
		result.addObject("minSponsorshipSponsor", minSponsorshipSponsor);
		result.addObject("stddevSponsorshipSponsor", stddevSponsorshipSponsor);
		result.addObject("top5Sponsors", top5Sponsors);

		return result;
	}

	private ModelAndView forbiddenOpperation() {
		return new ModelAndView("redirect:/");
	}

	// Ancillary methods -----------------------------------------------------------------------
	protected ModelAndView createEditModelAndView(final AdministratorForm adminForm) {
		ModelAndView result;

		result = this.createEditModelAndView(adminForm, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final AdministratorForm adminForm, final String message) {
		ModelAndView result;

		result = new ModelAndView("administrator/create");
		result.addObject("administratorForm", adminForm);
		result.addObject("message", message);

		return result;
	}

	protected ModelAndView editModelAndView(final Administrator admin) {
		ModelAndView result;

		result = this.editModelAndView(admin, null);

		return result;
	}

	protected ModelAndView editModelAndView(final Administrator admin, final String message) {
		ModelAndView result;

		result = new ModelAndView("administrator/edit");
		result.addObject("admin", admin);
		result.addObject("message", message);

		return result;
	}

}
