
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.PersonalRecordRepository;
import domain.Actor;
import domain.Curriculum;
import domain.PersonalRecord;
import domain.Worker;

@Service
@Transactional
public class PersonalRecordService {

	@Autowired
	private PersonalRecordRepository	personalRecordRepository;

	@Autowired
	private ActorService				actorService;

	@Autowired
	private WorkerService				workerService;

	@Autowired
	private CurriculumService			curriculumService;


	public PersonalRecordService() {
		super();
	}

	public PersonalRecord create(final Integer curriculumId) {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		final PersonalRecord res = new PersonalRecord();
		final Curriculum curriculum = this.curriculumService.findOne(curriculumId);
		curriculum.setPersonalRecord(res);
		this.curriculumService.save(curriculum);
		Assert.notNull(res);

		return res;
	}

	public PersonalRecord findOne(final int personalRecordId) {
		Assert.isTrue(personalRecordId > 0);

		final PersonalRecord res = this.personalRecordRepository.findOne(personalRecordId);

		Assert.notNull(res);

		return res;
	}

	public PersonalRecord save(final PersonalRecord personalRecord) {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		this.checkPersonalRecord(personalRecord);

		final PersonalRecord res = this.personalRecordRepository.save(personalRecord);

		return res;
	}

	public void delete(final PersonalRecord personalRecord) {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		Assert.notNull(personalRecord);
		Assert.isTrue(personalRecord.getId() > 0);

		this.personalRecordRepository.delete(personalRecord);
	}

	public Collection<PersonalRecord> findAll() {

		Collection<PersonalRecord> result;

		result = this.personalRecordRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void flush() {
		this.personalRecordRepository.flush();
	}

	public void checkPersonalRecord(final PersonalRecord personalRecord) {
		Boolean res = true;

		if (personalRecord.getName() == null || personalRecord.getPhoto() == null || personalRecord.getLinkedInProfile() == null || personalRecord.getPhoneNumber() == null || personalRecord.getEmail() == null)
			res = false;

		Assert.isTrue(res);
	}

	public PersonalRecord findOneToEdit(final int id) {
		Worker principal;

		// Principal must be a Company
		principal = this.workerService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		final PersonalRecord result = this.personalRecordRepository.findOne(id);

		final Curriculum curriculum = this.curriculumService.findCurriculumByPersonalRecordId(id);

		final Worker worker = curriculum.getWorker();
		Assert.notNull(result);

		// Debe ser el mismo Company que al que pertenece la Position
		Assert.isTrue(principal.equals(worker));

		return result;

	}
}
