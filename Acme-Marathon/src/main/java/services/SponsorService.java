
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.SponsorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountRepository;
import domain.CreditCard;
import domain.SocialProfile;
import domain.Sponsor;
import forms.SponsorForm;

@Service
@Transactional
public class SponsorService {

	// Manage Repositories
	@Autowired
	private SponsorRepository		sponsorRepository;

	@Autowired(required = false)
	private Validator				validator;

	@Autowired
	private FolderService			folderService;

	@Autowired
	private UserAccountRepository	userAccountRepository;


	//CRUD methods

	public Sponsor create() {
		final Sponsor result = new Sponsor();
		final CreditCard creditCard = new CreditCard();
		final UserAccount userAccount = new UserAccount();
		final Collection<Authority> authorities = new ArrayList<Authority>();
		final Authority authority = new Authority();
		final Collection<SocialProfile> socialProfiles = new ArrayList<SocialProfile>();

		authority.setAuthority(Authority.SPONSOR);
		authorities.add(authority);
		userAccount.setAuthorities(authorities);

		result.setUserAccount(userAccount);
		result.setCreditCard(creditCard);
		result.setSocialProfiles(socialProfiles);

		return result;

	}

	public Sponsor save(final Sponsor sponsor) {
		Sponsor res;
		Assert.notNull(sponsor);
		Assert.isTrue(this.checkDate(sponsor.getCreditCard().getExpiryMonth(), sponsor.getCreditCard().getExpiryYear()));

		if (sponsor.getId() == 0) {
			final Md5PasswordEncoder encoder = new Md5PasswordEncoder();
			final UserAccount userAccount = sponsor.getUserAccount();
			final String password = userAccount.getPassword();
			final String hashedPassword = encoder.encodePassword(password, null);
			userAccount.setPassword(hashedPassword);

			final UserAccount ua = this.userAccountRepository.save(userAccount);
			sponsor.setUserAccount(ua);
		}
		res = this.sponsorRepository.save(sponsor);

		if (sponsor.getId() == 0)
			this.folderService.generateFolders(res.getId());

		return res;
	}

	public Sponsor findOne(final int sponsorId) {
		final Sponsor result = this.sponsorRepository.findOne(sponsorId);
		Assert.notNull(result);

		return result;
	}

	public Sponsor findOneNoAssert(final int sponsorId) {
		final Sponsor result = this.sponsorRepository.findOne(sponsorId);

		return result;
	}

	public Collection<Sponsor> findAll() {
		final Collection<Sponsor> result = this.sponsorRepository.findAll();
		Assert.notNull(result);
		Assert.notEmpty(result);

		return result;
	}

	public void delete(final Sponsor sponsor) {
		Assert.notNull(sponsor);

		this.sponsorRepository.delete(sponsor);
	}

	/*** Reconstruct object, check validity and update binding ***/
	public Sponsor reconstruct(final SponsorForm sponsorForm, final BindingResult binding) {
		final Sponsor result = this.create();
		result.getUserAccount().setPassword(sponsorForm.getUserAccount().getPassword());
		result.getUserAccount().setUsername(sponsorForm.getUserAccount().getUsername());

		result.getCreditCard().setBrandName(sponsorForm.getCreditCard().getBrandName());
		result.getCreditCard().setCvv(sponsorForm.getCreditCard().getCvv());
		result.getCreditCard().setExpiryMonth(sponsorForm.getCreditCard().getExpiryMonth());
		result.getCreditCard().setExpiryYear(sponsorForm.getCreditCard().getExpiryYear());
		result.getCreditCard().setHolderName(sponsorForm.getCreditCard().getHolderName());
		result.getCreditCard().setNumber(sponsorForm.getCreditCard().getNumber());

		result.setAddress(sponsorForm.getAddress());
		result.setEmail(sponsorForm.getEmail());
		result.setName(sponsorForm.getName());
		result.setPhoneNumber(sponsorForm.getPhoneNumber());
		result.setPhoto(sponsorForm.getPhoto());
		result.setSurname(sponsorForm.getSurname());
		result.setIsBanned(false);
		result.setIsSuspicious(false);

		this.validator.validate(result, binding);

		return result;
	}

	public Sponsor reconstruct(final Sponsor sponsor, final BindingResult binding) {
		Sponsor result = new Sponsor();
		final Sponsor res = new Sponsor();

		if (sponsor.getId() == 0)
			result = sponsor;
		else {
			result = this.findOne(sponsor.getId());
			final CreditCard creditCard = sponsor.getCreditCard();
			res.setAddress(sponsor.getAddress());
			res.setEmail(sponsor.getEmail());
			res.setName(sponsor.getName());
			res.setPhoneNumber(sponsor.getPhoneNumber());
			res.setPhoto(sponsor.getPhoto());
			res.setSurname(sponsor.getSurname());
			res.setId(result.getId());
			res.setVersion(result.getVersion());
			res.setUserAccount(result.getUserAccount());
			res.setSocialProfiles(result.getSocialProfiles());

			creditCard.setBrandName(sponsor.getCreditCard().getBrandName());
			creditCard.setCvv(sponsor.getCreditCard().getCvv());
			creditCard.setExpiryMonth(sponsor.getCreditCard().getExpiryMonth());
			creditCard.setExpiryYear(sponsor.getCreditCard().getExpiryYear());
			creditCard.setHolderName(sponsor.getCreditCard().getHolderName());
			creditCard.setNumber(sponsor.getCreditCard().getNumber());
			res.setCreditCard(creditCard);
		}
		this.validator.validate(res, binding);

		return res;
	}

	// Other business methods
	public Sponsor findByPrincipal() {
		Sponsor result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public Sponsor findByUserAccount(final UserAccount userAccount) {
		Assert.notNull(userAccount);

		Sponsor result;

		result = this.sponsorRepository.findByUserAccountId(userAccount.getId());

		return result;
	}

	public Sponsor findByUserName(final String username) {
		Assert.notNull(username);

		Sponsor result;

		result = this.sponsorRepository.findByUserName(username);

		return result;
	}

	public void checkIfSponsor() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.SPONSOR))
				res = true;
		Assert.isTrue(res);
	}

	public void flush() {
		this.sponsorRepository.flush();
	}

	public Boolean checkDate(final int expiryMonth, final int expiryYear) {
		Boolean res = true;
		final Date date = new Date();
		final int month = date.getMonth();
		final int year = date.getYear() % 100;
		if (expiryYear == year)
			if (month > expiryMonth)
				res = false;

		return res;
	}

}
