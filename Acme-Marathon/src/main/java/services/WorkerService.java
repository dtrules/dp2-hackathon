
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.WorkerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountRepository;
import domain.CreditCard;
import domain.Curriculum;
import domain.SocialProfile;
import domain.Worker;
import forms.WorkerForm;

@Service
@Transactional
public class WorkerService {

	// Manage Repository
	@Autowired
	private WorkerRepository		workerRepository;

	@Autowired(required = false)
	private Validator				validator;

	@Autowired
	private UserAccountRepository	userAccountRepository;

	@Autowired
	private CreditCardService		creditCardService;

	@Autowired
	private FolderService			folderService;

	@Autowired
	private CurriculumService		curriculumService;


	// CRUD methods
	public Worker create() {
		final Worker result = new Worker();
		final CreditCard creditCard = new CreditCard();
		final UserAccount userAccount = new UserAccount();
		final Collection<Authority> authorities = new ArrayList<Authority>();
		final Authority authority = new Authority();
		final Collection<SocialProfile> profiles = new ArrayList<SocialProfile>();

		authority.setAuthority(Authority.WORKER);
		authorities.add(authority);
		userAccount.setAuthorities(authorities);

		result.setUserAccount(userAccount);
		result.setCreditCard(creditCard);
		result.setSocialProfiles(profiles);

		return result;

	}

	public Worker save(final Worker worker) {
		Worker res;
		Assert.notNull(worker);
		Assert.isTrue(this.checkDate(worker.getCreditCard().getExpiryMonth(), worker.getCreditCard().getExpiryYear()));

		if (worker.getId() == 0) {
			final Md5PasswordEncoder encoder = new Md5PasswordEncoder();
			final UserAccount userAccount = worker.getUserAccount();
			final String password = userAccount.getPassword();
			final String hashedPassword = encoder.encodePassword(password, null);
			userAccount.setPassword(hashedPassword);
			final UserAccount ua = this.userAccountRepository.save(userAccount);
			worker.setUserAccount(ua);
		}

		res = this.workerRepository.save(worker);

		if (worker.getId() == 0)
			this.folderService.generateFolders(res.getId());

		return res;

	}

	public Worker findOne(final int workerId) {
		final Worker result = this.workerRepository.findOne(workerId);
		//Assert.notNull(result);

		return result;
	}

	public Worker findOneNoAssert(final int workerId) {
		final Worker result = this.workerRepository.findOne(workerId);

		return result;
	}

	public Collection<Worker> findAll() {
		final Collection<Worker> result = this.workerRepository.findAll();
		Assert.notNull(result);
		Assert.notEmpty(result);

		return result;
	}

	public void delete(final Worker worker) {
		Assert.notNull(worker);

		this.workerRepository.delete(worker);
	}

	// Other business methods
	public Worker findByPrincipal() {
		Worker result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	/*** Reconstruct object, check validity and update binding ***/
	public Worker reconstruct(final WorkerForm workerForm, final BindingResult binding) {
		final Worker result = this.create();

		result.getCreditCard().setBrandName(workerForm.getCreditCard().getBrandName());
		result.getCreditCard().setCvv(workerForm.getCreditCard().getCvv());
		result.getCreditCard().setExpiryMonth(workerForm.getCreditCard().getExpiryMonth());
		result.getCreditCard().setExpiryYear(workerForm.getCreditCard().getExpiryYear());
		result.getCreditCard().setHolderName(workerForm.getCreditCard().getHolderName());
		result.getCreditCard().setNumber(workerForm.getCreditCard().getNumber());
		result.getUserAccount().setPassword(workerForm.getUserAccount().getPassword());
		result.getUserAccount().setUsername(workerForm.getUserAccount().getUsername());
		//		Assert.isTrue(workerForm.getUserAccount().getPassword() == workerForm.getConfirmPassword());

		result.setAddress(workerForm.getAddress());
		result.setEmail(workerForm.getEmail());
		result.setName(workerForm.getName());
		result.setPhoneNumber(workerForm.getPhoneNumber());
		result.setPhoto(workerForm.getPhoto());
		result.setSurname(workerForm.getSurname());
		result.setIsBanned(false);

		this.validator.validate(result, binding);

		return result;
	}

	public Worker reconstruct(final Worker worker, final BindingResult binding) {
		Worker result = new Worker();
		final Worker res = new Worker();

		if (worker.getId() == 0)
			result = worker;
		else {
			result = this.findOne(worker.getId());
			final CreditCard creditCard = worker.getCreditCard();
			res.setAddress(worker.getAddress());
			res.setEmail(worker.getEmail());
			res.setName(worker.getName());
			res.setPhoneNumber(worker.getPhoneNumber());
			res.setPhoto(worker.getPhoto());
			res.setSurname(worker.getSurname());
			res.setId(result.getId());
			res.setVersion(result.getVersion());
			res.setUserAccount(result.getUserAccount());
			res.setSocialProfiles(worker.getSocialProfiles());

			creditCard.setBrandName(worker.getCreditCard().getBrandName());
			creditCard.setCvv(worker.getCreditCard().getCvv());
			creditCard.setExpiryMonth(worker.getCreditCard().getExpiryMonth());
			creditCard.setExpiryYear(worker.getCreditCard().getExpiryYear());
			creditCard.setHolderName(worker.getCreditCard().getHolderName());
			creditCard.setNumber(worker.getCreditCard().getNumber());
			res.setCreditCard(creditCard);
		}
		this.validator.validate(res, binding);

		return res;
	}
	/************************************************************************************************/

	public Worker findByUserAccount(final UserAccount userAccount) {
		Assert.notNull(userAccount);

		Worker result;

		result = this.workerRepository.findByUserAccountId(userAccount.getId());

		return result;
	}

	public void checkIfWorker() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.WORKER))
				res = true;
		Assert.isTrue(res);
	}

	public Worker findOneByUsername(final String username) {
		Assert.notNull(username);

		return this.workerRepository.findByUserName(username);
	}

	public void flush() {
		this.workerRepository.flush();
	}

	public Boolean checkDate(final int expiryMonth, final int expiryYear) {
		Boolean res = true;
		final Date date = new Date();
		final int month = date.getMonth();
		final int year = date.getYear() % 100;
		if (expiryYear == year)
			if (month > expiryMonth)
				res = false;

		return res;
	}

	public Worker findWorkerByCurriculaId(final int curriculumId) {
		final Curriculum curriculum = this.curriculumService.findOne(curriculumId);
		final Worker worker = curriculum.getWorker();
		return worker;
	}
}
