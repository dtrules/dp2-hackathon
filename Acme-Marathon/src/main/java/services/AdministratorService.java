
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.AdministratorRepository;
import repositories.RunnerApplicationRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountRepository;
import domain.Actor;
import domain.Administrator;
import domain.CreditCard;
import domain.Organizer;
import domain.Runner;
import domain.RunnerApplication;
import domain.SocialProfile;
import domain.Worker;
import forms.AdministratorForm;

@Service
@Transactional
public class AdministratorService {

	@Autowired(required = false)
	private Validator				validator;

	// Manage Repository
	@Autowired
	private AdministratorRepository	adminRepository;

	// Supporting services
	@Autowired
	private CustomizationService	customizationService;

	@Autowired
	private ActorService			actorService;

	@Autowired
	private FolderService			folderService;

	@Autowired
	private MessageService			messageService;
	
	@Autowired
	private RunnerApplicationRepository	runnerApplicationRepository;

	@Autowired
	private UserAccountRepository	userAccountRepository;


	/*************************************
	 * CRUD methods
	 ********************************/
	public Administrator create() {
		// Initialice
		final UserAccount userAccount = new UserAccount();
		final CreditCard creditCard = new CreditCard();
		final Collection<Authority> authorities = new ArrayList<Authority>();
		final Authority authority = new Authority();
		final Collection<SocialProfile> socialProfiles = new ArrayList<SocialProfile>();

		authority.setAuthority(Authority.ADMIN);
		authorities.add(authority);
		userAccount.setAuthorities(authorities);

		final Administrator admin = new Administrator();
		admin.setUserAccount(userAccount);
		admin.setCreditCard(creditCard);
		admin.setSocialProfiles(socialProfiles);

		return admin;
	}

	public Collection<Administrator> findAll() {
		final Collection<Administrator> result = this.adminRepository.findAll();
		Assert.notNull(result);
		return result;
	}

	public Administrator findOne(final int adminID) {
		final Administrator result = this.adminRepository.findOne(adminID);
		Assert.notNull(result);
		return result;
	}

	public Administrator save(final Administrator admin) {
		Assert.notNull(admin);
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		Assert.isTrue(this.checkDate(admin.getCreditCard().getExpiryMonth(), admin.getCreditCard().getExpiryYear()));

		if (admin.getId() == 0) {

			final Md5PasswordEncoder encoder = new Md5PasswordEncoder();
			final UserAccount userAccount = admin.getUserAccount();
			final String password = userAccount.getPassword();
			final String hashedPassword = encoder.encodePassword(password, null);
			userAccount.setPassword(hashedPassword);
			final UserAccount ua = this.userAccountRepository.save(userAccount);
			admin.setUserAccount(ua);

			if (!admin.getPhoneNumber().startsWith("+")) {
				final String countryCode = this.customizationService.findCustomization().getCountryCode();
				final String phoneNumer = admin.getPhoneNumber();
				admin.setPhoneNumber(countryCode.concat(phoneNumer));
			}
		} else {
			if (!admin.getPhoneNumber().startsWith("+")) {
				final String countryCode = this.customizationService.findCustomization().getCountryCode();
				final String phoneNumer = admin.getPhoneNumber();
				admin.setPhoneNumber(countryCode.concat(phoneNumer));
			}
			;
		}

		final Administrator result = this.adminRepository.save(admin);

		this.folderService.generateFolders(result.getId());

		return result;

	}

	public void delete(final Administrator admin) {
		Assert.notNull(admin);
		Assert.isTrue(admin.getId() != 0);
		this.adminRepository.delete(admin);
	}

	/*************************************
	 * Other business methods
	 ********************************/
	public Administrator findByPrincipal() {
		Administrator result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public Administrator findByUserAccount(final UserAccount userAccount) {
		Assert.notNull(userAccount);

		Administrator result;

		result = this.adminRepository.findByUserAccountId(userAccount.getId());

		return result;
	}

	// 12.1 Create user accounts for new administrators---------------------------------------------------
	public Administrator registerNewAdmin(final Administrator admin) {
		Administrator principal;

		// Make sure that the principal is an Admin
		principal = this.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);

		// Check admin is not null
		Assert.notNull(admin);

		// Saves admin in the databese
		return this.adminRepository.save(admin);
	}

	// 28.2 Spammers actors--------------------------------------------------------------------

	public Collection<Actor> getSpammers() {
		Actor principal;

		// Make sure that the principal is an Admin
		principal = this.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);

		return this.adminRepository.findSuspicious();
	}

	// 28.5 Ban an actor
	// ----------------------------------------------------------------

	public Actor ban(final Actor actor) {
		Assert.notNull(actor);
		Assert.isTrue(actor.getIsSuspicious());
		Assert.isTrue(actor.getIsBanned() == false);

		// Make sure that the principal is an Admin
		final Object principal = this.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);

		actor.setIsBanned(true);

		return this.actorService.save(actor);

	}

	// 28.6 Unbans an actor, which means that his or her user account is
	// re-activated

	public Actor unban(final Actor actor) {
		Assert.notNull(actor);
		Assert.isTrue(actor.getIsBanned());

		// Make sure that the principal is an Admin
		final Object principal = this.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);

		actor.setIsBanned(false);

		return this.actorService.save(actor);

	}

	public void flush() {
		this.adminRepository.flush();
	}

	public Administrator reconstruct(final AdministratorForm administratorForm, final BindingResult binding) {
		final Administrator result = this.create();
		result.getUserAccount().setPassword(administratorForm.getUserAccount().getPassword());
		result.getUserAccount().setUsername(administratorForm.getUserAccount().getUsername());

		result.getCreditCard().setBrandName(administratorForm.getCreditCard().getBrandName());
		result.getCreditCard().setCvv(administratorForm.getCreditCard().getCvv());
		result.getCreditCard().setExpiryMonth(administratorForm.getCreditCard().getExpiryMonth());
		result.getCreditCard().setExpiryYear(administratorForm.getCreditCard().getExpiryYear());
		result.getCreditCard().setHolderName(administratorForm.getCreditCard().getHolderName());
		result.getCreditCard().setNumber(administratorForm.getCreditCard().getNumber());

		result.setAddress(administratorForm.getAddress());
		result.setEmail(administratorForm.getEmail());
		result.setName(administratorForm.getName());
		result.setPhoneNumber(administratorForm.getPhoneNumber());
		result.setPhoto(administratorForm.getPhoto());
		result.setSurname(administratorForm.getSurname());
		result.setIsBanned(false);

		this.validator.validate(result, binding);

		return result;
	}
	
	public Administrator reconstruct(final Administrator admin, final BindingResult binding) {
		Administrator result = new Administrator();
		final Administrator res = new Administrator();

		if (admin.getId() == 0)
			result = admin;
		else {
			result = this.findOne(admin.getId());
			final CreditCard creditCard = admin.getCreditCard();
			res.setAddress(admin.getAddress());
			res.setEmail(admin.getEmail());
			res.setName(admin.getName());
			res.setPhoneNumber(admin.getPhoneNumber());
			res.setPhoto(admin.getPhoto());
			res.setSurname(admin.getSurname());
			res.setId(result.getId());
			res.setVersion(result.getVersion());
			res.setUserAccount(result.getUserAccount());
			res.setSocialProfiles(admin.getSocialProfiles());

			creditCard.setBrandName(admin.getCreditCard().getBrandName());
			creditCard.setCvv(admin.getCreditCard().getCvv());
			creditCard.setExpiryMonth(admin.getCreditCard().getExpiryMonth());
			creditCard.setExpiryYear(admin.getCreditCard().getExpiryYear());
			creditCard.setHolderName(admin.getCreditCard().getHolderName());
			creditCard.setNumber(admin.getCreditCard().getNumber());
			res.setCreditCard(creditCard);
		}
		this.validator.validate(res, binding);

		return res;
	}

	public Boolean checkDate(final int expiryMonth, final int expiryYear) {
		Boolean res = true;
		final Date date = new Date();
		final int month = date.getMonth();
		final int year = date.getYear() % 100;
		if (expiryYear == year)
			if (month > expiryMonth)
				res = false;

		return res;
	}
	
	//Score
	public Double obtainScore(final int runnerId) {
		// Make sure that the principal is an Admin
		final Actor principal = this.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		
		double res = 0.0;
		
		Collection<RunnerApplication> col;
		
		col = this.runnerApplicationRepository.getRunnerApplicationsRunnedByRunner(runnerId);
		
		for(RunnerApplication ra : col){
			
			int puntos = 0;
			
			if (ra.getPosition().equals(10)){
				puntos = 1;
			}
			else if (ra.getPosition().equals(9)){
				puntos = 2;
			}
			else if (ra.getPosition().equals(8)){
				puntos = 3;
			}
			else if (ra.getPosition().equals(7)){
				puntos = 4;
			}
			else if (ra.getPosition().equals(6)){
				puntos = 5;
			}
			else if (ra.getPosition().equals(5)){
				puntos = 6;
			}
			else if (ra.getPosition().equals(4)){
				puntos = 7;
			}
			else if (ra.getPosition().equals(3)){
				puntos = 8;
			}
			else if (ra.getPosition().equals(2)){
				puntos = 9;
			}
			else if (ra.getPosition().equals(1)){
				puntos = 10;
			}
			else{
				puntos = 0;
			}
			
			res = res + puntos;
		}
		
		if(col.isEmpty() || col.size() == 0){
			res = 0;
		}
		
		return res;
	}
	
	// DASHBOARD -----------------

	//NIVEL B

	public Double avgComplaintsMarathon() {
		Actor principal;

		// Principal must be an admin
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.avgComplaintsMarathon();
	}

	public Integer maxComplaintsMarathon() {
		Actor principal;

		// Principal must be an admin
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.maxComplaintsMarathon();
	}

	public Integer minComplaintsMarathon() {
		Actor principal;

		// Principal must be an admin
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.minComplaintsMarathon();
	}

	public Double stddevComplaintsMarathon() {
		Actor principal;

		// Principal must be an admin
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.stddevComplaintsMarathon();
	}

	public Double avgComplaintsRunner() {
		Actor principal;

		// Principal must be an admin
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.avgComplaintsRunner();
	}

	public Integer maxComplaintsRunner() {
		Actor principal;

		// Principal must be an admin
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.maxComplaintsRunner();
	}

	public Integer minComplaintsRunner() {
		Actor principal;

		// Principal must be an admin
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.minComplaintsRunner();
	}

	public Double stddevComplaintsRunner() {
		Actor principal;

		// Principal must be an admin
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.stddevComplaintsRunner();
	}

	public Collection<Object> top3Runners() {
		Actor principal;

		// Principal must be an admin
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.top3RunnersTermsOfComplaints();

	}

	public Collection<Object> top3Organizers() {
		Actor principal;

		// Principal must be an admin
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.top3OrganizersTermsOfComplaints();

	}

	public Double ratioMarathons() {
		Actor principal;

		// Principal must be an admin
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.ratioMarathonsComplaint();
	}

	//NIVEL C

	public Double avgMarathonsRunner() {
		Actor principal;

		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.avgMarathonsRunner();
	}

	public int maxMarathonsRunner() {
		Actor principal;

		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.maxMarathonsRunner();
	}
	public int minMarathonsRunner() {
		Actor principal;

		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.minMarathonsRunner();
	}

	public Double stddevMarathonsRunner() {
		Actor principal;

		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.stddevMarathonsRunner();
	}

	public Double avgMarathonsOrganizer() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);

		return this.adminRepository.avgMarathonsOrganizer();
	}

	public Double maxMarathonsOrganizer() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);

		return this.adminRepository.maxMarathonsOrganizer();
	}

	public Double minMarathonsOrganizer() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);

		return this.adminRepository.minMarathonsOrganizer();
	}

	public Double stddevMarathonsOrganizer() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);

		return this.adminRepository.stddevMarathonsOrganizer();
	}

	public Double avgApplicationsMarathon() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);

		return this.adminRepository.avgApplicationsMarathon();
	}

	public Double maxApplicationsMarathon() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);

		return this.adminRepository.maxApplicationsMarathon();
	}

	public Double minApplicationsMarathon() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);

		return this.adminRepository.minApplicationsMarathon();
	}

	public Double stddevApplicationsMarathon() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);

		return this.adminRepository.stddevApplicationsMarathon();
	}

	public Double avgPriceMarathon() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.avgPriceMarathon();
	}

	public Double maxPriceMarathon() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.maxPriceMarathon();
	}

	public Double minPriceMarathon() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.minPriceMarathon();
	}

	public Double stddevPriceMarathon() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.stddevPriceMarathon();
	}

	public Double ratioPending() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.ratioPending();
	}

	public Double ratioAccepted() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.ratioAccepted();
	}

	public Double ratioRejected() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.ratioRejected();
	}

	public Collection<Object> top5MarathonsTermsSponsorships() {
		Actor principal;
		// Principal must be an admin
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.top5MarathonsTermsOfSponsorships();

	}

	public Double avgSponsorshipsSponsor() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.avgSponsorshipsSponsor();
	}

	public int maxSponsorshipsSponsor() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.maxSponsorshipsSponsor();
	}

	public int minSponsorshipsSponsor() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.minavgSponsorshipsSponsor();
	}
	public Double stddevSponsorshipsSponsor() {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.stddevavgSponsorshipsSponsor();
	}

	public Collection<Object> top5SponsorsTermsSponsorships() {
		Actor principal;
		// Principal must be an admin
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);
		return this.adminRepository.top5SponsorsTermsOfSponsorships();

	}
}
