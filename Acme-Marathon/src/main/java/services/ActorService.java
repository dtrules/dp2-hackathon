
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ActorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountRepository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import domain.Actor;
import domain.SocialProfile;

@Service
@Transactional
public class ActorService {

	// Manage Repository
	@Autowired
	private ActorRepository			actorRepository;

	// Supported Services
	@Autowired
	private CustomizationService	customizationService;

	@Autowired
	private SocialProfileService	socialProfileService;

	@Autowired
	private UserAccountRepository	userAccountRepository;


	// CRUD methods

	public Actor findOne(final int actorId) {
		final Actor result = this.actorRepository.findOne(actorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Actor> findAll() {
		final Collection<Actor> result = this.actorRepository.findAll();
		Assert.notNull(result);
		Assert.notEmpty(result);

		return result;
	}

	public Actor save(final Actor actor) {
		Assert.notNull(actor);
		final Actor result = this.actorRepository.save(actor);

		return result;
	}

	public void delete(final Actor actor) {
		Assert.notNull(actor);

		this.actorRepository.delete(actor);
	}

	// Other business methods
	public Actor findByPrincipal() {
		Actor result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public Actor findByUserAccount(final UserAccount userAccount) {
		Assert.notNull(userAccount);

		Actor result;

		result = this.actorRepository.findByUserAccountId(userAccount.getId());

		return result;
	}

	public Actor findOneByUsername(final String username) {
		Assert.notNull(username);

		return this.actorRepository.findByUserName(username);
	}

	public Actor findActorBySocialProfile(final int socialProfileId) {
		return this.actorRepository.findActorBySocialProfileId(socialProfileId);
	}

	public void checkAuth(final String auth) {
		Assert.notNull(auth);
		final Authority a = new Authority();
		a.setAuthority(auth);
		Assert.isTrue(LoginService.getPrincipal().getAuthorities().contains(a));
	}

	public void checkAuth(final String... auths) {
		boolean check = false;
		final Authority a = new Authority();
		for (final String auth : auths) {
			a.setAuthority(auth);
			if (LoginService.getPrincipal().getAuthorities().contains(a)) {
				check = true;
				break;
			}
		}
		Assert.isTrue(check);
	}

	public void checkSpamWords(final String string) {
		final Collection<String> spamWords = this.customizationService.findCustomization().getSpamWords();
		final Actor principal = this.findByPrincipal();
		if (string != null)
			for (final String s : spamWords)
				if (string.toLowerCase().contains(s.toLowerCase())) {
					principal.setIsSuspicious(true);
					break;
				}
	}

	public void deleteActor() {
		final Actor principal = this.findByPrincipal();

		final Collection<SocialProfile> socialProfiles = principal.getSocialProfiles();

		principal.setName("Lorem ipsum");
		principal.setSurname("Lorem ipsum");
		principal.setPhoto("Lorem ipsum");
		principal.setEmail("loremipsum@loremipsum.com");
		principal.setPhoneNumber("666666666");
		principal.setAddress("Lorem ipsum");

		if (socialProfiles.isEmpty() == false)
			for (final SocialProfile s : socialProfiles) {
				s.setSocialNetworkName("Lorem ipsum");
				s.setLink("https://www.loremipsum.com");
				s.setNick("Lorem ipsum");
				this.socialProfileService.save(s);
			}

		// Cambiamos la contraseņa para que no se pueda acceder.
		final Md5PasswordEncoder encoder = new Md5PasswordEncoder();
		final String hashedPassword = encoder.encodePassword("1234", null);
		principal.getUserAccount().setPassword(hashedPassword);

		this.userAccountRepository.save(principal.getUserAccount());

		this.save(principal);
	}

	public String exportPersonalData() {
		final Actor principal = this.findByPrincipal();

		final Gson gson = new GsonBuilder().create();

		final String result = gson.toJson(principal);

		return result;
	}

}
