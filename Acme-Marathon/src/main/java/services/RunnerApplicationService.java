
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.RunnerApplicationRepository;
import domain.Actor;
import domain.Marathon;
import domain.Organizer;
import domain.Runner;
import domain.RunnerApplication;

@Service
@Transactional
public class RunnerApplicationService {

	// Manage Repository
	@Autowired
	private RunnerApplicationRepository	runnerApplicationRepository;

	// Supported services -------------------------------------------
	@Autowired
	private ActorService				actorService;

	@Autowired
	private RunnerService				runnerService;

	@Autowired
	private OrganizerService			organizerService;


	// Constructor methods -------------------------------------------
	public RunnerApplicationService() {
		super();
	}

	public RunnerApplication create() {
		RunnerApplication result;
		Actor principal;

		result = new RunnerApplication();

		// Principal must be a Runner
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Runner.class, principal);

		final Runner r = this.runnerService.findByPrincipal();
		final Date creation = new Date();

		result.setMoment(creation);
		result.setStatus("PENDING");
		result.setRunner(r);

		return result;
	}

	public RunnerApplication save(final RunnerApplication runnerApplication) {
		Assert.notNull(runnerApplication);
		Actor principal;

		// Principal must be a Runner
		principal = this.actorService.findByPrincipal();

		final Date submit = new Date(System.currentTimeMillis() - 1000);
		runnerApplication.setMoment(submit);

		this.checkRunnerApplication(runnerApplication);

		if (runnerApplication.getId() != 0){
			//runnerApplication.setRunner(this.runnerService.findByPrincipal());
			Assert.isInstanceOf(Organizer.class, principal);
			Assert.isTrue(principal.equals(runnerApplication.getMarathon().getOrganizer()));
		}else{
			Assert.isInstanceOf(Runner.class, principal);
			Assert.isTrue(principal.equals(runnerApplication.getRunner()));
		}
		return this.runnerApplicationRepository.save(runnerApplication);
	}

	public void delete(final int runnerApplicationId) {

		Actor principal;

		final RunnerApplication r = this.findOne(runnerApplicationId);

		Assert.notNull(r);

		// Principal must be a Runner
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Runner.class, principal);

		Assert.isTrue(principal.equals(r.getRunner()));

		this.runnerApplicationRepository.delete(r);
	}

	public RunnerApplication findOne(final int id) {
		final RunnerApplication result = this.runnerApplicationRepository.findOne(id);

		Assert.notNull(result);

		return result;
	}

	public Collection<RunnerApplication> findAll() {
		final Collection<RunnerApplication> result = this.runnerApplicationRepository.findAll();

		return result;
	}

	public void acceptRunnerApplication(final int runnerApplicationId) {
		Assert.isTrue(runnerApplicationId != 0);
		final RunnerApplication runnerApplication = this.findOne(runnerApplicationId);
		Assert.notNull(runnerApplication);
		Actor principal;

		// Principal must be a Organiser
		principal = this.actorService.findByPrincipal();
		//Assert.isInstanceOf(Organizer.class, principal);

		runnerApplication.setStatus("ACCEPTED");
		this.runnerApplicationRepository.save(runnerApplication);
	}

	public void rejectRunnerApplication(final int runnerApplicationId) {
		Assert.isTrue(runnerApplicationId != 0);
		final RunnerApplication runnerApplication = this.findOne(runnerApplicationId);
		Assert.notNull(runnerApplication);
		Actor principal;

		// Principal must be a Organiser
		principal = this.actorService.findByPrincipal();
		//Assert.isInstanceOf(Organizer.class, principal);

		runnerApplication.setStatus("REJECTED");
		this.runnerApplicationRepository.save(runnerApplication);
	}
	
	public void runRunnerApplication(final int runnerApplicationId) {
		Assert.isTrue(runnerApplicationId != 0);
		final RunnerApplication runnerApplication = this.findOne(runnerApplicationId);
		Assert.notNull(runnerApplication);
		Actor principal;

		// Principal must be a Organiser
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);

		runnerApplication.setStatus("RUNNED");
		this.runnerApplicationRepository.save(runnerApplication);
	}

	//Other business methods ---------------------------------------------------------
	public Collection<RunnerApplication> getRunnerApplicationsByRunner(final int runnerId) {
		final Actor logueado = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Runner.class, logueado);
		return this.runnerApplicationRepository.getRunnerApplicationsByRunner(runnerId);
	}

	public Collection<RunnerApplication> getRunnerApplicationsByMarathon(final int marathonId) {
		final Actor logueado = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, logueado);
		return this.runnerApplicationRepository.getRunnerApplicationsByMarathon(marathonId);
	}

	public Collection<RunnerApplication> getRunnerApplicationsByOrganizer(final int organizerId) {
		final Actor logueado = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, logueado);
		return this.runnerApplicationRepository.getRunnerApplicationsByOrganizer(organizerId);
	}

	public Collection<RunnerApplication> findRunnerApplicationsByStatusOrganizer(final String status) {
		final Collection<RunnerApplication> result = new ArrayList<RunnerApplication>();

		final Actor act = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, act);
		final Organizer o = this.organizerService.findByPrincipal();

		Assert.notNull(o);

		final Collection<RunnerApplication> apps = this.getRunnerApplicationsByOrganizer(o.getId());

		for (final RunnerApplication a : apps)
			if (a.getStatus().equals(status))
				result.add(a);

		return result;
	}

	public Collection<RunnerApplication> findRunnerApplicationsByStatusRunner(final String status) {
		final Collection<RunnerApplication> result = new ArrayList<RunnerApplication>();

		final Actor act = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Runner.class, act);
		final Runner r = this.runnerService.findByPrincipal();

		Assert.notNull(r);

		final Collection<RunnerApplication> apps = this.getRunnerApplicationsByRunner(r.getId());

		for (final RunnerApplication a : apps)
			if (a.getStatus().equals(status))
				result.add(a);

		return result;
	}

	public void checkRunnerHasAlreadyApplied(final Marathon marathon) {

		Actor principal;
		boolean res = true;

		// Principal must be a Runner
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Runner.class, principal);

		final Runner r = this.runnerService.findByPrincipal();

		final Collection<RunnerApplication> apps = this.runnerApplicationRepository.getRunnerApplicationsByMarathon(marathon.getId());

		for (final RunnerApplication ra : apps)
			if (ra.getRunner().equals(r))
				res = false;

		Assert.isTrue(res);

	}

	public RunnerApplication findOneToEdit(final int id) {
		Actor principal;

		// Principal must be a Runner
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);

		final RunnerApplication result = this.runnerApplicationRepository.findOne(id);

		Assert.notNull(result);

		// Debe ser el mismo Runner que al que pertenece la Position
		Assert.isTrue(principal.equals(result.getMarathon().getOrganizer()));

		return result;
	}

	public void flush() {
		this.runnerApplicationRepository.flush();
	}

	public void checkRunnerApplication(final RunnerApplication runnerApplication) {
		boolean check = true;

		if (runnerApplication.getStatus() == null || runnerApplication.getMoment() == null || runnerApplication.getPrice() == null)
			check = false;

		Assert.isTrue(check);
	}

}
