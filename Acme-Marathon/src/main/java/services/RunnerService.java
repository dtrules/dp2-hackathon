
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.RunnerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountRepository;
import domain.CreditCard;
import domain.Runner;
import domain.RunnerApplication;
import domain.SocialProfile;
import forms.RunnerForm;

@Service
@Transactional
public class RunnerService {

	// Manage Repository
	@Autowired
	private RunnerRepository			runnerRepository;

	@Autowired(required = false)
	private Validator					validator;

	@Autowired
	private UserAccountRepository		userAccountRepository;

	@Autowired
	private CreditCardService			creditCardService;

	@Autowired
	private FolderService				folderService;

	@Autowired
	private RunnerApplicationService	runnerApplicationService;


	// CRUD methods
	public Runner create() {
		final Runner result = new Runner();
		final CreditCard creditCard = new CreditCard();
		final UserAccount userAccount = new UserAccount();
		final Collection<Authority> authorities = new ArrayList<Authority>();
		final Authority authority = new Authority();
		//final Collection<Curricula> curricula = new ArrayList<Curricula>();
		final Date now = new Date(System.currentTimeMillis() - 1000);
		final Collection<SocialProfile> profiles = new ArrayList<SocialProfile>();

		authority.setAuthority(Authority.RUNNER);
		authorities.add(authority);
		userAccount.setAuthorities(authorities);

		//result.setCurriculas(curricula);
		result.setUserAccount(userAccount);
		result.setCreditCard(creditCard);
		result.setSocialProfiles(profiles);

		return result;

	}

	public Runner save(final Runner runner) {
		Runner res;
		Assert.notNull(runner);
		Assert.isTrue(this.checkDate(runner.getCreditCard().getExpiryMonth(), runner.getCreditCard().getExpiryYear()));

		if (runner.getId() == 0) {
			final Md5PasswordEncoder encoder = new Md5PasswordEncoder();
			final UserAccount userAccount = runner.getUserAccount();
			final String password = userAccount.getPassword();
			final String hashedPassword = encoder.encodePassword(password, null);
			userAccount.setPassword(hashedPassword);
			final UserAccount ua = this.userAccountRepository.save(userAccount);
			runner.setUserAccount(ua);
		}

		res = this.runnerRepository.save(runner);

		if (runner.getId() == 0)
			this.folderService.generateFolders(res.getId());

		return res;

	}

	public Runner findOne(final int runnerId) {
		final Runner result = this.runnerRepository.findOne(runnerId);
		Assert.notNull(result);

		return result;
	}

	public Runner findOneNoAssert(final int runnerId) {
		final Runner result = this.runnerRepository.findOne(runnerId);

		return result;
	}

	public Collection<Runner> findAll() {
		final Collection<Runner> result = this.runnerRepository.findAll();
		Assert.notNull(result);
		Assert.notEmpty(result);

		return result;
	}

	public void delete(final Runner runner) {
		Assert.notNull(runner);

		this.runnerRepository.delete(runner);
	}

	// Other business methods
	public Runner findByPrincipal() {
		Runner result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	/*** Reconstruct object, check validity and update binding ***/
	public Runner reconstruct(final RunnerForm runnerForm, final BindingResult binding) {
		final Runner result = this.create();

		result.getCreditCard().setBrandName(runnerForm.getCreditCard().getBrandName());
		result.getCreditCard().setCvv(runnerForm.getCreditCard().getCvv());
		result.getCreditCard().setExpiryMonth(runnerForm.getCreditCard().getExpiryMonth());
		result.getCreditCard().setExpiryYear(runnerForm.getCreditCard().getExpiryYear());
		result.getCreditCard().setHolderName(runnerForm.getCreditCard().getHolderName());
		result.getCreditCard().setNumber(runnerForm.getCreditCard().getNumber());
		result.getUserAccount().setPassword(runnerForm.getUserAccount().getPassword());
		result.getUserAccount().setUsername(runnerForm.getUserAccount().getUsername());
		//		Assert.isTrue(runnerForm.getUserAccount().getPassword() == runnerForm.getConfirmPassword());

		result.setAddress(runnerForm.getAddress());
		result.setEmail(runnerForm.getEmail());
		result.setName(runnerForm.getName());
		result.setPhoneNumber(runnerForm.getPhoneNumber());
		result.setPhoto(runnerForm.getPhoto());
		result.setSurname(runnerForm.getSurname());
		result.setIsBanned(false);

		this.validator.validate(result, binding);

		return result;
	}

	public Runner reconstruct(final Runner runner, final BindingResult binding) {
		Runner result = new Runner();
		final Runner res = new Runner();

		if (runner.getId() == 0)
			result = runner;
		else {
			result = this.findOne(runner.getId());
			final CreditCard creditCard = runner.getCreditCard();
			res.setAddress(runner.getAddress());
			res.setEmail(runner.getEmail());
			res.setName(runner.getName());
			res.setPhoneNumber(runner.getPhoneNumber());
			res.setPhoto(runner.getPhoto());
			res.setSurname(runner.getSurname());
			res.setId(result.getId());
			res.setVersion(result.getVersion());
			res.setUserAccount(result.getUserAccount());
			//res.setCurriculas(result.getCurriculas());
			res.setSocialProfiles(result.getSocialProfiles());

			creditCard.setBrandName(runner.getCreditCard().getBrandName());
			creditCard.setCvv(runner.getCreditCard().getCvv());
			creditCard.setExpiryMonth(runner.getCreditCard().getExpiryMonth());
			creditCard.setExpiryYear(runner.getCreditCard().getExpiryYear());
			creditCard.setHolderName(runner.getCreditCard().getHolderName());
			creditCard.setNumber(runner.getCreditCard().getNumber());
			res.setCreditCard(creditCard);
		}
		this.validator.validate(res, binding);

		return res;
	}
	/************************************************************************************************/

	public Runner findByUserAccount(final UserAccount userAccount) {
		Assert.notNull(userAccount);

		Runner result;

		result = this.runnerRepository.findByUserAccountId(userAccount.getId());

		return result;
	}

	public void checkIfRunner() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.RUNNER))
				res = true;
		Assert.isTrue(res);
	}

	public Runner findOneByUsername(final String username) {
		Assert.notNull(username);

		return this.runnerRepository.findByUserName(username);
	}

	//	public Runner findRunnerByApplicationId(final int applicationId) {
	//		return this.runnerRepository.findRunnerByApplicationId(applicationId);
	//	}

	public void flush() {
		this.runnerRepository.flush();
	}

	public Boolean checkDate(final int expiryMonth, final int expiryYear) {
		Boolean res = true;
		final Date date = new Date();
		final int month = date.getMonth();
		final int year = date.getYear() % 100;
		if (expiryYear == year)
			if (month > expiryMonth)
				res = false;

		return res;
	}

	public Collection<Runner> findRunnerByRunnerApplication(final int marathonId) {
		final Collection<Runner> runnersOfMarathon = new ArrayList<Runner>();
		final Collection<RunnerApplication> runnerApplications = this.runnerApplicationService.getRunnerApplicationsByMarathon(marathonId);
		for (final RunnerApplication r : runnerApplications)
			runnersOfMarathon.add(r.getRunner());

		return runnersOfMarathon;
	}

}
