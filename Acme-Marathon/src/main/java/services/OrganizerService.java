
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.OrganizerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountRepository;
import domain.CreditCard;
import domain.Organizer;
import domain.SocialProfile;
import forms.OrganizerForm;

@Service
@Transactional
public class OrganizerService {

	// Managed Repositories

	@Autowired
	private OrganizerRepository		organizerRepository;

	@Autowired(required = false)
	private Validator				validator;

	@Autowired
	private UserAccountRepository	userAccountRepository;

	@Autowired
	private CreditCardService		creditCardService;

	@Autowired
	private FolderService			folderService;


	// CRUD methods
	public Organizer create() {
		final Organizer result = new Organizer();
		final CreditCard creditCard = new CreditCard();
		final UserAccount userAccount = new UserAccount();
		final Collection<Authority> authorities = new ArrayList<Authority>();
		final Authority authority = new Authority();
		//final Collection<Curricula> curricula = new ArrayList<Curricula>();
		final Date now = new Date(System.currentTimeMillis() - 1000);
		final Collection<SocialProfile> profiles = new ArrayList<SocialProfile>();

		authority.setAuthority(Authority.ORGANIZER);
		authorities.add(authority);
		userAccount.setAuthorities(authorities);

		//result.setCurriculas(curricula);
		result.setUserAccount(userAccount);
		result.setCreditCard(creditCard);
		result.setSocialProfiles(profiles);

		return result;

	}

	public Organizer save(final Organizer organizer) {
		Organizer res;
		Assert.notNull(organizer);
		Assert.isTrue(this.checkDate(organizer.getCreditCard().getExpiryMonth(), organizer.getCreditCard().getExpiryYear()));

		if (organizer.getId() == 0) {
			final Md5PasswordEncoder encoder = new Md5PasswordEncoder();
			final UserAccount userAccount = organizer.getUserAccount();
			final String password = userAccount.getPassword();
			final String hashedPassword = encoder.encodePassword(password, null);
			userAccount.setPassword(hashedPassword);
			final UserAccount ua = this.userAccountRepository.save(userAccount);
			organizer.setUserAccount(ua);
		}

		res = this.organizerRepository.save(organizer);

		if (organizer.getId() == 0)
			this.folderService.generateFolders(res.getId());

		return res;

	}

	public Organizer findOne(final int organizerId) {
		//		final Organizer result = this.organizerRepository.findOne(organizerId);
		final Organizer result = this.organizerRepository.findOne(organizerId);
		Assert.notNull(result);

		return result;
	}

	public Organizer findOneNoAssert(final int organizerId) {
		final Organizer result = this.organizerRepository.findOne(organizerId);

		return result;
	}

	public Collection<Organizer> findAll() {
		final Collection<Organizer> result = this.organizerRepository.findAll();
		Assert.notNull(result);
		Assert.notEmpty(result);

		return result;
	}

	public void delete(final Organizer organizer) {
		Assert.notNull(organizer);

		this.organizerRepository.delete(organizer);
	}

	// Other business methods
	public Organizer findByPrincipal() {
		Organizer result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	/*** Reconstruct object, check validity and update binding ***/
	public Organizer reconstruct(final OrganizerForm organizerForm, final BindingResult binding) {
		final Organizer result = this.create();

		result.getCreditCard().setBrandName(organizerForm.getCreditCard().getBrandName());
		result.getCreditCard().setCvv(organizerForm.getCreditCard().getCvv());
		result.getCreditCard().setExpiryMonth(organizerForm.getCreditCard().getExpiryMonth());
		result.getCreditCard().setExpiryYear(organizerForm.getCreditCard().getExpiryYear());
		result.getCreditCard().setHolderName(organizerForm.getCreditCard().getHolderName());
		result.getCreditCard().setNumber(organizerForm.getCreditCard().getNumber());
		result.getUserAccount().setPassword(organizerForm.getUserAccount().getPassword());
		result.getUserAccount().setUsername(organizerForm.getUserAccount().getUsername());
		//		Assert.isTrue(runnerForm.getUserAccount().getPassword() == runnerForm.getConfirmPassword());

		result.setAddress(organizerForm.getAddress());
		result.setEmail(organizerForm.getEmail());
		result.setName(organizerForm.getName());
		result.setPhoneNumber(organizerForm.getPhoneNumber());
		result.setPhoto(organizerForm.getPhoto());
		result.setSurname(organizerForm.getSurname());
		result.setIsBanned(false);

		this.validator.validate(result, binding);

		return result;
	}

	public Organizer reconstruct(final Organizer organizer, final BindingResult binding) {
		Organizer result = new Organizer();
		final Organizer res = new Organizer();

		if (organizer.getId() == 0)
			result = organizer;
		else {
			result = this.findOne(organizer.getId());
			final CreditCard creditCard = organizer.getCreditCard();
			res.setAddress(organizer.getAddress());
			res.setEmail(organizer.getEmail());
			res.setName(organizer.getName());
			res.setPhoneNumber(organizer.getPhoneNumber());
			res.setPhoto(organizer.getPhoto());
			res.setSurname(organizer.getSurname());
			res.setId(result.getId());
			res.setVersion(result.getVersion());
			res.setUserAccount(result.getUserAccount());
			//res.setCurriculas(result.getCurriculas());
			res.setSocialProfiles(result.getSocialProfiles());

			creditCard.setBrandName(organizer.getCreditCard().getBrandName());
			creditCard.setCvv(organizer.getCreditCard().getCvv());
			creditCard.setExpiryMonth(organizer.getCreditCard().getExpiryMonth());
			creditCard.setExpiryYear(organizer.getCreditCard().getExpiryYear());
			creditCard.setHolderName(organizer.getCreditCard().getHolderName());
			creditCard.setNumber(organizer.getCreditCard().getNumber());
			res.setCreditCard(creditCard);
		}
		this.validator.validate(res, binding);

		return res;
	}
	/************************************************************************************************/

	public Organizer findByUserAccount(final UserAccount userAccount) {
		Assert.notNull(userAccount);

		Organizer result;

		result = this.organizerRepository.findByUserAccountId(userAccount.getId());

		return result;
	}

	public void checkIfRunner() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.RUNNER))
				res = true;
		Assert.isTrue(res);
	}

	public Organizer findOneByUsername(final String username) {
		Assert.notNull(username);

		return this.organizerRepository.findByUserName(username);
	}

	//	public Runner findRunnerByApplicationId(final int applicationId) {
	//		return this.runnerRepository.findRunnerByApplicationId(applicationId);
	//	}

	public void flush() {
		this.organizerRepository.flush();
	}

	public Boolean checkDate(final int expiryMonth, final int expiryYear) {
		Boolean res = true;
		final Date date = new Date();
		final int month = date.getMonth();
		final int year = date.getYear() % 100;
		if (expiryYear == year)
			if (month > expiryMonth)
				res = false;

		return res;
	}
}
