
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.WorkerApplicationRepository;
import domain.Actor;
import domain.Organizer;
import domain.Worker;
import domain.WorkerApplication;

@Service
@Transactional
public class WorkerApplicationService {

	// Manage Repository
	@Autowired
	private WorkerApplicationRepository	WorkerApplicationRepository;

	// Supported services -------------------------------------------
	@Autowired
	private ActorService				actorService;

	@Autowired
	private WorkerService				workerService;

	@Autowired
	private OrganizerService			organizerService;


	// Constructor methods -------------------------------------------
	public WorkerApplicationService() {
		super();
	}

	public WorkerApplication create() {
		WorkerApplication result;
		Actor principal;

		result = new WorkerApplication();

		// Principal must be a Worker
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		final Worker w = this.workerService.findByPrincipal();
		final Date creation = new Date();

		result.setMoment(creation);
		result.setStatus("PENDING");
		result.setHours(0);
		result.setWorker(w);

		return result;
	}

	public WorkerApplication save(final WorkerApplication WorkerApplication) {
		Assert.notNull(WorkerApplication);
		Actor principal;

		// Principal must be a Worker
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		final Date submit = new Date(System.currentTimeMillis() - 1000);
		WorkerApplication.setMoment(submit);

		this.checkWorkerApplication(WorkerApplication);

		if (WorkerApplication.getId() != 0 && WorkerApplication.getWorker() == null)
			WorkerApplication.setWorker(this.workerService.findByPrincipal());
		else
			Assert.isTrue(principal.equals(WorkerApplication.getWorker()));

		return this.WorkerApplicationRepository.save(WorkerApplication);
	}

	public void delete(final int WorkerApplicationId) {

		Actor principal;

		final WorkerApplication app = this.findOne(WorkerApplicationId);

		Assert.notNull(app);

		// Principal must be a Worker
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		Assert.isTrue(principal.equals(app.getWorker()));

		this.WorkerApplicationRepository.delete(app);
	}

	public WorkerApplication findOne(final int id) {
		final WorkerApplication result = this.WorkerApplicationRepository.findOne(id);

		Assert.notNull(result);

		return result;
	}

	public Collection<WorkerApplication> findAll() {
		final Collection<WorkerApplication> result = this.WorkerApplicationRepository.findAll();

		return result;
	}

	public void acceptWorkerApplication(final int WorkerApplicationId) {

		Assert.isTrue(WorkerApplicationId != 0);
		final WorkerApplication WorkerApplication = this.findOne(WorkerApplicationId);
		Assert.notNull(WorkerApplication);
		Actor principal;

		// Principal must be a Organiser
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);

		WorkerApplication.setStatus("ACCEPTED");
		this.WorkerApplicationRepository.save(WorkerApplication);
	}

	public void rejectWorkerApplication(final int WorkerApplicationId) {

		Assert.isTrue(WorkerApplicationId != 0);
		final WorkerApplication WorkerApplication = this.findOne(WorkerApplicationId);
		Assert.notNull(WorkerApplication);
		Actor principal;

		// Principal must be a Organiser
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);

		WorkerApplication.setStatus("REJECTED");
		this.WorkerApplicationRepository.save(WorkerApplication);
	}

	//Other business methods ---------------------------------------------------------

	public Collection<WorkerApplication> getWorkerApplicationsByWorker(final int WorkerId) {
		final Actor logueado = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, logueado);
		return this.WorkerApplicationRepository.getWorkerApplicationsByWorker(WorkerId);
	}

	public Collection<WorkerApplication> getWorkerApplicationsByMarathon(final int marathonId) {

		final Actor logueado = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, logueado);
		return this.WorkerApplicationRepository.getWorkerApplicationsByMarathon(marathonId);
	}

	public Collection<WorkerApplication> getWorkerApplicationsByOrganizer(final int organizerId) {
		final Actor logueado = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, logueado);
		return this.WorkerApplicationRepository.getWorkerApplicationsByOrganizer(organizerId);
	}

	public Collection<WorkerApplication> findWorkerApplicationsByStatusOrganizer(final String status) {

		final Collection<WorkerApplication> result = new ArrayList<WorkerApplication>();

		final Actor act = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, act);
		final Organizer o = this.organizerService.findByPrincipal();

		Assert.notNull(o);

		final Collection<WorkerApplication> apps = this.getWorkerApplicationsByOrganizer(o.getId());

		for (final WorkerApplication a : apps)
			if (a.getStatus().equals(status))
				result.add(a);

		return result;
	}

	public Collection<WorkerApplication> findWorkerApplicationsByStatusWorker(final String status) {
		final Collection<WorkerApplication> result = new ArrayList<WorkerApplication>();

		final Actor act = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, act);
		final Worker r = this.workerService.findByPrincipal();

		Assert.notNull(r);

		final Collection<WorkerApplication> apps = this.getWorkerApplicationsByWorker(r.getId());

		for (final WorkerApplication a : apps)
			if (a.getStatus().equals(status))
				result.add(a);

		return result;
	}

	public WorkerApplication findOneToEdit(final int id) {
		Actor principal;

		// Principal must be a Worker
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		final WorkerApplication result = this.WorkerApplicationRepository.findOne(id);

		Assert.notNull(result);

		// Debe ser el mismo Worker que al que pertenece la Position
		Assert.isTrue(principal.equals(result.getWorker()));

		return result;
	}

	public void flush() {
		this.WorkerApplicationRepository.flush();
	}

	public void checkWorkerApplication(final WorkerApplication workerApplication) {
		boolean check = true;

		if (workerApplication.getStatus() == null || workerApplication.getMoment() == null)
			check = false;

		Assert.isTrue(check);
	}

}
