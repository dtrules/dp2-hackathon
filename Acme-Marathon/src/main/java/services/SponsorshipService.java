
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.SponsorshipRepository;
import domain.Actor;
import domain.Marathon;
import domain.Sponsor;
import domain.Sponsorship;

@Service
@Transactional
public class SponsorshipService {

	// Managed repository -------------------------------------------

	@Autowired
	private SponsorshipRepository	sponsorshipRepository;

	@Autowired
	private ActorService			actorService;

	@Autowired
	private SponsorService			sponsorService;


	// Constructor methods -------------------------------------------
	public SponsorshipService() {
		super();
	}

	// Simple CRUD methods ------------------------------------------

	public Sponsorship create() {
		Sponsorship result;
		final Actor principal = this.actorService.findByPrincipal();
		Assert.notNull(principal);
		Assert.isInstanceOf(Sponsor.class, principal);
		result = new Sponsorship();
		Marathon marathon = new Marathon();
		Sponsor sponsor = this.sponsorService.findOne(principal.getId());
		result.setSponsor(sponsor);
		result.setMarathon(marathon);
		return result;
	}

	public Sponsorship save(final Sponsorship sponsorship) {
		Assert.notNull(sponsorship);
		Sponsorship res = new Sponsorship();
		final Actor principal = this.actorService.findByPrincipal();
		Assert.notNull(principal);
		Assert.isInstanceOf(Sponsor.class, principal);

		if (res.getId() == 0)
			res = this.sponsorshipRepository.save(sponsorship);
		else
			res = this.findOneToEdit(sponsorship.getId());

		return res;
	}
	public void delete(final int sponsorshipId) {

		Actor principal = this.actorService.findByPrincipal();
		Assert.notNull(principal);
		Assert.isInstanceOf(Sponsor.class, principal);

		final Sponsorship a = this.findOneToEdit(sponsorshipId);

		Assert.notNull(a);

		this.sponsorshipRepository.delete(a);

	}

	public Sponsorship findOne(final int id) {
		final Sponsorship result = this.sponsorshipRepository.findOne(id);

		Assert.notNull(result);

		return result;
	}

	public Collection<Sponsorship> findAll() {
		final Collection<Sponsorship> result = this.sponsorshipRepository.findAll();

		return result;
	}

	public Sponsorship findOneToEdit(final int id) {
		Actor principal;

		// Principal must be a Sponsor
		principal = this.actorService.findByPrincipal();

		final Sponsorship result = this.sponsorshipRepository.findOne(id);
		Assert.notNull(result);

		final Actor actor = result.getSponsor();
		Assert.notNull(actor);

		Assert.isTrue(principal.equals(actor));

		return result;

	}

	public Collection<Sponsorship> findSponsorshipsBySponsor(int sponsorId) {
		Sponsor sponsor = this.sponsorService.findOne(sponsorId);

		Collection<Sponsorship> res = this.findAll();
		Collection<Sponsorship> sponsorshipsSponsor = new ArrayList<Sponsorship>();

		for (Sponsorship s : res)
			if (s.getSponsor().getId() == sponsorId)
				sponsorshipsSponsor.add(s);
		return sponsorshipsSponsor;
	}

	public void flush() {
		this.sponsorshipRepository.flush();
	}

	public Sponsorship findRandomOneByMarathon(final int marathonId) {
		final List<Sponsorship> aux = this.sponsorshipRepository.findByMarathon(marathonId);
		Sponsorship res = null;
		final int aleatorio = (int) (Math.random() * aux.size());

		if (aux.size() != 0)
			res = aux.get(aleatorio);
		else
			for (final Sponsorship s : this.sponsorshipRepository.findAll())
				res = s;
		return res;
	}

	public Collection<Sponsorship> findByMarathon(final Integer marathonId) {
		return this.sponsorshipRepository.findByMarathon(marathonId);
	}
}
