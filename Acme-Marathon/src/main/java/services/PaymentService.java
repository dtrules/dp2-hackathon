
package services;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.PaymentRepository;
import domain.Actor;
import domain.Administrator;
import domain.Payment;
import domain.Organizer;
import domain.Worker;

@Service
@Transactional
public class PaymentService {

	// Managed repository -------------------------------------------
	@Autowired
	private PaymentRepository	paymentRepository;

	// Supported services -------------------------------------------

	@Autowired
	private ActorService	 actorService;
	
	@Autowired
	private OrganizerService organizerService;
	
	@Autowired
	private WorkerService workerService;

	@Autowired(required = false)
	@Qualifier("validator")
	private Validator		 validator;


	// Constructor methods -------------------------------------------
	public PaymentService() {
		super();
	}

	// Simple CRUD methods ------------------------------------------

	public Payment create() {
		
		Payment res;
		Actor principal;

		// Principal must be a Organizer
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);
		final Organizer principalOrganizer = this.organizerService.findByPrincipal();

		res = new Payment();

		String ticker = this.generateTicker();
		res.setMoney(0.0);
		Date moment = new Date();
		moment.setTime(moment.getTime() - 1);
		res.setMoment(moment);
		res.setTicker(ticker);
		res.setOrganizer(principalOrganizer);

		return res;
	}

	public Payment findOne(final int id) {
		final Payment result = this.paymentRepository.findOne(id);

		Assert.notNull(result);

		return result;
	}

	public Collection<Payment> findAll() {
		final Collection<Payment> result = this.paymentRepository.findAll();

		return result;

	}

	public Payment save (final Payment c) {
		
		Assert.notNull(c);
		Actor principal;

		// Principal must be a Company
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);

		this.checkPayment(c);

		return this.paymentRepository.save(c);
		
	}

	
	public void delete(final int PaymentId) {
		Assert.notNull(PaymentId);

		final Payment c = this.findOne(PaymentId);
		Assert.notNull(c);

		// Principal must be a Company
		final Actor principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);
		final Organizer org = this.organizerService.findByPrincipal();
		//Another company can't delete other company's problems
		Assert.isTrue(c.getOrganizer().equals(org));

		this.paymentRepository.delete(PaymentId);

	}

	public Payment reconstruct(final Payment c, final BindingResult binding) {
		
		Payment res = new Payment();

		if (c.getId() == 0)
			
			res = c;
		
		else {
			final Payment copy = this.findOne(c.getId());

			res.setTicker(c.getTicker());
			res.setMoney(c.getMoney());
			res.setMoment(c.getMoment());

			res.setId(copy.getId());
			res.setVersion(copy.getVersion());
			res.setOrganizer(copy.getOrganizer());
			res.setWorker(copy.getWorker());
		}

		this.validator.validate(res, binding);
		return res;
		
	}

	// Other business methods ---------------------------------------------------------

	public void flush() {
		this.paymentRepository.flush();
	}
	
	public String generateTicker () {
		String ticker = "";
		final Date moment = new Date();
		String year = new Integer(moment.getYear() % 100).toString();
		String month = new Integer(moment.getMonth() + 1).toString();
		String day = new Integer(moment.getDate()).toString();
		if (year.length() == 1)
			year = "0" + year;
		if (month.length() == 1)
			month = "0" + month;
		if (day.length() == 1)
			day = "0" + day;

		final String l = RandomStringUtils.randomAlphanumeric(6);

		ticker = ticker + year + month + day + "-" + l.toUpperCase();
		
		return ticker;
	}

	public void checkPayment(final Payment a) {
		boolean check = true;

		if (a.getMoney() == null || a.getTicker() == null || a.getMoment() == null)
			check = false;

		Assert.isTrue(check);
	}

	public Collection<Payment> getPaymentsByOrganizer() {
		final Actor logueado = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, logueado);
		Organizer org = this.organizerService.findByPrincipal();
		int organizerId = org.getId();
		return this.paymentRepository.findPaymentByOrganizer(organizerId);
	}

	public Collection<Payment> getPaymentsByWorker() {
		final Actor logueado = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, logueado);
		Worker work = this.workerService.findByPrincipal();
		int workerId = work.getId();
		return this.paymentRepository.findPaymentByOrganizer(workerId);
	}

}
