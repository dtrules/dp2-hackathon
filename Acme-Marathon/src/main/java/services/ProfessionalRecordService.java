
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ProfessionalRecordRepository;
import domain.Actor;
import domain.Curriculum;
import domain.ProfessionalRecord;
import domain.Worker;

@Service
@Transactional
public class ProfessionalRecordService {

	@Autowired
	private ProfessionalRecordRepository	professionalRecordRepository;

	@Autowired
	private ActorService					actorService;

	@Autowired
	private WorkerService					workerService;

	@Autowired
	private CurriculumService				curriculumService;


	public ProfessionalRecordService() {
		super();
	}

	public ProfessionalRecord create(final Integer curriculumId) {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		final ProfessionalRecord res = new ProfessionalRecord();

		Assert.notNull(res);

		return res;
	}

	public ProfessionalRecord findOne(final int professionalRecordId) {
		Assert.isTrue(professionalRecordId > 0);

		final ProfessionalRecord res = this.professionalRecordRepository.findOne(professionalRecordId);

		Assert.notNull(res);

		return res;
	}

	public ProfessionalRecord save(final ProfessionalRecord professionalRecord, final int curriculumId) {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		this.checkProfessionalRecord(professionalRecord);

		final ProfessionalRecord res = this.professionalRecordRepository.save(professionalRecord);

		final Curriculum curriculum = this.curriculumService.findOne(curriculumId);
		final Collection<ProfessionalRecord> professionalRecordsCopia = curriculum.getProfessionalRecords();
		professionalRecordsCopia.add(res);
		curriculum.setProfessionalRecords(professionalRecordsCopia);
		this.curriculumService.save(curriculum);

		return res;
	}

	public void delete(final ProfessionalRecord professionalRecord) {
		Assert.notNull(professionalRecord);
		Assert.isTrue(professionalRecord.getId() > 0);
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);
		final Curriculum curriculum = this.curriculumService.findCurriculumByProfessionalRecordId(professionalRecord.getId());
		final Collection<ProfessionalRecord> professionalsRecordCopia = curriculum.getProfessionalRecords();
		professionalsRecordCopia.remove(professionalRecord);
		curriculum.setProfessionalRecords(professionalsRecordCopia);
		this.curriculumService.save(curriculum);

		this.professionalRecordRepository.delete(professionalRecord);
	}

	public Collection<ProfessionalRecord> findAll() {

		Collection<ProfessionalRecord> result;

		result = this.professionalRecordRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void checkProfessionalRecord(final ProfessionalRecord professionalRecord) {
		Boolean res = true;

		if (professionalRecord.getCompany() == null || professionalRecord.getRole() == null || professionalRecord.getAttachment() == null || professionalRecord.getStartMoment() == null || professionalRecord.getComment() == null
			|| professionalRecord.getComment() == null)
			res = false;

		Assert.isTrue(res);
	}

	public ProfessionalRecord findOneToEdit(final int id) {
		Worker principal;

		// Principal must be a Company
		principal = this.workerService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		final ProfessionalRecord result = this.professionalRecordRepository.findOne(id);

		final Curriculum curriculum = this.curriculumService.findCurriculumByProfessionalRecordId(id);

		final Worker worker = curriculum.getWorker();
		Assert.notNull(result);

		Assert.isTrue(principal.equals(worker));

		return result;

	}

	public ProfessionalRecord saveEdit(final ProfessionalRecord professionalRecord) {
		return this.professionalRecordRepository.save(professionalRecord);
	}

}
