
package services;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ComplaintRepository;
import domain.Actor;
import domain.Administrator;
import domain.Complaint;
import domain.Runner;
import domain.Organizer;

@Service
@Transactional
public class ComplaintService {

	// Managed repository -------------------------------------------
	@Autowired
	private ComplaintRepository	complaintRepository;

	// Supported services -------------------------------------------

	@Autowired
	private ActorService	 actorService;
	
	@Autowired
	private RunnerService	 runnerService;

	@Autowired(required = false)
	@Qualifier("validator")
	private Validator		 validator;


	// Constructor methods -------------------------------------------
	public ComplaintService() {
		super();
	}

	// Simple CRUD methods ------------------------------------------

	public Complaint create() {
		Complaint res;
		Actor principal;

		// Principal must be a Runner
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Runner.class, principal);
		final Runner principalRunner = this.runnerService.findByPrincipal();

		res = new Complaint();

		String ticker = this.generateTicker();
		res.setDescription("");
		res.setAttachment("");
		Date moment = new Date();
		moment.setTime(moment.getTime() - 1);
		res.setMoment(moment);
		res.setTicker(ticker);
		res.setStatus(true);
		res.setRunner(principalRunner);

		return res;
	}

	public Complaint findOne(final int id) {
		final Complaint result = this.complaintRepository.findOne(id);

		Assert.notNull(result);

		return result;
	}

	public Collection<Complaint> findAll() {
		final Collection<Complaint> result = this.complaintRepository.findAll();

		return result;

	}

	public Complaint save (final Complaint c) {
		Assert.notNull(c);
		Actor principal;

		
		principal = this.actorService.findByPrincipal();
		
		// Principal must be one of this
		if(principal instanceof Organizer || principal instanceof Runner || principal instanceof Administrator) {
			//Actualizar el moment answer solo cuando se guarde despues de su creado, es decir, cuando sea editada
			//la queja ya que el runner solo puede crearla y, ademas, solo cuando el actor logueado sea el organizador
			// es decir, solo cuando se a�ada o edite la answer description
        if(c.getId() != 0 && principal instanceof Organizer && c.getAnswerDescription() != null  && !"".equals(c.getAnswerDescription()) ) {
			
		final Date answerMoment = new Date(System.currentTimeMillis() - 1000);
		c.setAnswerMoment(answerMoment);
		
		}

		}
		
		this.checkComplaint(c);

		return this.complaintRepository.save(c);
		
		
	}
	
	public void delete(final int ComplaintId) {
		Assert.notNull(ComplaintId);

		final Complaint c = this.findOne(ComplaintId);
		Assert.notNull(c);

		// Principal must be a Company
		final Actor principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Runner.class, principal);
		final Runner run = this.runnerService.findByPrincipal();
		//Another company can't delete other company's problems
		Assert.isTrue(c.getRunner().equals(run));
		//Complaint must be in draft mode
		Assert.isTrue(c.getStatus());

		this.complaintRepository.delete(ComplaintId);

	}

	public Complaint reconstruct(final Complaint c, final BindingResult binding) {
		
		Complaint res = new Complaint();

		if (c.getId() == 0)
			
			res = c;
		
		else {
			final Complaint copy = this.findOne(c.getId());

			res.setDescription(c.getDescription());
			res.setTicker(c.getTicker());
			res.setStatus(c.getStatus());
			res.setMoment(c.getMoment());
			res.setAttachment(c.getAttachment());
			res.setAnswerDescription(c.getAnswerDescription());
			res.setAnswerMoment(c.getAnswerMoment());

			res.setId(copy.getId());
			res.setVersion(copy.getVersion());
			res.setRunner(copy.getRunner());
			res.setMarathon(copy.getMarathon());
		}

		this.validator.validate(res, binding);
		return res;
		
	}

	// Other business methods ---------------------------------------------------------

	public void flush() {
		this.complaintRepository.flush();
	}
	
	public String generateTicker () {
		String ticker = "";
		final Date moment = new Date();
		String year = new Integer(moment.getYear() % 100).toString();
		String month = new Integer(moment.getMonth() + 1).toString();
		String day = new Integer(moment.getDate()).toString();
		if (year.length() == 1)
			year = "0" + year;
		if (month.length() == 1)
			month = "0" + month;
		if (day.length() == 1)
			day = "0" + day;

		final String l = RandomStringUtils.randomAlphanumeric(6);

		ticker = ticker + year + month + day + "-" + l.toUpperCase();
		
		return ticker;
	}

	public void checkComplaint(final Complaint a) {
		boolean check = true;

		if (a.getDescription() == null || a.getTicker() == null || a.getMoment() == null)
			check = false;

		Assert.isTrue(check);
	}

	public Collection<Complaint> getComplaintsByRunner(final int runnerId) {
		final Actor logueado = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Runner.class, logueado);
		return this.complaintRepository.findComplaintByRunner(runnerId);
	}

	public Collection<Complaint> getComplaintsByMarathon(final int MarathonId) {
		//    	Actor logueado = this.actorService.findByPrincipal();
		//    	Assert.isInstanceOf(Complaintor.class, logueado);
		return this.complaintRepository.findComplaintByMarathon(MarathonId);
	}

}
