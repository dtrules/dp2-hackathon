
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.FolderRepository;
import domain.Actor;
import domain.Folder;

@Service
@Transactional
public class FolderService {

	// Managed repository -------------------------------------------
	@Autowired
	private FolderRepository	folderRepository;

	// Supported services -------------------------------------------
	@Autowired
	private ActorService		actorService;


	// Constructor methods -------------------------------------------
	public FolderService() {
		super();
	}

	// Simple CRUD methods ------------------------------------------

	public Folder create() {
		final Folder res = new Folder();

		res.setOwner(this.actorService.findByPrincipal());

		return res;
	}

	public Folder findOne(final int id) {
		final Folder result = this.folderRepository.findOne(id);

		Assert.notNull(result);

		return result;
	}

	public Folder findOneToEdit(final int id) {
		final Folder result = this.folderRepository.findOne(id);

		Assert.notNull(result);

		//Assert.isTrue(this.checkIfSystemFolder(result) == false);
		Assert.isTrue(this.actorService.findByPrincipal().equals(result.getOwner()));

		return result;
	}

	public Collection<Folder> findAll() {
		final Collection<Folder> result = this.folderRepository.findAll();

		return result;
	}

	public void delete(final Folder folder) {
		Assert.notNull(folder);

		this.folderRepository.delete(folder);
	}

	public Folder save(final Folder folder) {
		Assert.notNull(folder);

		if (folder.getId() != 0) {
			final Folder old = this.findOne(folder.getId());
			Assert.isTrue(this.checkIfSystemFolder(old) == false);
		}

		Assert.notNull(folder.getName());
		Assert.notNull(folder.getOwner());

		Assert.isTrue(this.actorService.findByPrincipal().equals(folder.getOwner()));

		Assert.isTrue(this.checkIfSystemFolder(folder) == false);

		return this.folderRepository.save(folder);
	}

	// Other business methods ---------------------------------------------------------

	public void flush() {
		this.folderRepository.flush();
	}

	public Collection<Folder> findAllFoldersByActor(final int actorId) {
		// Principal must be an actor
		final Actor principal = this.actorService.findByPrincipal();
		Assert.notNull(principal);

		return this.folderRepository.findAllFoldersByActor(actorId);
	}

	public Folder findFolderByActor(final int actorId, final String name) {
		return this.folderRepository.findFolderByActor(actorId, name);
	}

	public void generateFolders(final int actorId) {
		final Actor owner = this.actorService.findOne(actorId);

		Assert.notNull(actorId);

		final Folder inbox = new Folder();
		inbox.setName("Inbox");
		inbox.setOwner(owner);

		final Folder outbox = new Folder();
		outbox.setName("Outbox");
		outbox.setOwner(owner);

		final Folder spambox = new Folder();
		spambox.setName("Spambox");
		spambox.setOwner(owner);

		final Folder trashbox = new Folder();
		trashbox.setName("Trashbox");
		trashbox.setOwner(owner);

		this.folderRepository.save(inbox);
		this.folderRepository.save(outbox);
		this.folderRepository.save(spambox);
		this.folderRepository.save(trashbox);
	}

	public boolean checkIfSystemFolder(final Folder folder) {
		boolean result = false;
		final String name = folder.getName();
		if (name.equals("Inbox") || name.equals("Outbox") || name.equals("Spambox") || name.equals("Trashbox"))
			result = true;
		return result;
	}

}
