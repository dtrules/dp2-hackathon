
package services;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.CurriculumRepository;
import domain.Curriculum;
import domain.EducationRecord;
import domain.PersonalRecord;
import domain.ProfessionalRecord;
import domain.Worker;
import forms.CurriculumForm;

@Service
@Transactional
public class CurriculumService {

	//Managed repository --------------------------------------

	@Autowired
	private CurriculumRepository		curriculumRepository;

	//Supported Services -----------------------------------

	@Autowired(required = false)
	private Validator					validator;

	@Autowired
	private ActorService				actorService;

	@Autowired
	private WorkerService				workerService;

	@Autowired
	private PersonalRecordService		personalRecordService;

	@Autowired
	private EducationRecordService		educationRecordService;

	@Autowired
	private ProfessionalRecordService	professionalRecordService;


	// Contructor methods
	public CurriculumService() {
		super();
	}

	public Curriculum create() {
		Worker principal;
		principal = this.workerService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);
		final Curriculum res = new Curriculum();

		final Collection<EducationRecord> educationRecords = Collections.<EducationRecord> emptySet();
		res.setEducationRecords(educationRecords);

		final PersonalRecord personalRecord = new PersonalRecord();
		res.setPersonalRecord(personalRecord);

		final Collection<ProfessionalRecord> professionalRecords = Collections.<ProfessionalRecord> emptySet();
		res.setProfessionalRecords(professionalRecords);

		res.setWorker(principal);

		String ticker = "";
		final Date moment = new Date();
		String year = new Integer(moment.getYear() % 100).toString();
		String month = new Integer(moment.getMonth() + 1).toString();
		String day = new Integer(moment.getDate()).toString();
		if (year.length() == 1)
			year = "0" + year;
		if (month.length() == 1)
			month = "0" + month;
		if (day.length() == 1)
			day = "0" + day;

		final String l = RandomStringUtils.randomAlphanumeric(6);

		ticker = ticker + year + month + day + "-" + l.toUpperCase();

		res.setTicker(ticker);

		Assert.notNull(res);

		return res;
	}

	public Curriculum findOne(final int curriculumId) {
		Assert.isTrue(curriculumId > 0);

		final Curriculum res = this.curriculumRepository.findOne(curriculumId);

		Assert.notNull(res);

		return res;
	}

	public Curriculum save(final Curriculum curriculum) {
		Worker principal;
		principal = this.workerService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		this.checkCurriculum(curriculum);
		final Curriculum res = this.curriculumRepository.save(curriculum);

		return res;
	}

	public void delete(final Curriculum curriculum) {
		Worker principal;
		principal = this.workerService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		Assert.notNull(curriculum);
		Assert.isTrue(curriculum.getId() > 0);

		curriculum.getEducationRecords().removeAll(curriculum.getEducationRecords());
		curriculum.getProfessionalRecords().removeAll(curriculum.getProfessionalRecords());
		this.personalRecordService.delete(curriculum.getPersonalRecord());
		this.curriculumRepository.delete(curriculum);
	}

	public Curriculum reconstruct(final CurriculumForm form, final BindingResult binding) {
		final Curriculum res = this.create();
		res.setPersonalRecord(form.getPersonalRecord());
		this.validator.validate(res, binding);
		return res;
	}

	public Collection<Curriculum> findAll() {

		Collection<Curriculum> result;

		result = this.curriculumRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	//Check Curriculum
	public void checkCurriculum(final Curriculum curriculum) {
		Boolean res = true;

		if (curriculum.getEducationRecords() == null || curriculum.getPersonalRecord() == null || curriculum.getProfessionalRecords() == null)
			res = false;

		Assert.isTrue(res);
	}

	public Curriculum findOneToEdit(final int id) {
		Worker principal;

		// Principal must be a Company
		principal = this.workerService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		final Curriculum result = this.curriculumRepository.findOne(id);

		final Worker worker = result.getWorker();
		Assert.notNull(result);

		// Debe ser el mismo rUNNER que al que pertenece la Curriculum
		Assert.isTrue(principal.equals(worker));

		return result;

	}

	public Curriculum findCurriculumByEducationRecordId(final int educationRecordId) {
		return this.curriculumRepository.findCurriculumByEducationRecordId(educationRecordId);
	}

	public Curriculum findCurriculumByProfessionalRecordId(final int professionalRecordId) {
		return this.curriculumRepository.findCurriculumByProfessionalRecordId(professionalRecordId);
	}

	public Curriculum findCurriculumByPersonalRecordId(final int personalRecordId) {
		return this.curriculumRepository.findCurriculumByPersonalRecordId(personalRecordId);
	}

	public Collection<Curriculum> findCurriculumByWorker(final int workerId) {
		return this.curriculumRepository.findCurriculumByWorker(workerId);
	}
}
