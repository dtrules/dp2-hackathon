
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.MessageRepository;
import domain.Actor;
import domain.Administrator;
import domain.Folder;
import domain.Message;

@Service
@Transactional
public class MessageService {

	// Managed repository -------------------------------------------
	@Autowired
	private MessageRepository		messageRepository;

	// Supported services -------------------------------------------
	@Autowired
	private ActorService			actorService;

	@Autowired
	private FolderService			folderService;

	@Autowired
	private CustomizationService	customizationService;


	// Constructor methods -------------------------------------------
	public MessageService() {
		super();
	}

	// Simple CRUD methods ------------------------------------------

	public Message create() {
		Message result;
		Actor principal;

		result = new Message();

		// Principal must be an actor
		principal = this.actorService.findByPrincipal();
		Assert.notNull(principal);

		result.setSender(principal);

		final Date moment = new Date(System.currentTimeMillis() - 1000);
		result.setMoment(moment);

		result.setTags(new ArrayList<String>());

		return result;
	}

	public Message findOne(final int id) {
		final Message result = this.messageRepository.findOne(id);

		Assert.notNull(result);

		return result;
	}

	public Collection<Message> findAll() {
		final Collection<Message> result = this.messageRepository.findAll();

		return result;
	}

	public Message save(final Message message) {
		Assert.notNull(message);

		message.setFolder(this.folderService.findFolderByActor(message.getSender().getId(), "Outbox"));

		this.checkMessage(message);

		// Creamos la copia para el outbox del receiver
		final Message copy = this.createCopyForInbox(message);
		this.messageRepository.save(copy);

		return this.messageRepository.save(message);
	}
	public void delete(final int messageId) {

		Actor principal;

		final Message m = this.findOne(messageId);

		Assert.notNull(m);

		// Principal must be an actor
		principal = this.actorService.findByPrincipal();
		Assert.notNull(principal);

		// Message must belong to any folder of the principal
		final Collection<Folder> folders = this.folderService.findAllFoldersByActor(principal.getId());
		Assert.isTrue(folders.contains(m.getFolder()));

		// Si en Trashbox, se borra, de lo contrario se mueve a Trashbox
		if (m.getFolder().getName().equals("Trashbox"))
			this.messageRepository.delete(m);
		else {
			final Folder f = this.folderService.findFolderByActor(principal.getId(), "Trashbox");
			m.setFolder(f);
			this.messageRepository.save(m);
		}

	}
	// Other business methods ---------------------------------------------------------

	public void flush() {
		this.messageRepository.flush();
	}

	public void checkMessage(final Message message) {
		boolean check = true;

		//Hacer comprobaciones
		if (message.getReceiver() == null || message.getFolder() == null || message.getBody() == null || message.getSubject() == null || message.getPriority() == null)
			check = false;

		if (message.getMoment() == null || message.getMoment().after(new Date()))
			check = false;

		Assert.isTrue(check);
	}

	public Message createCopyForInbox(final Message m) {
		final Message result = new Message();

		Folder inbox = new Folder();

		result.setSubject(m.getSubject());
		result.setBody(m.getBody());
		result.setMoment(m.getMoment());
		result.setTags(m.getTags());
		result.setPriority(m.getPriority());

		result.setSender(m.getSender());
		result.setReceiver(m.getReceiver());

		if (this.isSpam(m))
			inbox = this.folderService.findFolderByActor(m.getReceiver().getId(), "Spambox");
		else
			inbox = this.folderService.findFolderByActor(m.getReceiver().getId(), "Inbox");

		result.setFolder(inbox);

		return result;
	}
	public Collection<Message> findAllMessagesByFolder(final int folderId) {
		// Principal must be an actor
		final Actor principal = this.actorService.findByPrincipal();
		Assert.notNull(principal);

		return this.messageRepository.findAllMessagesByFolder(folderId);
	}

	public Collection<Message> findAllMessagesByFolderName(final String name) {
		// Principal must be an actor
		final Actor principal = this.actorService.findByPrincipal();
		Assert.notNull(principal);

		final Folder f = this.folderService.findFolderByActor(principal.getId(), name);

		return this.findAllMessagesByFolder(f.getId());
	}

	public void broadcast(final Message m) {
		// Principal must be an admin
		final Actor principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Administrator.class, principal);

		Assert.isTrue(m.getSender().equals(principal));

		final Collection<Actor> receivers = this.actorService.findAll();
		receivers.remove(principal);

		m.getTags().add("SYSTEM");

		for (final Actor a : receivers) {
			m.setReceiver(a);
			this.save(m);
		}
	}

	public boolean isSpam(final Message message) {
		boolean res = false;
		for (final String spamWord : this.customizationService.getSpamWords()) {
			res = message.getBody().contains(spamWord) || message.getSubject().contains(spamWord);
			if (res)
				break;

		}
		return res;
	}

}
