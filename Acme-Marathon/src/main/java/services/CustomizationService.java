
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.CustomizationRepository;
import security.Authority;
import domain.Customization;

@Service
@Transactional
public class CustomizationService {

	//Managed repository -----------------------------------------------------------

	@Autowired
	private CustomizationRepository	customizationRepository;

	//Supported Services ----------------------------------------------

	@Autowired
	private ActorService			actorService;


	// Constructor methods ---------------------------------------------------------
	public CustomizationService() {
		super();
	}

	//Simple CRUD methods-----------------------------------------------------------

	public Customization save(final Customization customization) {
		Assert.notNull(customization);
		Customization res = new Customization();
		this.actorService.checkAuth(Authority.ADMIN);
		this.checkCustomization(customization);
		this.actorService.checkSpamWords(customization.getSystemName());
		for (final String s : customization.getBrandNames())
			this.actorService.checkSpamWords(s);
		this.actorService.checkSpamWords(customization.getWelcomeMessageEn());
		this.actorService.checkSpamWords(customization.getWelcomeMessageEs());

		res = this.customizationRepository.save(customization);
		return res;
	}

	public Customization findCustomization() {
		Customization res;
		res = this.customizationRepository.findCustomization();
		Assert.notNull(res);
		return res;
	}

	//Other methods------------------------------------------------------------------
	public Collection<String> getSpamWords() {
		Customization cus;
		cus = this.findCustomization();
		Assert.notNull(cus);
		Collection<String> res;
		res = cus.getSpamWords();
		Assert.notNull(res);
		return res;
	}

	//Check customization------------------------------------------------------------
	public void checkCustomization(final Customization customization) {
		Boolean result = true;

		if (customization.getSystemName() == null || customization.getWelcomeMessageEn() == null || customization.getWelcomeMessageEs() == null || customization.getCountryCode() == null || customization.getSpamWords() == null)
			result = false;

		Assert.isTrue(result);
	}
}
