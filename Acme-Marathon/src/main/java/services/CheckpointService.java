
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.Validator;

import repositories.CheckpointRepository;
import domain.Actor;
import domain.Checkpoint;
import domain.Marathon;
import domain.Organizer;

@Service
@Transactional
public class CheckpointService {

	// Managed repository -------------------------------------------
	@Autowired
	private CheckpointRepository	checkpointRepository;

	// Supported services -------------------------------------------

	@Autowired
	private ActorService			actorService;

	@Autowired
	private OrganizerService		organizerService;

	@Autowired(required = false)
	@Qualifier("validator")
	private Validator				validator;


	// Constructor methods -------------------------------------------
	public CheckpointService() {
		super();
	}

	// Simple CRUD methods ------------------------------------------

	public Checkpoint create() {
		Checkpoint res;
		res = new Checkpoint();
		Actor principal;

		// Principal must be a Organizer
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);
		final Organizer principalOrganizer = this.organizerService.findByPrincipal();

		final Marathon marathon = new Marathon();

		res.setMarathon(marathon);

		return res;
	}

	public Checkpoint findOne(final int id) {
		final Checkpoint result = this.checkpointRepository.findOne(id);

		Assert.notNull(result);

		return result;
	}

	public Collection<Checkpoint> findAll() {
		final Collection<Checkpoint> result = this.checkpointRepository.findAll();

		return result;

	}

	public Checkpoint save(final Checkpoint checkpoint) {
		Assert.notNull(checkpoint);
		Actor principal;

		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);

		this.checkCheckpoint(checkpoint);

		Assert.isTrue(checkpoint.getMoment().after(checkpoint.getMarathon().getMarathonDate()));

		return this.checkpointRepository.save(checkpoint);
	}

	// Other business methods ---------------------------------------------------------

	public void flush() {
		this.checkpointRepository.flush();
	}

	public Checkpoint findOneToEdit(final int id) {
		Actor principal;

		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);

		final Checkpoint result = this.checkpointRepository.findOne(id);

		Assert.notNull(result);

		Assert.isTrue(principal.equals(result.getMarathon().getOrganizer()));

		return result;

	}

	public void checkCheckpoint(final Checkpoint a) {
		boolean check = true;

		if (a.getNumber() == null || a.getMoment() == null || a.getMarathon() == null)
			check = false;

		Assert.isTrue(check);
	}

	public Collection<Checkpoint> findByOrganizer(final int organizerId) {
		final Organizer organizer = this.organizerService.findOne(organizerId);

		final Collection<Checkpoint> res = this.findAll();
		final Collection<Checkpoint> checkpointsOrganizer = new ArrayList<Checkpoint>();

		for (final Checkpoint s : res)
			if (s.getMarathon().getOrganizer().getId() == organizerId)
				checkpointsOrganizer.add(s);
		return checkpointsOrganizer;
	}

	public Collection<Checkpoint> findByMarathon(final int marathonId) {
		return this.checkpointRepository.findByMarathonId(marathonId);
	}
}
