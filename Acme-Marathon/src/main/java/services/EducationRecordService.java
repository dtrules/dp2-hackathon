
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.EducationRecordRepository;
import domain.Actor;
import domain.Curriculum;
import domain.EducationRecord;
import domain.Worker;

@Service
@Transactional
public class EducationRecordService {

	@Autowired
	private EducationRecordRepository	educationRecordRepository;

	@Autowired
	private ActorService				actorService;

	@Autowired
	private WorkerService				workerService;

	@Autowired
	private CurriculumService			curriculumService;


	public EducationRecordService() {
		super();
	}

	public EducationRecord create(final Integer curriculumId) {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		final EducationRecord res = new EducationRecord();

		Assert.notNull(res);

		return res;
	}

	public EducationRecord findOne(final int educationRecordId) {
		Assert.isTrue(educationRecordId > 0);

		final EducationRecord res = this.educationRecordRepository.findOne(educationRecordId);

		Assert.notNull(res);

		return res;
	}

	public EducationRecord save(final EducationRecord educationRecord, final int curriculumId) {
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		this.checkEducationRecord(educationRecord);

		final EducationRecord res = this.educationRecordRepository.save(educationRecord);

		final Curriculum curriculum = this.curriculumService.findOne(curriculumId);
		final Collection<EducationRecord> educationRecordsCopia = curriculum.getEducationRecords();
		educationRecordsCopia.add(res);
		curriculum.setEducationRecords(educationRecordsCopia);
		this.curriculumService.save(curriculum);

		return res;
	}

	public void delete(final EducationRecord educationRecord) {
		Assert.notNull(educationRecord);
		Assert.isTrue(educationRecord.getId() > 0);
		Actor principal;
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);
		final Curriculum curriculum = this.curriculumService.findCurriculumByEducationRecordId(educationRecord.getId());
		final Collection<EducationRecord> educationsRecordCopia = curriculum.getEducationRecords();
		educationsRecordCopia.remove(educationRecord);
		curriculum.setEducationRecords(educationsRecordCopia);
		this.curriculumService.save(curriculum);

		this.educationRecordRepository.delete(educationRecord);
	}

	public Collection<EducationRecord> findAll() {

		Collection<EducationRecord> result;

		result = this.educationRecordRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void checkEducationRecord(final EducationRecord educationRecord) {
		Boolean res = true;

		if (educationRecord.getTitle() == null || educationRecord.getEndMoment() == null || educationRecord.getInstitution() == null || educationRecord.getAttachment() == null || educationRecord.getStartMoment() == null
			|| educationRecord.getComment() == null)
			res = false;

		Assert.isTrue(res);
	}

	public EducationRecord findOneToEdit(final int id) {
		Worker principal;

		// Principal must be a Company
		principal = this.workerService.findByPrincipal();
		Assert.isInstanceOf(Worker.class, principal);

		final EducationRecord result = this.educationRecordRepository.findOne(id);

		final Curriculum curriculum = this.curriculumService.findCurriculumByEducationRecordId(id);

		final Worker worker = curriculum.getWorker();
		Assert.notNull(result);

		Assert.isTrue(principal.equals(worker));

		return result;

	}

	public EducationRecord saveEdit(final EducationRecord educationRecord) {
		return this.educationRecordRepository.save(educationRecord);
	}

}
