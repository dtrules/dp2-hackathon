
package services;

import java.util.ArrayList;
import java.util.Collection;

import domain.Marathon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.CategoryRepository;
import security.Authority;
import domain.Category;

@Service
@Transactional
public class CategoryService {

	//Managed repository ------------------------------------------------

	@Autowired
	private CategoryRepository	categoryRepository;

	//Other repository -----------------------------------------------------

	@Autowired
	private ActorService		actorService;

	@Autowired
	private MarathonService		marathonService;


	public CategoryService() {
		super();
	}

	//Simple CRUD methods ------------------------

	public Category create() {

		this.actorService.checkAuth(Authority.ADMIN);
		final Category res = new Category();
		final Collection<Category> subCategories = new ArrayList<Category>();

		res.setSubCategories(subCategories);

		return res;
	}
	public Category save(final Category category) {
		Assert.notNull(category);
		this.actorService.checkAuth(Authority.ADMIN);

		Category result;
		this.actorService.checkSpamWords(category.getName());
		this.actorService.checkSpamWords(category.getNombre());

		if (category.getParentCategory() == null)
			category.setParentCategory(this.findCATEGORY());

		result = this.categoryRepository.save(category);

		return result;
	}

	public void delete(final Category category) {

		Assert.notNull(category);

		this.actorService.checkAuth(Authority.ADMIN);

		Assert.isTrue(this.isDeleteable(category.getId()));
		category.getParentCategory().getSubCategories().remove(category);

		this.categoryRepository.delete(category);

	}

	public boolean isDeleteable(final int categoryId) {
		boolean result = false;
		final Collection<Marathon> res = this.marathonService.findMarathonsByCategory(categoryId);
		final Category category = this.findOne(categoryId);
		if (res.isEmpty() && this.isParentCategory(category)==false) {
			result = true;
		}
		return result;
	}

	public Category findOne(final int categoryId) {
		Assert.isTrue(categoryId != 0);
		Category result;

		result = this.categoryRepository.findOne(categoryId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Category> findAll() {

		Collection<Category> result;

		result = this.categoryRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Boolean isParentCategory(final Category category) {
		Boolean res = false;
		if (category.getName().equals("CATEGORY"))
			res = true;
		return res;
	}

	public Category findCATEGORY() {
		return this.categoryRepository.findCategory();
	}
}
