
package services;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.MarathonRepository;
import domain.Actor;
import domain.Category;
import domain.Marathon;
import domain.Message;
import domain.Organizer;
import domain.Runner;

@Service
@Transactional
public class MarathonService {

	// Managed repository -------------------------------------------
	@Autowired
	private MarathonRepository	marathonRepository;

	// Supported services -------------------------------------------
	@Autowired
	private OrganizerService	organizerService;

	@Autowired
	private ActorService		actorService;

	@Autowired
	private CheckpointService	checkpointService;

	@Autowired
	private MessageService		messageService;

	@Autowired
	private RunnerService		runnerService;


	// Constructor methods -------------------------------------------
	public MarathonService() {
		super();
	}

	public Marathon create() {
		Marathon result;
		Actor principal;
		result = new Marathon();

		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);

		final Organizer o = this.organizerService.findByPrincipal();
		final Category category = new Category();

		String ticker = "";
		final Date moment = new Date();
		String year = new Integer(moment.getYear() % 100).toString();
		String month = new Integer(moment.getMonth() + 1).toString();
		String day = new Integer(moment.getDate()).toString();
		if (year.length() == 1)
			year = "0" + year;
		if (month.length() == 1)
			month = "0" + month;
		if (day.length() == 1)
			day = "0" + day;

		final String l = RandomStringUtils.randomAlphanumeric(6);

		ticker = ticker + year + month + day + "-" + l.toUpperCase();

		result.setOrganizer(o);
		result.setCategory(category);
		result.setTicker(ticker);

		return result;
	}

	public Marathon findOne(final int id) {
		final Marathon result = this.marathonRepository.findOne(id);

		Assert.notNull(result);

		return result;
	}

	public Collection<Marathon> findAll() {
		final Collection<Marathon> result = this.marathonRepository.findAll();

		return result;
	}

	public Marathon save(final Marathon marathon) {
		Assert.notNull(marathon);
		Actor principal;

		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);

		this.checkMarathon(marathon);

		Assert.isTrue(marathon.getInscriptionStart().before(marathon.getInscriptionEnd()));
		Assert.isTrue(marathon.getInscriptionEnd().before(marathon.getMarathonDate()));
		Assert.isTrue(marathon.getInscriptionStart().before(marathon.getMarathonDate()));

		if (marathon.getId() != 0) {
			Assert.isTrue(principal.equals(marathon.getOrganizer()));
			final Message message = this.messageService.create();
			message.setBody("Marathon Information has changed- La información de la maratón ha cambiado");
			message.getTags().add("SYSTEM");
			message.setSubject("Marathon Information-Información de la Maratón");
			message.setPriority("HIGH");
			this.broadcast(message, marathon);
		}
		return this.marathonRepository.save(marathon);
	}

	public void flush() {
		this.marathonRepository.flush();
	}

	public void checkMarathon(final Marathon marathon) {
		boolean check = true;
		final Date now = new Date();

		if (marathon.getTicker() == null || marathon.getDescription() == null || marathon.getAddress() == null || marathon.getCategory() == null || marathon.getOrganizer() == null || marathon.getInscriptionEnd() == null
			|| marathon.getInscriptionStart() == null || marathon.getMarathonDate() == null)
			check = false;

		Assert.isTrue(check);
	}

	public Marathon findOneToEdit(final int id) {
		Actor principal;

		// Principal must be a Company
		principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);

		final Marathon result = this.marathonRepository.findOne(id);

		Assert.notNull(result);

		// Debe ser el mismo Company que al que pertenece la Position
		Assert.isTrue(principal.equals(result.getOrganizer()));

		return result;

	}

	public Collection<Marathon> listByOrganizer(final int organizerId) {
		return this.marathonRepository.findByOrganizer(organizerId);
	}

	public Integer numberOfRunnerApplicationsByMarathon(final int marathonId) {
		return this.marathonRepository.numberOfRunnerApplicationsByMarathon(marathonId);
	}

	public Collection<Marathon> findMarathonsByCategory(final int categoryId) {
		return this.marathonRepository.findMarathonsByCategory(categoryId);
	}

	public void broadcast(final Message m, final Marathon ma) {
		// Principal must be an admin
		final Actor principal = this.actorService.findByPrincipal();
		Assert.isInstanceOf(Organizer.class, principal);

		Assert.isTrue(m.getSender().equals(principal));

		final Collection<Runner> runners = this.runnerService.findRunnerByRunnerApplication(ma.getId());
		m.getTags().add("SYSTEM");

		for (final Runner a : runners) {
			m.setReceiver(this.actorService.findOne(a.getId()));
			this.messageService.save(m);
		}
	}
}
