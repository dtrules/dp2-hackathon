
package converters;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Complaint;


@Component
@Transactional
public class ComplaintToStringConverter extends EntityToString<Complaint> {

}
