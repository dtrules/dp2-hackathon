
package converters;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Organizer;
import domain.Runner;

@Component
@Transactional
public class OrganizerToStringConverter extends EntityToString<Organizer> {

}
