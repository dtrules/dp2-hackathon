
package converters;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import repositories.WorkerApplicationRepository;

import domain.WorkerApplication;

@Component
@Transactional
public class StringToWorkerApplicationConverter extends StringToEntity<WorkerApplication, WorkerApplicationRepository> {

}