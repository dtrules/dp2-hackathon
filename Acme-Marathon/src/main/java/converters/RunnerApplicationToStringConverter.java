
package converters;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.RunnerApplication;

@Component
@Transactional
public class RunnerApplicationToStringConverter extends EntityToString<RunnerApplication> {

}
