
package converters;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.ComplaintRepository;

import domain.Complaint;


@Component
@Transactional
public class StringToComplaintConverter extends StringToEntity <Complaint, ComplaintRepository> {

}
