
package converters;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Checkpoint;

@Component
@Transactional
public class CheckpointToStringConverter extends EntityToString<Checkpoint> {

}
