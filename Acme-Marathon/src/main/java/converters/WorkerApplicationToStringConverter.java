
package converters;


import domain.WorkerApplication;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class WorkerApplicationToStringConverter extends EntityToString<WorkerApplication> {

}
