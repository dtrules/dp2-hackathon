
package converters;

import domain.Folder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.FolderRepository;

@Component
@Transactional
public class StringToFolderConverter extends StringToEntity<Folder, FolderRepository> {

}
