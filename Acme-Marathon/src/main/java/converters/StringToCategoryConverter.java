
package converters;

import domain.Category;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.CategoryRepository;

@Component
@Transactional
public class StringToCategoryConverter extends StringToEntity<Category, CategoryRepository> {

}
