
package converters;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Payment;


@Component
@Transactional
public class PaymentToStringConverter extends EntityToString<Payment> {

}
