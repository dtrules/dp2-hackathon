
package converters;

import domain.Folder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class FolderToStringConverter extends EntityToString<Folder> {

}
