
package converters;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.RunnerRepository;
import domain.Runner;

@Component
@Transactional
public class StringToRunnerConverter extends StringToEntity<Runner, RunnerRepository> {

}
