
package converters;

import domain.Worker;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class WorkerToStringConverter extends EntityToString<Worker> {

}
