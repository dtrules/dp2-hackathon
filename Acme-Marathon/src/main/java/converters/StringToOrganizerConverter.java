
package converters;

import domain.Organizer;
import domain.Worker;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.OrganizerRepository;
import repositories.WorkerRepository;

@Component
@Transactional
public class StringToOrganizerConverter extends StringToEntity<Organizer, OrganizerRepository> {

}
