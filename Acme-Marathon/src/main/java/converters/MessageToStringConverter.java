
package converters;

import domain.Message;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class MessageToStringConverter extends EntityToString<Message> {

}
