
package converters;

import domain.Customization;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class CustomizationToStringConverter extends EntityToString<Customization> {

}
