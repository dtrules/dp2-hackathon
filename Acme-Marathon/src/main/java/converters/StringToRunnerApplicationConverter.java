
package converters;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.RunnerApplicationRepository;
import domain.RunnerApplication;

@Component
@Transactional
public class StringToRunnerApplicationConverter extends StringToEntity<RunnerApplication, RunnerApplicationRepository> {

}
