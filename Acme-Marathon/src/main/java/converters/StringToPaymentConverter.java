
package converters;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import repositories.PaymentRepository;

import domain.Payment;


@Component
@Transactional
public class StringToPaymentConverter extends StringToEntity <Payment, PaymentRepository> {

}
