
package converters;

import domain.Worker;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.WorkerRepository;

@Component
@Transactional
public class StringToPersonalRecordConverter extends StringToEntity<Worker, WorkerRepository> {

}
