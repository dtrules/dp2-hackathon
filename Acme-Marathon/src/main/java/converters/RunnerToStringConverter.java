
package converters;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Runner;

@Component
@Transactional
public class RunnerToStringConverter extends EntityToString<Runner> {

}
