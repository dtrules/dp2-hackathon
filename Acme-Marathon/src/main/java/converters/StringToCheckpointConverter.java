
package converters;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.CheckpointRepository;
import domain.Checkpoint;

@Component
@Transactional
public class StringToCheckpointConverter extends StringToEntity<Checkpoint, CheckpointRepository> {

}
