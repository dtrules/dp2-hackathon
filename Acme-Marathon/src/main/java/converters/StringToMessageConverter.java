
package converters;

import domain.Message;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.MessageRepository;

@Component
@Transactional
public class StringToMessageConverter extends StringToEntity<Message, MessageRepository> {

}
