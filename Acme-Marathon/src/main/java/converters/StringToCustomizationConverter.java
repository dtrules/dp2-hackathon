
package converters;

import domain.Customization;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.CustomizationRepository;

@Component
@Transactional
public class StringToCustomizationConverter extends StringToEntity<Customization, CustomizationRepository> {

}
