
package converters;

import domain.Category;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class CategoryToStringConverter extends EntityToString<Category> {

}
