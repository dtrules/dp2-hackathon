
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import security.LoginService;
import utilities.AbstractTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class RunnerServiceTest extends AbstractTest {

	@Autowired
	private RunnerService		runnerService;

	@Autowired
	private ComplaintService	complaintService;


	protected void testListComplaints(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			this.complaintService.getComplaintsByRunner(LoginService.getPrincipal().getId());
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverListComplaints() {
		final Object testingData[][] = {
			{
				//Test positivo consiguiendo los complaints de un runner
				"runner1", null,
			}, {

				//test negativos
				"admin", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testListComplaints((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testCreateComplaints(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			this.complaintService.create();
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverCreateComplaints() {
		final Object testingData[][] = {
			{
				//Test positivo consiguiendo los complaints de un runner
				"runner1", null,
			}, {

				//test negativos
				"admin", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testCreateComplaints((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
}
