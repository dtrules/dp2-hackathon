
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import security.LoginService;
import utilities.AbstractTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class WorkerServiceTest extends AbstractTest {

	@Autowired
	private WorkerService				workerService;

	@Autowired
	private WorkerApplicationService	workerApplicationService;

	@Autowired
	private CurriculumService			curriculumService;

	@Autowired
	private ProfessionalRecordService	professionalRecordService;


	protected void testListApplications(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.workerApplicationService.getWorkerApplicationsByWorker(LoginService.getPrincipal().getId());
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverListApplications() {

		final Object testingData[][] = {
			//Tests positivos, conun runer y un organizer
			{
				"worker1", null,
			}, {
				"runner1", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testListApplications((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testCreateApplications(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.workerApplicationService.create();
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverCreateApplications() {

		final Object testingData[][] = {
			//Tests positivos, conun runer y un organizer
			{
				"worker1", null,
			}, {
				"runner1", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testCreateApplications((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testListCurriculum(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.curriculumService.findCurriculumByWorker(LoginService.getPrincipal().getId());
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverListCurriculum() {

		final Object testingData[][] = {
			//Tests positivos, conun runer y un organizer
			{
				"worker1", null,
			}, {
				"runner1", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testCreateApplications((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

}
