
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Category;
import domain.Complaint;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class AdministratorServiceTest extends AbstractTest {

	// Service under test---------------------------
	@Autowired
	private AdministratorService	administratorService;

	@Autowired
	private CategoryService			categoryService;

	@Autowired
	private ComplaintService		complaintService;

	@Autowired
	private SponsorshipService		sponsorshipService;


	protected void testCreateAdministrator(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			this.administratorService.create();
			super.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverCreateAdministrator() {

		// sentence coverage 100%
		//12. An actor who is authenticated as an administrator must be able to:
		//1. Create user accounts for new administrators.
		final Object testingData[][] = {
			{
				//Test positivo, crear una dministrador siendo un administrador
				"admin", null,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testCreateAdministrator((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	// List categories

	protected void testListCategories(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			this.categoryService.findAll();
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);

	}

	@Test
	public void driverListCategories() {

		// sentence coverage 100%
		//		
		//		12. An actor who is authenticated as an administrator must be able to:
		//			Manage the catalogue of categories, which includes listing, showing, creating,
		//			updating, and deleting them.
		final Object testingData[][] = {
			{
				//Test positivos logeado como un administrador
				"admin", null,
			}, {
				// Test negativo, no se puede listar con un usuario no logeado
				"null", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testListCategories((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testCreateCategory(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {
			super.authenticate(username);
			this.categoryService.create();
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverCreateCategory() {

		// sentence coverage 100%

		//12. An actor who is authenticated as an administrator must be able to:
		//			Manage the catalogue of categories, which includes listing, showing, creating,
		//			updating, and deleting them.
		final Object testingData[][] = {
			{
				//Test positivo logeado como un admin
				"admin", null,
			}, {

				//Test negativo no se puede hacer logeado commo  un organizer
				"organizer1", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testCreateCategory((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testDeleteCategory(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {
			super.authenticate(username);
			final Category cat = this.categoryService.findOne(509);
			this.categoryService.delete(cat);
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverDeleteCategory() {

		// sentence coverage 100%

		//12. An actor who is authenticated as an administrator must be able to:
		//			Manage the catalogue of categories, which includes listing, showing, creating,
		//			updating, and deleting them.
		final Object testingData[][] = {
			{
				//Test positivo logeado como un admin
				"admin", IllegalArgumentException.class,
			}, {

				//Test negativo, no se puede hacer logeado como  un organizer
				"organizer1", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testDeleteCategory((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testMaxMarathonsRunner(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {
			super.authenticate(username);
			this.administratorService.maxMarathonsRunner();
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverMaxMarathonsRunner() {

		// sentence coverage 100%

		//12. An actor who is authenticated as an administrator must be able to:
		//		4. Display a dashboard with the following information:
		//			 The average, the minimum, the maximum, and the standard deviation of the
		//			number of marathons per runner.
		final Object testingData[][] = {
			{
				//Test positivo logeado como un admin
				"admin", null,
			}, {

				//Test negativo, no se puede hacer logeado como  un organizer
				"organizer1", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testMaxMarathonsRunner((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testMinMarathonsRunner(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {
			super.authenticate(username);
			this.administratorService.minMarathonsRunner();
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverMinMarathonsRunner() {

		// sentence coverage 100%

		//12. An actor who is authenticated as an administrator must be able to:
		//		4. Display a dashboard with the following information:
		//			 The average, the minimum, the maximum, and the standard deviation of the
		//			number of marathons per runner.
		final Object testingData[][] = {
			{
				//Test positivo logeado como un admin
				"admin", null,
			}, {

				//Test negativo no se puede hacer logeado commo  un organizer
				"organizer1", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testMinMarathonsRunner((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testAvgMarathonsRunner(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {
			super.authenticate(username);
			this.administratorService.avgMarathonsRunner();
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverAvgMarathonsRunner() {

		// sentence coverage 100%

		//12. An actor who is authenticated as an administrator must be able to:
		//		4. Display a dashboard with the following information:
		//			 The average, the minimum, the maximum, and the standard deviation of the
		//			number of marathons per runner.
		final Object testingData[][] = {
			{
				//Test positivo logeado como un admin
				"admin", null,
			}, {

				//Test negativo no se puede hacer logeado commo  un organizer
				"organizer1", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testAvgMarathonsRunner((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testStddevMarathonsRunner(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {
			super.authenticate(username);
			this.administratorService.stddevMarathonsRunner();
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverStddevMarathonsRunner() {

		// sentence coverage 100%

		//12. An actor who is authenticated as an administrator must be able to:
		//		4. Display a dashboard with the following information:
		//			 The average, the minimum, the maximum, and the standard deviation of the
		//			number of marathons per runner.
		final Object testingData[][] = {
			{
				//Test positivo logeado como un admin
				"admin", null,
			}, {

				//Test negativo no se puede hacer logeado commo  un organizer
				"organizer1", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testStddevMarathonsRunner((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testStatusComplaint(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {
			super.authenticate(username);
			final Complaint c = this.complaintService.findOne(587);
			c.setStatus(false);
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverStatusComplaint() {

		// sentence coverage 100%
		final Object testingData[][] = {
			{
				//Test positivo logeado como un admin
				"admin", null,
			}, {

				//Test negativo usuario no logeado
				"null", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testStatusComplaint((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testMinComplaintsMarathon(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {
			super.authenticate(username);
			this.administratorService.minComplaintsMarathon();

			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverMinComplaintsMarathon() {

		// sentence coverage 100%
		final Object testingData[][] = {
			{
				//Test positivo logeado como un admin
				"admin", null,
			}, {

				//Test negativo logeado commo  un organizer
				"organizer1", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testMinComplaintsMarathon((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testMaxComplaintsMarathon(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {
			super.authenticate(username);
			this.administratorService.maxComplaintsMarathon();

			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverMaxComplaintsMarathon() {

		// sentence coverage 100%
		final Object testingData[][] = {
			{
				//Test positivo logeado como un admin
				"admin", null,
			}, {

				//Test negativo logeado commo  un organizer
				"organizer1", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testMaxComplaintsMarathon((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testAvgComplaintsMarathon(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {
			super.authenticate(username);
			this.administratorService.avgComplaintsMarathon();

			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverAvgComplaintsMarathon() {

		// sentence coverage 100%
		final Object testingData[][] = {
			{
				//Test positivo logeado como un admin
				"admin", null,
			}, {

				//Test negativo logeado commo  un organizer
				"organizer1", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testAvgComplaintsMarathon((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testComputeScore(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.administratorService.obtainScore(513);
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverComputeScore() {

		final Object testingData[][] = {
			//Tests positivos, conun runer y un organizer
			{
				"admin", null,
			}, {
				//Test negativo de un usuario no logeado
				"organizer1", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testComputeScore((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testMinSponsorshipsSponsor(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {
			super.authenticate(username);
			this.administratorService.minSponsorshipsSponsor();

			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverMinSponsorshipsSponsor() {

		// sentence coverage 100%
		final Object testingData[][] = {
			{
				//Test positivo logeado como un admin
				"admin", null,
			}, {

				//Test negativo logeado commo  un organizer
				"organizer1", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testMinSponsorshipsSponsor((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testMaxSponsorshipsSponsor(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {
			super.authenticate(username);
			this.administratorService.maxSponsorshipsSponsor();

			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverMaxSponsorshipsSponsor() {

		// sentence coverage 100%
		final Object testingData[][] = {
			{
				//Test positivo logeado como un admin
				"admin", null,
			}, {

				//Test negativo logeado commo  un organizer
				"organizer1", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testMaxSponsorshipsSponsor((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testTop5(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {
			super.authenticate(username);
			this.administratorService.top5SponsorsTermsSponsorships();

			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverTop5() {

		// sentence coverage 100%
		final Object testingData[][] = {
			{
				//Test positivo logeado como un admin
				"admin", null,
			}, {

				//Test negativo logeado commo  un organizer
				"organizer1", IllegalArgumentException.class,
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.testTop5((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
}
// Templates -------------------------------------------------------
//
//	protected void testAvgAppRookie(final String username, final Class<?> expected) {
//
//		Class<?> caught = null;
//
//		try {
//
//			System.out.println("------------------------------------TEST AVG SALARY --------------------------------------------------");
//
//			super.authenticate(username);
//
//			@SuppressWarnings("unused")
//			final Double avgSalary = this.administratorService.avgAppRookie();
//			final String print = avgSalary.toString();
//			System.out.println(print);
//
//			super.unauthenticate();
//			System.out.println("----------------------------PASSED-------------------------------\r");
//		} catch (final Throwable oops) {
//			caught = oops.getClass();
//		}
//
//		this.checkExceptions(expected, caught);
//
//	}
//
//	protected void testMinAppRookie(final String username, final Class<?> expected) {
//
//		Class<?> caught = null;
//
//		try {
//
//			System.out.println("------------------------------------TEST MIN SALARY --------------------------------------------------");
//
//			super.authenticate(username);
//
//			@SuppressWarnings("unused")
//			final Double minSalary = this.administratorService.minAppRookie();
//			final String print = minSalary.toString();
//			System.out.println(print);
//
//			super.unauthenticate();
//			System.out.println("----------------------------PASSED-------------------------------\r");
//		} catch (final Throwable oops) {
//			caught = oops.getClass();
//		}
//
//		this.checkExceptions(expected, caught);
//
//	}
//
//	protected void testMaxAppRookie(final String username, final Class<?> expected) {
//
//		Class<?> caught = null;
//
//		try {
//
//			System.out.println("------------------------------------TEST MAX SALARY --------------------------------------------------");
//
//			super.authenticate(username);
//
//			@SuppressWarnings("unused")
//			final Double maxSalary = this.administratorService.maxAppRookie();
//			final String print = maxSalary.toString();
//			System.out.println(print);
//
//			super.unauthenticate();
//			System.out.println("----------------------------PASSED-------------------------------\r");
//		} catch (final Throwable oops) {
//			caught = oops.getClass();
//		}
//
//		this.checkExceptions(expected, caught);
//
//	}
//
//	protected void testStddevAppRookie(final String username, final Class<?> expected) {
//
//		Class<?> caught = null;
//
//		try {
//
//			System.out.println("------------------------------------TEST STDDEV SALARY --------------------------------------------------");
//
//			super.authenticate(username);
//
//			@SuppressWarnings("unused")
//			final Double stddevSalary = this.administratorService.stddevAppRookie();
//			final String print = stddevSalary.toString();
//			System.out.println(print);
//
//			super.unauthenticate();
//			System.out.println("----------------------------PASSED-------------------------------\r");
//		} catch (final Throwable oops) {
//			caught = oops.getClass();
//		}
//
//		this.checkExceptions(expected, caught);
//
//	}
//
//	protected void testAvgAuditScore(final String username, final Class<?> expected) {
//
//		Class<?> caught = null;
//
//		try {
//
//			System.out.println("------------------------------------TEST AVG CURRICULAS--------------------------------------------------");
//
//			super.authenticate(username);
//
//			@SuppressWarnings("unused")
//			final Double avgCurriculas = this.administratorService.avgAuditScore();
//			final String print = avgCurriculas.toString();
//			System.out.println(print);
//
//			super.unauthenticate();
//			System.out.println("----------------------------PASSED-------------------------------\r");
//		} catch (final Throwable oops) {
//			caught = oops.getClass();
//		}
//
//		this.checkExceptions(expected, caught);
//
//	}
//
//	protected void testMinAuditScore(final String username, final Class<?> expected) {
//
//		Class<?> caught = null;
//
//		try {
//
//			System.out.println("------------------------------------TEST MIN CURRICULAS--------------------------------------------------");
//
//			super.authenticate(username);
//
//			@SuppressWarnings("unused")
//			final Integer minCurriculas = this.administratorService.minAuditScore();
//			final String print = minCurriculas.toString();
//			System.out.println(print);
//
//			super.unauthenticate();
//			System.out.println("----------------------------PASSED-------------------------------\r");
//		} catch (final Throwable oops) {
//			caught = oops.getClass();
//		}
//
//		this.checkExceptions(expected, caught);
//
//	}
//
//	protected void testMaxAuditScore(final String username, final Class<?> expected) {
//
//		Class<?> caught = null;
//
//		try {
//
//			System.out.println("------------------------------------TEST MAX CURRICULAS --------------------------------------------------");
//
//			super.authenticate(username);
//
//			@SuppressWarnings("unused")
//			final Integer maxCurriculas = this.administratorService.maxAuditScore();
//			final String print = maxCurriculas.toString();
//			System.out.println(print);
//
//			super.unauthenticate();
//			System.out.println("----------------------------PASSED-------------------------------\r");
//		} catch (final Throwable oops) {
//			caught = oops.getClass();
//		}
//
//		this.checkExceptions(expected, caught);
//
//	}
//
//	protected void testStddevAuditScore(final String username, final Class<?> expected) {
//
//		Class<?> caught = null;
//
//		try {
//
//			System.out.println("------------------------------------TEST STDDEV CURRICULAS  --------------------------------------------------");
//
//			super.authenticate(username);
//
//			@SuppressWarnings("unused")
//			final Double stddevCurriculas = this.administratorService.stddevAuditScore();
//			final String print = stddevCurriculas.toString();
//			System.out.println(print);
//
//			super.unauthenticate();
//			System.out.println("----------------------------PASSED-------------------------------\r");
//		} catch (final Throwable oops) {
//			caught = oops.getClass();
//		}
//
//		this.checkExceptions(expected, caught);
//
//	}
//
//	protected void testAvgItemsProvider(final String username, final Class<?> expected) {
//
//		Class<?> caught = null;
//
//		try {
//
//			System.out.println("------------------------------------TEST AVG RESULTS --------------------------------------------------");
//
//			super.authenticate(username);
//
//			@SuppressWarnings("unused")
//			final Double avgResults = this.administratorService.avgItemsProvider();
//			final String print = avgResults.toString();
//			System.out.println(print);
//
//			super.unauthenticate();
//			System.out.println("----------------------------PASSED-------------------------------\r");
//		} catch (final Throwable oops) {
//			caught = oops.getClass();
//		}
//
//		this.checkExceptions(expected, caught);
//
//	}
//
//	protected void testMinitemsProvider(final String username, final Class<?> expected) {
//
//		Class<?> caught = null;
//
//		try {
//
//			System.out.println("------------------------------------TEST MIN RESULTS --------------------------------------------------");
//
//			super.authenticate(username);
//
//			@SuppressWarnings("unused")
//			final Double minResults = this.administratorService.minItemsProvider();
//			final String print = minResults.toString();
//			System.out.println(print);
//
//			super.unauthenticate();
//			System.out.println("----------------------------PASSED-------------------------------\r");
//		} catch (final Throwable oops) {
//			caught = oops.getClass();
//		}
//
//		this.checkExceptions(expected, caught);
//
//	}
//
//	protected void testMaxItemsProvider(final String username, final Class<?> expected) {
//
//		Class<?> caught = null;
//
//		try {
//
//			System.out.println("------------------------------------TEST MAX RESULTS --------------------------------------------------");
//
//			super.authenticate(username);
//
//			@SuppressWarnings("unused")
//			final Double maxResults = this.administratorService.maxItemsProvider();
//			final String print = maxResults.toString();
//			System.out.println(print);
//
//			super.unauthenticate();
//			System.out.println("----------------------------PASSED-------------------------------\r");
//		} catch (final Throwable oops) {
//			caught = oops.getClass();
//		}
//
//		this.checkExceptions(expected, caught);
//
//	}
//
//	protected void testStddevItemsProvider(final String username, final Class<?> expected) {
//
//		Class<?> caught = null;
//
//		try {
//
//			System.out.println("------------------------------------TEST STDDEV RESULTS --------------------------------------------------");
//
//			super.authenticate(username);
//
//			@SuppressWarnings("unused")
//			final Double stddevResults = this.administratorService.stddevItemsProvider();
//			final String print = stddevResults.toString();
//			System.out.println(print);
//
//			super.unauthenticate();
//			System.out.println("----------------------------PASSED-------------------------------\r");
//		} catch (final Throwable oops) {
//			caught = oops.getClass();
//		}
//
//		this.checkExceptions(expected, caught);
//
//	}
//
//	protected void testRatioFinders(final String username, final Class<?> expected) {
//
//		Class<?> caught = null;
//
//		try {
//
//			System.out.println("------------------------------------TEST RATIO FINDERS --------------------------------------------------");
//
//			super.authenticate(username);
//
//			@SuppressWarnings("unused")
//			final Double ratioFinders = this.administratorService.ratioFinders();
//			final String print = ratioFinders.toString();
//			System.out.println(print);
//
//			super.unauthenticate();
//			System.out.println("----------------------------PASSED-------------------------------\r");
//		} catch (final Throwable oops) {
//			caught = oops.getClass();
//		}
//
//		this.checkExceptions(expected, caught);
//
//	}
//
//	//DRIVERS
//
//	@Test
//	public void driverAvgSalary() {
//
//		final Object testingData[][] = {
//
//			// sentence coverage 100%
//
//			// a) Mostrar información de la dashboard como admin
//			{
//				"admin", null,
//			},
//			// a) Mostrar información de la dashboard como admin
//			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
//			{
//				"rookie1", IllegalArgumentException.class,
//			},
//
//		};
//
//		for (int i = 0; i < testingData.length; i++)
//			this.testAvgAppRookie((String) testingData[i][0], (Class<?>) testingData[i][1]);
//	}
//
//	@Test
//	public void driverMinSalary() {
//
//		final Object testingData[][] = {
//
//			// sentence coverage 100%
//
//			// a) Mostrar información de la dashboard como admin
//			{
//				"admin", null,
//			},
//			// a) Mostrar información de la dashboard como admin
//			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
//			{
//				"rookie1", IllegalArgumentException.class,
//			},
//
//		};
//
//		for (int i = 0; i < testingData.length; i++)
//			this.testMinAppRookie((String) testingData[i][0], (Class<?>) testingData[i][1]);
//	}
//
//	@Test
//	public void driverMaxSalary() {
//
//		final Object testingData[][] = {
//
//			// sentence coverage 100%
//
//			// a) Mostrar información de la dashboard como admin
//			{
//				"admin", null,
//			},
//			// a) Mostrar información de la dashboard como admin
//			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
//			{
//				"rookie1", IllegalArgumentException.class,
//			},
//
//		};
//
//		for (int i = 0; i < testingData.length; i++)
//			this.testMaxAppRookie((String) testingData[i][0], (Class<?>) testingData[i][1]);
//	}
//
//	@Test
//	public void driverStddevSalary() {
//
//		final Object testingData[][] = {
//
//			// sentence coverage 100%
//
//			// a) Mostrar información de la dashboard como admin
//			{
//				"admin", null,
//			},
//			// a) Mostrar información de la dashboard como admin
//			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
//			{
//				"rookie1", IllegalArgumentException.class,
//			},
//
//		};
//
//		for (int i = 0; i < testingData.length; i++)
//			this.testStddevAppRookie((String) testingData[i][0], (Class<?>) testingData[i][1]);
//	}
//
//	@Test
//	public void driverAvgAuditScore() {
//
//		final Object testingData[][] = {
//
//			// sentence coverage 100%
//
//			// a) Mostrar información de la dashboard como admin
//			{
//				"admin", null,
//			},
//			// a) Mostrar información de la dashboard como admin
//			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
//			{
//				"rookie1", IllegalArgumentException.class,
//			},
//
//		};
//
//		for (int i = 0; i < testingData.length; i++)
//			this.testAvgAuditScore((String) testingData[i][0], (Class<?>) testingData[i][1]);
//	}
//
//	@Test
//	public void driverMinAuditScore() {
//
//		final Object testingData[][] = {
//
//			// sentence coverage 100%
//
//			// a) Mostrar información de la dashboard como admin
//			{
//				"admin", null,
//			},
//			// a) Mostrar información de la dashboard como admin
//			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
//			{
//				"rookie1", IllegalArgumentException.class,
//			},
//
//		};
//
//		for (int i = 0; i < testingData.length; i++)
//			this.testMinAuditScore((String) testingData[i][0], (Class<?>) testingData[i][1]);
//	}
//
//	@Test
//	public void driverMaxAuditScore() {
//
//		final Object testingData[][] = {
//
//			// sentence coverage 100%
//
//			// a) Mostrar información de la dashboard como admin
//			{
//				"admin", null,
//			},
//			// a) Mostrar información de la dashboard como admin
//			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
//			{
//				"rookie1", IllegalArgumentException.class,
//			},
//
//		};
//
//		for (int i = 0; i < testingData.length; i++)
//			this.testMaxAuditScore((String) testingData[i][0], (Class<?>) testingData[i][1]);
//	}
//
//	@Test
//	public void driverStddevAuditScore() {
//
//		final Object testingData[][] = {
//
//			// sentence coverage 100%
//
//			// a) Mostrar información de la dashboard como admin
//			{
//				"admin", null,
//			},
//			// a) Mostrar información de la dashboard como admin
//			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
//			{
//				"rookie1", IllegalArgumentException.class,
//			},
//
//		};
//
//		for (int i = 0; i < testingData.length; i++)
//			this.testStddevAuditScore((String) testingData[i][0], (Class<?>) testingData[i][1]);
//	}
//
//	@Test
//	public void driverAvgItemsProvider() {
//
//		final Object testingData[][] = {
//
//			// sentence coverage 100%
//
//			// a) Mostrar información de la dashboard como admin
//			{
//				"admin", null,
//			},
//			// a) Mostrar información de la dashboard como admin
//			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
//			{
//				"rookie1", IllegalArgumentException.class,
//			},
//
//		};
//
//		for (int i = 0; i < testingData.length; i++)
//			this.testAvgItemsProvider((String) testingData[i][0], (Class<?>) testingData[i][1]);
//	}
//
//	@Test
//	public void driverMinItemsProvider() {
//
//		final Object testingData[][] = {
//
//			// sentence coverage 100%
//
//			// a) Mostrar información de la dashboard como admin
//			{
//				"admin", null,
//			},
//			// a) Mostrar información de la dashboard como admin
//			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
//			{
//				"rookie1", IllegalArgumentException.class,
//			},
//
//		};
//
//		for (int i = 0; i < testingData.length; i++)
//			this.testMinitemsProvider((String) testingData[i][0], (Class<?>) testingData[i][1]);
//	}
//
//	@Test
//	public void driverMaxItemsProvider() {
//
//		final Object testingData[][] = {
//
//			// sentence coverage 100%
//
//			// a) Mostrar información de la dashboard como admin
//			{
//				"admin", null,
//			},
//			// a) Mostrar información de la dashboard como admin
//			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
//			{
//				"rookie1", IllegalArgumentException.class,
//			},
//
//		};
//
//		for (int i = 0; i < testingData.length; i++)
//			this.testMaxItemsProvider((String) testingData[i][0], (Class<?>) testingData[i][1]);
//	}
//
//	@Test
//	public void driverStddevItemsProvider() {
//
//		final Object testingData[][] = {
//
//			// sentence coverage 100%
//
//			// a) Mostrar información de la dashboard como admin
//			{
//				"admin", null,
//			},
//			// a) Mostrar información de la dashboard como admin
//			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
//			{
//				"rookie1", IllegalArgumentException.class,
//			},
//
//		};
//
//		for (int i = 0; i < testingData.length; i++)
//			this.testStddevItemsProvider((String) testingData[i][0], (Class<?>) testingData[i][1]);
//	}
//
//	@Test
//	public void driverRatioFindersResults() {
//
//		final Object testingData[][] = {
//
//			// sentence coverage 100%
//
//			// a) Mostrar información de la dashboard como admin
//			{
//				"admin", null,
//			},
//			// a) Mostrar información de la dashboard como admin
//			// b) Un usuario logueado como administrador debe ser capaz de mostrar ésta información en la dashboard
//			{
//				"rookie1", IllegalArgumentException.class,
//			},
//
//		};
//
//		for (int i = 0; i < testingData.length; i++)
//			this.testRatioFinders((String) testingData[i][0], (Class<?>) testingData[i][1]);
//	}
//
//}
