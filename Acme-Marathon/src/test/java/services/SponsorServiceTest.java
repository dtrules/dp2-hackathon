
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import security.LoginService;
import utilities.AbstractTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class SponsorServiceTest extends AbstractTest {

	@Autowired
	private SponsorshipService	sponsorshipService;


	protected void testListSponsorships(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.sponsorshipService.findSponsorshipsBySponsor(LoginService.getPrincipal().getId());
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverListSponsorships() {

		final Object testingData[][] = {
			//Tests positivos, conun runer y un organizer
			{
				"sponsor1", null,
			}, {
				//Test negativo de un usuario no logeado
				"null", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testListSponsorships((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testCreateSponsorships(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.sponsorshipService.create();
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverCreateSponsorships() {

		final Object testingData[][] = {
			//Tests positivos, conun runer y un organizer
			{
				"sponsor1", null,
			}, {
				//Test negativo de un usuario no logeado
				"null", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testCreateSponsorships((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testDeleteSponsorships(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.sponsorshipService.delete(590);
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverDeleteSponsorships() {

		final Object testingData[][] = {
			//Tests positivos, conun runer y un organizer
			{
				"sponsor1", null,
			}, {
				//Test negativo de un usuario no logeado
				"null", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testDeleteSponsorships((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
}
