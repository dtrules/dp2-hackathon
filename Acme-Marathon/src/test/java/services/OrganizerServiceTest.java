
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Complaint;
import domain.Marathon;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class OrganizerServiceTest extends AbstractTest {

	//Services in use
	@Autowired
	private MarathonService				marathonService;

	@Autowired
	private ComplaintService			complaintService;

	@Autowired
	private PaymentService				paymentService;

	@Autowired
	private CheckpointService			checkpointService;

	@Autowired
	private WorkerApplicationService	workerApplicationService;


	protected void testListMarathons(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.marathonService.findAll();
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverListMarathons() {

		final Object testingData[][] = {
			//Tests positivos, conun runer y un organizer
			{
				"organizer1", null,
			}, {
				"runner1", null,
			}, {
				//Test negativo de un usuario no logeado
				"null", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testListMarathons((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	//Test create

	protected void testCreateMarathons(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.marathonService.create();
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverCreateMarathons() {

		final Object testingData[][] = {
			//Tests positivos, crear una maraton con un organizador
			{
				"organizer1", null,
			}, {

				//Test negativo, crear una maraton siendo un runner
				"runner1", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testCreateMarathons((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	//Test update

	protected void testUpdateMarathons(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			final Marathon marathon = this.marathonService.findOne(544);
			marathon.setAddress("Edited adress");
			this.marathonService.save(marathon);
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverUpdateMarathons() {

		final Object testingData[][] = {
			//Tests positivos, editar una maraton con un organizador
			{
				"organizer1", null,
			}, {

				//Test negativo, editarS una maraton siendo un runner
				"runner1", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testCreateMarathons((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testListComplaints(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.complaintService.getComplaintsByMarathon(576);
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverListComplaints() {

		final Object testingData[][] = {
			//Tests positivos, editar una maraton con un organizador
			{
				"organizer1", null,
			}, {

				//Test negativo, editarS una maraton siendo un suuario no logeado
				"null", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testListComplaints((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testAnswerComplaints(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			final Complaint c = this.complaintService.findOne(587);
			c.setAnswerDescription("new answer");
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverAnswerComplaints() {

		final Object testingData[][] = {
			//Tests positivos, editar una maraton con un organizador
			{
				"organizer1", null,
			}, {

				//Test negativo, editarS una maraton siendo un suuario no logeado
				"null", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testAnswerComplaints((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testPayment(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.paymentService.create();
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverPayment() {

		final Object testingData[][] = {
			//Tests positivos, editar una maraton con un organizador
			{
				"organizer1", null,
			}, {

				//Test negativo, editarS una maraton siendo un suuario no logeado
				"null", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testAnswerComplaints((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testListCheckpoint(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.checkpointService.findByMarathon(576);
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverListCheckpoint() {

		final Object testingData[][] = {
			//Tests positivos, editar una maraton con un organizador
			{
				"organizer1", null,
			}, {

				//Test negativo, editarS una maraton siendo un suuario no logeado
				"null", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testListCheckpoint((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testCreateCheckpoint(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.checkpointService.create();
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverCreateCheckpoint() {

		final Object testingData[][] = {
			//Tests positivos, editar una maraton con un organizador
			{
				"organizer1", null,
			}, {

				//Test negativo, editarS una maraton siendo un suuario no logeado
				"null", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testCreateCheckpoint((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testListWorkerApp(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.workerApplicationService.getWorkerApplicationsByMarathon(576);
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverListWorkerApp() {

		final Object testingData[][] = {
			//Tests positivos, editar una maraton con un organizador
			{
				"organizer1", null,
			}, {

				//Test negativo, editarS una maraton siendo un suuario no logeado
				"null", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testListWorkerApp((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	protected void testUpdateWorkerApp(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			super.authenticate(username);
			this.workerApplicationService.acceptWorkerApplication(582);
			super.unauthenticate();
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	@Test
	public void driverUpdateWorkerApp() {

		final Object testingData[][] = {
			//Tests positivos, editar una maraton con un organizador
			{
				"organizer1", null,
			}, {

				//Test negativo, editarS una maraton siendo un suuario no logeado
				"null", IllegalArgumentException.class,
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.testUpdateWorkerApp((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

}
